﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SalesTemplate.Filters
{
    public class Secure : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                JObject userData = (JObject)JsonConvert.DeserializeObject(ticket.UserData);

                var claims = new List<Claim>();
                claims.Add(new Claim("FirstName", userData["FirstName"].ToString()));
                claims.Add(new Claim("LastName", userData["LastName"].ToString()));
                claims.Add(new Claim("EmailAddress", userData["EmailAddress"].ToString()));
                claims.Add(new Claim("UserID", userData["UserID"].ToString()));

                var identity = new ClaimsIdentity(claims, "FormsAuth");
                var claimsPrincipal = new ClaimsPrincipal(identity);

                // Set current principal
                Thread.CurrentPrincipal = claimsPrincipal;
            }

            base.OnAuthorization(context);
        }
    }
}