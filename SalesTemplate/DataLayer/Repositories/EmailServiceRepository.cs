﻿using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using System;
using System.Collections.Generic;

namespace DataLayer.Repositories
{
    public class EmailServiceRepository : BaseRepository
    {
        public async Task<bool> IsIPInEmailRequestConversion(string ip)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.EmailRequestConversions
                .Where(x => x.IPAddress == ip)
                .AnyAsync()
                .ConfigureAwait(false);
            }
        }

        public async Task<Tuple<bool, string>> IsEmailInEmailRequestConversion(string email, string ipAddress = null)
        {
            using (var _db = new SalesTemplateContext())
            {
                if (!String.IsNullOrEmpty(ipAddress))
                {
                    var record = await _db.EmailRequestConversions
                                        .Where(x => x.EmailAddress == email || x.IPAddress == ipAddress)
                                        .ToListAsync()
                                        .ConfigureAwait(false);

                    if (record.Count > 0)
                        if (record.Where(x => x.EmailAddress == email).Any())
                            return new Tuple<bool, string>(true, "email");
                        else if (record.Where(x => x.IPAddress == ipAddress).Any())
                            return new Tuple<bool, string>(true, "ipaddress");
                        else
                            return new Tuple<bool, string>(false, String.Empty);
                    else
                        return new Tuple<bool, string>(false, String.Empty);
                }
                else
                {
                    return new Tuple<bool, string>(
                        await _db.EmailRequestConversions
                                        .Where(x => x.EmailAddress == email)
                                        .AnyAsync()
                                        .ConfigureAwait(false),
                            String.Empty
                        );
                }
            }
        }        

        public async Task<bool> RecordEmailAddress(string email, string ip, decimal reward)
        {
            using (var _db = new SalesTemplateContext())
            {
                var record = new EmailRequestConversion()
                {
                    AccountCreated = false,
                    IPAddress = ip,
                    EmailAddress = email,
                    CreditAwarded = reward,
                    DateCreated = DateTime.Now
                };

                _db.EmailRequestConversions.Add(record);
                return await _db.SaveChangesAsync() > 0 ? true : false;
            }
        }

        public async Task UpdateEmailRequestConversion(string email, bool accountCreated)
        {
            using (var _db = new SalesTemplateContext())
            {
                var erc = await _db.EmailRequestConversions.Where(x => x.EmailAddress == email)
                                                           .FirstOrDefaultAsync();

                if (erc != null)
                {
                    erc.AccountCreated = accountCreated;
                    _db.EmailRequestConversions.Attach(erc);
                    _db.Entry(erc).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                }
            }
        }

        public async Task DeleteEmailRequestConversion(string email)
        {
            using (var _db = new SalesTemplateContext())
            {
                var erc = await _db.EmailRequestConversions.Where(x => x.EmailAddress == email).FirstOrDefaultAsync();

                if (erc != null)
                {
                    _db.EmailRequestConversions.Remove(erc);
                    await _db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
        }

        public async Task<List<string>> GetEmailAddressList()
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.EmailRequestConversions
                                .Where(x => x.AccountCreated == false)
                                .Select(x => x.EmailAddress)
                                .ToListAsync();
            }
        }
    }
}
