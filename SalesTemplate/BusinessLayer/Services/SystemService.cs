﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer.Services
{
    public class SystemService
    {
        private SystemRepository _repo { get; set; }

        public SystemService()
        {
            _repo = new SystemRepository();
        }

        public async Task<AddressType> GetAddressTypeByName(string name)
        {
            return await _repo.GetAddressTypeByName(name).ConfigureAwait(false);
        }

        public async Task<List<sys_State>> GetStatesByCountry(int country)
        {
            return await _repo.GetStatesByCountry(country).ConfigureAwait(false);
        }

        public async Task<List<sys_Country>> GetCountries()
        {
            return await _repo.GetCountries().ConfigureAwait(false);
        }

        public async Task<sys_Country> GetCountryByID(int id)
        {
            return await _repo.GetCountryByID(id).ConfigureAwait(false);
        }
    }
}
