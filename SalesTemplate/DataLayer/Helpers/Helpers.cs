﻿using DataLayer.Data.Entities;
using DataLayer.Helpers.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Helpers
{
    public class Helpers
    {
        public static decimal GetUndiscountedPrice(int productID, decimal? price)
        {
            return (Convert.ToInt32(ConfigurationManager.AppSettings["SpecialProductDiscountID"]) == productID ?
                        Math.Round((Convert.ToDecimal(ConfigurationManager.AppSettings["SpecialProductDiscountMultiplier"]) * (decimal)price), 2) :
                        Math.Round((Convert.ToDecimal(ConfigurationManager.AppSettings["RegularDiscountMultiplier"]) * (decimal)price), 2));
        }

        public static List<Product> FilterProductsAndOptions(List<Product> retval)
        {
            try
            {
                for (int i = 0; i < retval.Count; i++)
                {
                    var options = retval[i].ProductOptions;

                    // remove products with 0 quantity in either product, parent option with no children, or child option
                    if (options.Count > 0)
                    {
                        var children = options.Where(x => x.ParentOptionID != null).ToList();

                        if (children.Count == 0)
                        {
                            var parents = options.Where(x => x.ParentOptionID == null).ToList();

                            parents = parents.Where(x => x.QuantityAvailable > 0).ToList();

                            if (parents.Count == 0)
                            {
                                retval.Remove(retval[i]);
                                i--;
                                continue;
                            }
                        }
                        else
                        {
                            children = children.Where(x => x.QuantityAvailable > 0).ToList();

                            if (children.Count == 0)
                            {
                                retval.Remove(retval[i]);
                                i--;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        if (retval[i].QuantityAvailable < 1)
                        {
                            retval.Remove(retval[i]);
                            i--;
                            continue;
                        }
                    }

                    var childList1wParent = options.Where(x => x.ParentOptionID != null && x.QuantityAvailable > 0).ToList();
                    var childrenOfParentsToRemove = options.Where(x => x.ParentOptionID != null && x.QuantityAvailable == 0).ToList();
                    var childList2noParents = options.Where(x => x.ParentOptionID == null && x.QuantityAvailable > 0).ToList();
                    var parentList = options.Where(x => childList1wParent.Select(a => a.ParentOptionID).ToList().Contains(x.ID)).ToList();

                    if (retval[i].ProductOptions == null)
                    {
                        retval[i].ProductOptions = new List<ProductOption>();
                    }

                    // remove all and add back only options with quantity > 0
                    // ignores erroneous quanities in parents who's children have 0 quantity 
                    retval[i].ProductOptions.Clear();

                    foreach (var option in childList1wParent)
                        retval[i].ProductOptions.Add(option);

                    foreach (var option in childList2noParents)
                        if (!childrenOfParentsToRemove.Select(x => x.ParentOptionID).ToList().Contains(option.ID))
                            retval[i].ProductOptions.Add(option);

                    foreach (var option in parentList)
                        retval[i].ProductOptions.Add(option);

                    // Get crossed out undiscounted price for front-end
                    if (retval[i].Price != null)
                        retval[i].UndiscountedPrice = GetUndiscountedPrice(retval[i].ID, retval[i].Price);

                    foreach (var item in retval[i].ProductOptions)
                    {
                        if (item.Price != null)
                            item.UndiscountedPrice = GetUndiscountedPrice(item.ProductID, item.Price);
                    }
                }
            }
            catch (Exception ex)
            {
                // silent
            }

            return retval;
        }

        public static OptionSelection FilterOptionSelection(Product product, ProductOption option)
        {
            decimal? price = 0;
            string imagePath = String.Empty;
            string optionList = String.Empty;

            if (option != null)
            {
                if (option.ParentOptionID == null) // parent/only-child option changed
                {
                    var children = product.ProductOptions.Where(x => x.ParentOptionID == option.ID).ToList();

                    if (children.Count > 0)
                    {
                        imagePath = children.Select(x => x.ImagePath).FirstOrDefault();
                        price = children.Select(x => x.Price).FirstOrDefault();

                        if (String.IsNullOrEmpty(imagePath))
                            imagePath = (!String.IsNullOrEmpty(option.ImagePath) ? option.ImagePath : product.ImagePath);

                        if (price == null)
                            price = (option.Price != null ? option.Price : product.Price);
                    }
                    else
                    {
                        imagePath = (String.IsNullOrEmpty(option.ImagePath) ? product.ImagePath : option.ImagePath);
                        price = (option.Price == null ? product.Price : option.Price);
                        optionList = String.Empty;
                    }

                    return new OptionSelection()
                    {
                        ImagePath = imagePath,
                        Options = (children.Count == 0 ? null : children),
                        Price = Convert.ToDecimal(price),
                        UndiscountedPrice = DataLayer.Helpers.Helpers.GetUndiscountedPrice(product.ID, price)
                    };
                }
                else // child option changed
                {
                    imagePath = option.ImagePath;
                    price = option.Price;

                    if (String.IsNullOrEmpty(imagePath))
                    {
                        if (String.IsNullOrEmpty(imagePath))
                            imagePath = (!String.IsNullOrEmpty(option.ImagePath) ? option.ImagePath : product.ImagePath);
                    }

                    if (price == null)
                    {
                        if (price == null)
                            price = (option.Price != null ? option.Price : product.Price);
                    }

                    return new OptionSelection()
                    {
                        ImagePath = imagePath,
                        Options = null,
                        Price = Convert.ToDecimal(price),
                        UndiscountedPrice = DataLayer.Helpers.Helpers.GetUndiscountedPrice(product.ID, price)
                    };
                }
            }
            else
            {
                return new OptionSelection()
                {
                    ImagePath = product.ImagePath,
                    Price = (decimal)product.Price,
                    UndiscountedPrice = DataLayer.Helpers.Helpers.GetUndiscountedPrice(product.ID, product.Price)
                };
            }
        }
    }
}
