﻿var elem;
new autoComplete({
    selector: 'input[name="header-search"]',
    minChars: 2,
    source: function (term, suggest) {
        term = term.toLowerCase();
        var choices = ['Dresses', 'Hoodies', 'Outfits', 'Sweaters', 'Dinnerware', 'Collars', 'Shirts', 'Pants', 'Rain Gear', 'Dishes', 'Bowls'];
        var matches = [];
        for (i = 0; i < choices.length; i++)
            if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
        suggest(matches);
    }
});
$(document).on('ready', function () {
    $('select option:first-child').attr('selected', 'selected');
    if ($('[name="hdnIsAuthenticated"]').val() == 'False') {
        if (!sessionStorage.totclkh) {
            sessionStorage.totclkh = 0;
        }
        
        setTimeout(
            function () {
                if (Number(sessionStorage.totclkh) == 0) {
                    if ($('#hdnCurrentPromoAmount').val() > 0) {
                        sessionStorage.totclkh = 1;
                        requestCustomerEmail();
                    }
                }
            }, 800);
    }
});
$(document).on('submit', '#top-search-form', function (e) {
    e.preventDefault();
    window.location = 'Products';
});
$(document).on('click', '.add-cart-btn', function () {
    elem = $(this);
    $.post('/Cart/UpdateAndCalculateShoppingCart', getAddCartData(), setSnapshot);
});
$(document).on('change', '.option-parent-list', function () {
    elem = $(this);
    $.post('/Cart/UpdateProductPane', getListData(), parentOptionSelect);
});
$(document).on('change', '.option-child-list', function () {
    elem = $(this);
    $.post('/Cart/UpdateProductPane', getListData(), childOptionSelect);
});
$(document).on('click', '.grid-add-wishlist', function (e) {
    e.stopImmediatePropagation();
    if ($('#hdnIsAuthenticated').val() == 'True') {
        elem = $(this);
        $.post('/Wishlist/AddToWishlist', getAddWishlistData(), addWishlistResult);
    }
    else {
        alertify.error('You must be logged in to update your wishlist. <br /><br /><a href="/Login" class="wishlist-login">Login Now!</a>');
    }
});
$('#email-subscribe-form').on('submit', function (e) {
    e.preventDefault();
    var email = $('.email-subscribe');

    if (email.val() == '') {
        email.closest('div').addClass('has-error');
    }
    else if (!isValidEmailAddress(email.val())) {
        email.closest('div').addClass('has-error');
    }
    else {
        recordEmail(email);
    }
});
function addWishlistResult(result) {
    if (result.Success) {
        alertify.success('Your wishlist has been updated!');
    }
    else {
        if (result.Message != '') {
            alertify.error(result.Message);
        }
    }
}
function getAddWishlistData() {
    var optionID = '';

    if (elem.closest('.product').find('.option-child-list').val() != undefined) {
        optionID = elem.closest('.product').find('.option-child-list').val();
    }
    else if (elem.closest('.product').find('.option-parent-list').val() != undefined) {
        optionID = elem.closest('.product').find('.option-parent-list').val();
    }
    else {
        optionID = 0;
    }
    return {
        __RequestVerificationToken: GLB_TOKEN,
        productID: elem.closest('.product').attr('data-product-id'),
        optionID: optionID
    }
}
function parentOptionSelect(result) {
    if (result.ChildList == '') {
        elem.closest('.parent-opt-wrap').css('margin-top', '-2px');
        elem.closest('.product-caption').find('.product-price-wrap').css('margin-top', '0px');
        elem.closest('.product-caption').find('.product-rating').css('margin-top', '0px');
    }
    else {
        elem.closest('.parent-opt-wrap').css('margin-top', '-18px');
        elem.closest('.product-caption').find('.product-price-wrap').css('margin-top', '-7px');
        elem.closest('.product-caption').find('.product-rating').css('margin-top', '-4px');
    }
    elem.closest('.product').find('.img-overlay').attr('src', result.ImagePath);
    elem.closest('.product').find('.image').attr('src', result.ImagePath);
    elem.closest('.product-caption').find('.child-opt-wrap').html(result.ChildList);
    elem.closest('.product-caption').find('.product-price').html('<span>' +
        parseFloat(result.UndiscountedPrice).toFixed(2) + '</span> ' +
        parseFloat(result.Price).toFixed(2));
}
function childOptionSelect(result) {
    elem.closest('.product').find('.img-overlay').attr('src', result.ImagePath);
    elem.closest('.product').find('.image').attr('src', result.ImagePath);
    elem.closest('.product-caption').find('.product-price').html('<span>' +
        parseFloat(result.UndiscountedPrice).toFixed(2) + '</span> ' +
        parseFloat(result.Price).toFixed(2));
}
function getListData() {
    var productID = elem.closest('.product[data-product-id]').attr('data-product-id');
    var optionID = elem.val();

    return {
        __RequestVerificationToken: GLB_TOKEN,
        productID: productID,
        optionID: optionID
    }
}
function getAddCartData() {
    var optionID = '';

    if (elem.closest('.product').find('.option-child-list').val() != undefined) {
        optionID = elem.closest('.product').find('.option-child-list').val();
    }
    else if (elem.closest('.product').find('.option-parent-list').val() != undefined) {
        optionID = elem.closest('.product').find('.option-parent-list').val();
    }
    else {
        optionID = 0;
    }
    return {
        __RequestVerificationToken: GLB_TOKEN,
        productID: elem.closest('.product').attr('data-product-id'),
        optionID: optionID,
        quantity: 1,
        quantify: true,
        applyCredits: false
    }
}