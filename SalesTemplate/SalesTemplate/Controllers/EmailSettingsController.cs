﻿using BusinessLayer.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    public class EmailSettingsController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ViewResult Unsubscribe(string a)
        {
            return View(new UnsubscribeClass() { Hash = a });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task Unsubscribe(UnsubscribeClass a)
        {
            try
            {
                var emailSvc = new BusinessLayer.Services.EmailService();
                await emailSvc.Unsubscribe(a.Hash).ConfigureAwait(false);
            }
            catch { }
        }
        
        public class UnsubscribeClass
        {
            public string Hash { get; set; }
        }
    }
}