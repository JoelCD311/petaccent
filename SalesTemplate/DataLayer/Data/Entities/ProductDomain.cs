﻿using System.Collections.Generic;

namespace DataLayer.Data.Entities
{
    public partial class ProductDomain
    {
        public ProductDomain()
        {
            this.Products = new HashSet<Product>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
