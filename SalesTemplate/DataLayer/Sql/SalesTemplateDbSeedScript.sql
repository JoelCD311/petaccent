USE [PetAccentDB]
GO
SET IDENTITY_INSERT [dbo].[AddressType] ON 

INSERT [dbo].[AddressType] ([ID], [Type]) VALUES (1, N'Shipping')
INSERT [dbo].[AddressType] ([ID], [Type]) VALUES (2, N'Billing')
SET IDENTITY_INSERT [dbo].[AddressType] OFF
SET IDENTITY_INSERT [dbo].[Charity] ON 

INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (1, N'ASPCA: American Society for the Prevention of Cruelty to Animals', N'ASPCA', N'', N'PO Box 96929', N'', N'', N'Washington', N'DC', N'20090-6929', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (2, N'Humane Society of The United States', N'Humane Society International', N'', N'2100 L Street, NW', N'', N'', N'Washington', N'DC', N'20037', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (3, N'Paws for Purple Hearts', N'Paws for Purple Hearts', N'', N'5860 Labath Avenue, Suite A', N'', N'', N'Rohnert Park', N'CA', N'94928', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (4, N'Paws4Vets', N'Paws4Vets', N'', N'1121C-324 Military Cutoff Rd', N'', N'', N'Wilmington', N'NC', N'28405', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (5, N'PETA: People for the Ethical Treatment of Animals', N'People for the Ethical Treatment of Animals', N'', N'501 Front St.', N'', N'', N'Norfolk', N'VA', N'23510', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (6, N'San Diego Zoo Global', N'San Diego Zoo Development Department', N'Attn: Donation Processing', N'P.O. Box 120551', N'', N'', N'San Diego', N'CA', N'92112-0551', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (7, N'SNAP: Spay-Neuter Assistance Program', N'SNAP INC.', N'', N'6758 Ingram Rd.', N'', N'', N'San Antonio', N'TX', N'78238', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (8, N'House Rabbit Society', N'House Rabbit Society', N'', N'148 Broadway', N'', N'', N'Richmond', N'CA', N'94804', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (9, N'Horse Charities of America', N'Horse Charities of America', N'', N'168 Long Lots Road', N'', N'', N'Westport', N'CT', N'06880', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (10, N'NEADS: Dogs for Deaf and Disabled Americans', N'NEADS: Dogs for Deaf and Disabled Americans', N'', N'305 Redemption Rock Trail South', N'', N'', N'Princeton', N'MA', N'01541', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (11, N'Africa Network for Animal Welfare - USA', N'ANAW - USA Office', N'', N'1391 N. Speer Boulevard, Suite 360', N'', N'', N'Denver', N'CO', N'80204', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
INSERT [dbo].[Charity] ([ID], [CharityName], [ContactFirstName], [ContactLastName], [MailingStreet1], [MailingStreet2], [MailingStreet3], [MailingCity], [MailingState], [MailingPostalCode], [MailingCountry], [DateCreated]) VALUES (12, N'Alley Cat Rescue', N'Alley Cat Rescue, Inc.', N'', N'P O Box 585', N'', N'', N'Mount Rainier', N'MD', N'20712', N'', CAST(N'2017-04-23T19:16:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Charity] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [FirstName], [LastName], [EmailAddress], [Password], [Salt], [Phone], [Active], [EmailSubscribed], [PreferredCharityID], [PaymentProcessorID], [DateCreated]) VALUES (10, N'Joel', N'Dunn', N'joelcdunn@gmail.com', N'S6fYtCnzT+etLWt83BlVAg==', N'2aa27615-3dd9-49f3-afe4-52ee6dcc1487', CAST(8177166258 AS Numeric(18, 0)), 1, 1, 9, N'cus_BBH8j3BM3ehXGc', CAST(N'2017-08-05T18:55:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([ID], [UserID], [Type], [Street1], [Street2], [Street3], [City], [State], [PostalCode], [Country], [DateCreated]) VALUES (1, 10, 1, N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', CAST(N'2017-08-10T11:14:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Address] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (39, 10, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(133.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4.11 AS Decimal(18, 2)), CAST(128.10 AS Decimal(18, 2)), CAST(12.40 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T16:54:00' AS SmallDateTime), 0, N'ASDFASDFASDF', N'CDBD65B14C', NULL, N'::1', CAST(N'2017-08-15T16:54:00' AS SmallDateTime))
INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (40, 10, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(133.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4.11 AS Decimal(18, 2)), CAST(128.10 AS Decimal(18, 2)), CAST(12.40 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T16:56:00' AS SmallDateTime), 0, N'ASDFASDFASDF', N'36084F5A05', NULL, N'::1', CAST(N'2017-08-15T16:56:00' AS SmallDateTime))
INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (41, 10, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(133.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4.11 AS Decimal(18, 2)), CAST(128.10 AS Decimal(18, 2)), CAST(12.40 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T16:59:00' AS SmallDateTime), 0, N'ASDFASDFASDF', N'ED4DB88480', NULL, N'::1', CAST(N'2017-08-15T16:59:00' AS SmallDateTime))
INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (42, 10, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(133.99 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4.11 AS Decimal(18, 2)), CAST(128.10 AS Decimal(18, 2)), CAST(12.40 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T17:05:00' AS SmallDateTime), 0, N'ASDFASDFASDF', N'B09DA13DDF', NULL, N'::1', CAST(N'2017-08-15T17:05:00' AS SmallDateTime))
INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (43, NULL, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr', NULL, NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(64.98 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4.11 AS Decimal(18, 2)), CAST(69.09 AS Decimal(18, 2)), CAST(6.50 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T19:14:00' AS SmallDateTime), 0, N'ch_1ArF9gJAH5MLUoKr1EOOalpb', N'65CFA5A902', NULL, N'::1', CAST(N'2017-08-15T19:14:00' AS SmallDateTime))
INSERT [dbo].[Order] ([ID], [UserID], [EmailAddress], [ShippingName], [ShippingStreet1], [ShippingStreet2], [ShippingStreet3], [ShippingCity], [ShippingState], [ShippingPostalCode], [ShippingCountry], [ShippingNotes], [OrderSubTotal], [StoreCreditsApplied], [StoreCreditID], [TaxTotal], [ShippingCharge], [OrderTotal], [TotalToDonate], [CharityID], [ShippingCost], [ShippedDate], [TrackingNumber], [PaymentMethodID], [OrderStatusID], [OrderDate], [IsGift], [PaymentConfirmation], [OrderID], [CampaignID], [IPAddress], [DateCreated]) VALUES (44, NULL, N'joelcdunn@gmail.com', N'Joel Dunn', N'5916 Fantail Dr.', N'#234', NULL, N'Fort Worth', N'TX', N'76179', N'US', NULL, CAST(92.97 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(0.00 AS Decimal(18, 2)), CAST(5.28 AS Decimal(18, 2)), CAST(98.25 AS Decimal(18, 2)), CAST(9.30 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, 1, 1, CAST(N'2017-08-15T19:23:00' AS SmallDateTime), 1, N'ch_1ArFIAJAH5MLUoKroQE3RT9r', N'C2B00EAAED', NULL, N'::1', CAST(N'2017-08-15T19:23:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[ProductDomain] ON 

INSERT [dbo].[ProductDomain] ([ID], [Name], [Description]) VALUES (1, N'Pet Accents', N'Accessories for your best friends')
SET IDENTITY_INSERT [dbo].[ProductDomain] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (18, N'PA-BDWB-01', N'Blue Cotton Dress', N'Beautiful blue floral dress with large blue bow in the back. Perfect for breezy summer afternoons!', CAST(24.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 1, N'/content/images/products/blue-dress-bow.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (19, N'PA-PDWB-01', N'Pink Cotton Dress', N'Perfect pink floral dress with large pink bow in the back. Another great pick for an afternoon with your best friend!', CAST(24.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 10, N'/content/images/products/pink-dress-bow.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (20, N'PA-BWFD-01', N'Blue and White Tulle Dress', N'This pretty blue and white tulle dress features a blue and white striped top with a gold-accented bow and a white tulle tutu.', CAST(34.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 16, N'/content/images/products/blue-white-fluffy-dress.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (21, N'PA-CSWN-01', N'Dress Shirt and Sweater', N'This super cool blue and red plaid dress shirt with pullover sweater is perfect for any business casual occassion.', CAST(31.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 9, N'/content/images/products/collar-sweater-navy.jpg', 1, N'Cats', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (22, N'PA-DRDR-01', N'Deep Red Tulle Dress', N'This sensational deep red formal dress will catch all eyes at your next gala event.', CAST(34.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 6, N'/content/images/products/deep-red-dress.jpg', 1, N'Cats', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (23, N'PA-LBDR-01', N'Latin Dance Dress', N'Keep your heart rate up on the dance floor in this black latin dance dress! Accented with shimmering rindstones!', CAST(36.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 14, N'/content/images/products/latin-dress-black.jpg', 1, N'Cats', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (24, N'PA-GLCO-01', N'Genuine Leather Collar', N'Hand your ID tags from these sturdy genuine leather collars. For larger pets (30-50lbs).', CAST(14.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, 8, 1, 0, 8, N'/content/images/products/leather-collar.jpg', 1, N'Dogs', N'Accessories', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (25, N'PA-NPSO-01', N'Corduroy & Plaid', N'Handsome navy and plaid suspender outfit. Navy blue corduroy bottoms and suspenders with a red and blue plaid top.', CAST(31.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 13, N'/content/images/products/navy-outfit-susp.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (26, N'PA-PFBE-01', N'Pet Food Bowl Set Ceramic/Wood', N'Gorgeous square ceramic food and water bowls that sit perfectly in a beautiful dark wooden stand. Rustic elegance at it''s finest!', CAST(44.99 AS Decimal(18, 2)), NULL, 0, CAST(5.23 AS Decimal(18, 2)), NULL, 0, 1, 1, 4, N'/content/images/products/pet-food-bowl-and-stand-wooden.jpg', 1, N'Dogs', N'Accessories', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (27, N'PA-PFBM-01', N'Pet Food Bowl Set Metal', N'Elegant yet heavy metal stand with tin food and water bowls to curb your best friend''s hunger and quinch their thirst!', CAST(39.99 AS Decimal(18, 2)), NULL, 0, CAST(5.23 AS Decimal(18, 2)), NULL, 5, 1, 0, 15, N'/content/images/products/pet-food-bowl-and-stand-metal.jpg', 1, N'Cats', N'Accessories', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (28, N'PA-PSBG-01', N'Prep School Boy', N'Send your significant other back to school with these preppy prep school outfits (for boys)!', CAST(29.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 2, N'/content/images/products/prep-school-boy.jpg', 1, N'Cats', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (29, N'PA-PSGG-01', N'Prep School Girl', N'Attention class! Prep school girl outfit will have your friend learning in school and staying classy!', CAST(29.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 7, N'/content/images/products/prep-school-girl.jpg', 1, N'Cats', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (30, N'PA-RSAI-01', N'Red Sailor', N'Ahoy matey! Nice warm sweater for an afternoon on a sailboat! Life vests on please!', CAST(27.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 5, N'/content/images/products/sailor-red.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (31, N'PA-NSAI-01', N'Navy Sailor', N'Land ho! Spend a day at the beach with these blue (and red and white) sweaters when the wind picks up!', CAST(27.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 11, N'/content/images/products/sailor-navy.jpg', 1, N'Dogs', N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (32, N'PA-USHG-01', N'USA! USA! USA! (Gray)', N'USA! Let your friend show their pride and spirit in this gray pullover hooded sweatshirt sporting ''USA'' and the American flag!', CAST(34.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 0, 12, N'/content/images/products/usa-hoodie-gray.jpg', 1, NULL, N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
INSERT [dbo].[Product] ([ID], [SKU], [Name], [Description], [Price], [Cost], [Taxable], [ShippingCharge], [ShippingCost], [QuantityAvailable], [Active], [Featured], [Order], [ImagePath], [DomainID], [Category], [SubCategory], [DateCreated]) VALUES (33, N'PA-USHR-01', N'USA! USA! USA! (Red)', N'USA! Let your friend show their pride and spirit in this red pullover hooded sweatshirt sporting ''USA'' and the American flag!', CAST(34.99 AS Decimal(18, 2)), NULL, 0, CAST(1.17 AS Decimal(18, 2)), NULL, NULL, 1, 1, 3, N'/content/images/products/usa-hoodie-red.jpg', 1, NULL, N'Fashion', CAST(N'2017-04-28T18:26:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductOption] ON 

INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (43, NULL, 18, N'Size', N'S (2-5 lbs)', 3, CAST(1.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (44, NULL, 18, N'Size', N'M (6-12 lbs)', 3, CAST(2.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (45, NULL, 18, N'Size', N'L (13-25 lbs)', 3, CAST(3.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (46, NULL, 19, N'Size', N'S (2-5 lbs)', 3, CAST(4.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (47, NULL, 19, N'Size', N'M (6-12 lbs)', 3, CAST(5.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (48, NULL, 19, N'Size', N'L (13-25 lbs)', 3, CAST(6.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (49, NULL, 20, N'Size', N'S (2-5 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (50, NULL, 20, N'Size', N'M (6-12 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, N'/content/images/products/pink-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (51, NULL, 20, N'Size', N'L (13-25 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (52, NULL, 21, N'Size', N'S (2-5 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (53, NULL, 21, N'Size', N'M (6-12 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (54, NULL, 21, N'Size', N'L (13-25 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (55, NULL, 22, N'Size', N'S (2-5 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (56, NULL, 22, N'Size', N'M (6-12 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (57, NULL, 22, N'Size', N'L (13-25 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (58, NULL, 23, N'Size', N'S (2-5 lbs)', 3, CAST(36.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (59, NULL, 23, N'Size', N'M (6-12 lbs)', 3, CAST(36.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (60, NULL, 23, N'Size', N'L (13-25 lbs)', 3, CAST(36.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (61, NULL, 25, N'Size', N'S (2-5 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (62, NULL, 25, N'Size', N'M (6-12 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (63, NULL, 25, N'Size', N'L (13-25 lbs)', 3, CAST(31.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (64, NULL, 28, N'Size', N'S (2-5 lbs)', 0, CAST(29.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (65, NULL, 28, N'Size', N'M (6-12 lbs)', 0, CAST(99.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (66, NULL, 28, N'Size', N'L (13-25 lbs)', 1, CAST(29.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (67, NULL, 29, N'Size', N'S (2-5 lbs)', 3, CAST(29.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (68, NULL, 29, N'Size', N'M (6-12 lbs)', 3, CAST(29.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (69, NULL, 29, N'Size', N'L (13-25 lbs)', 3, CAST(29.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (70, NULL, 30, N'Size', N'S (2-5 lbs)', 2, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (71, NULL, 30, N'Size', N'M (6-12 lbs)', 3, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (72, NULL, 30, N'Size', N'L (13-25 lbs)', 3, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (73, NULL, 31, N'Size', N'S (2-5 lbs)', 3, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (74, NULL, 31, N'Size', N'M (6-12 lbs)', 3, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (75, NULL, 31, N'Size', N'L (13-25 lbs)', 3, CAST(27.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (76, NULL, 32, N'Size', N'S (2-5 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (77, NULL, 32, N'Size', N'M (6-12 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (78, NULL, 32, N'Size', N'L (13-25 lbs)', 3, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (79, NULL, 33, N'Size', N'S (2-5 lbs)', 0, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (80, NULL, 33, N'Size', N'M (6-12 lbs)', 0, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (81, NULL, 33, N'Size', N'L (13-25 lbs)', 1, CAST(34.99 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (82, 43, 18, N'Color', N'Blue', 3, CAST(4.00 AS Decimal(18, 2)), NULL, N'/content/images/products/pink-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (83, 43, 18, N'Color', N'Yellow', 3, CAST(5.00 AS Decimal(18, 2)), NULL, N'/content/images/products/pink-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (84, 43, 18, N'Color', N'Red', 3, CAST(6.00 AS Decimal(18, 2)), NULL, N'/content/images/products/pink-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (85, 46, 19, N'Color', N'Blue', 3, CAST(7.00 AS Decimal(18, 2)), NULL, N'/content/images/products/blue-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (86, 46, 19, N'Color', N'Yellow', 3, CAST(8.00 AS Decimal(18, 2)), NULL, N'/content/images/products/blue-dress-bow.jpg')
INSERT [dbo].[ProductOption] ([ID], [ParentOptionID], [ProductID], [Name], [Value], [QuantityAvailable], [Price], [ShortValue], [ImagePath]) VALUES (87, 46, 19, N'Color', N'Red', 3, CAST(9.00 AS Decimal(18, 2)), NULL, N'/content/images/products/blue-dress-bow.jpg')
SET IDENTITY_INSERT [dbo].[ProductOption] OFF
SET IDENTITY_INSERT [dbo].[OrderLineItem] ON 

INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (61, 39, 28, 65, 1, CAST(0.00 AS Decimal(18, 2)), CAST(99.00 AS Decimal(18, 2)), CAST(N'2017-08-15T16:54:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (62, 39, 33, 80, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T16:54:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (63, 40, 28, 65, 1, CAST(0.00 AS Decimal(18, 2)), CAST(99.00 AS Decimal(18, 2)), CAST(N'2017-08-15T16:56:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (64, 40, 33, 80, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T16:56:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (65, 41, 28, 65, 1, CAST(0.00 AS Decimal(18, 2)), CAST(99.00 AS Decimal(18, 2)), CAST(N'2017-08-15T16:59:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (66, 41, 33, 80, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T16:59:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (67, 42, 28, 65, 1, CAST(0.00 AS Decimal(18, 2)), CAST(99.00 AS Decimal(18, 2)), CAST(N'2017-08-15T17:05:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (68, 42, 33, 80, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T17:05:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (69, 43, 28, 66, 1, CAST(0.00 AS Decimal(18, 2)), CAST(29.99 AS Decimal(18, 2)), CAST(N'2017-08-15T19:14:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (70, 43, 33, 81, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T19:14:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (71, 44, 28, 66, 1, CAST(0.00 AS Decimal(18, 2)), CAST(29.99 AS Decimal(18, 2)), CAST(N'2017-08-15T19:23:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (72, 44, 30, 70, 1, CAST(0.00 AS Decimal(18, 2)), CAST(27.99 AS Decimal(18, 2)), CAST(N'2017-08-15T19:23:00' AS SmallDateTime))
INSERT [dbo].[OrderLineItem] ([ID], [OrderID], [ProductID], [ProductOptionID], [Quantity], [TaxTotal], [SubTotal], [DateCreated]) VALUES (73, 44, 33, 81, 1, CAST(0.00 AS Decimal(18, 2)), CAST(34.99 AS Decimal(18, 2)), CAST(N'2017-08-15T19:23:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[OrderLineItem] OFF
SET IDENTITY_INSERT [dbo].[Wishlist] ON 

INSERT [dbo].[Wishlist] ([ID], [UserID], [ProductID], [OptionID], [Quantity], [DateCreated]) VALUES (38, 10, 28, 65, NULL, CAST(N'2017-08-13T12:14:00' AS SmallDateTime))
INSERT [dbo].[Wishlist] ([ID], [UserID], [ProductID], [OptionID], [Quantity], [DateCreated]) VALUES (40, 10, 30, 70, NULL, CAST(N'2017-08-15T13:10:00' AS SmallDateTime))
INSERT [dbo].[Wishlist] ([ID], [UserID], [ProductID], [OptionID], [Quantity], [DateCreated]) VALUES (41, 10, 19, 85, NULL, CAST(N'2017-08-15T13:12:00' AS SmallDateTime))
INSERT [dbo].[Wishlist] ([ID], [UserID], [ProductID], [OptionID], [Quantity], [DateCreated]) VALUES (42, 10, 18, 82, NULL, CAST(N'2017-08-15T13:13:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Wishlist] OFF
SET IDENTITY_INSERT [dbo].[StoreCreditType] ON 

INSERT [dbo].[StoreCreditType] ([ID], [Type]) VALUES (1, N'Credit')
INSERT [dbo].[StoreCreditType] ([ID], [Type]) VALUES (2, N'Reward')
INSERT [dbo].[StoreCreditType] ([ID], [Type]) VALUES (4, N'Coupon')
SET IDENTITY_INSERT [dbo].[StoreCreditType] OFF
SET IDENTITY_INSERT [dbo].[StoreCredit] ON 

INSERT [dbo].[StoreCredit] ([ID], [TypeID], [UserID], [CouponCode], [Amount], [Description], [Notes], [Expires], [DateCreated]) VALUES (11, 2, 10, N'JGPZM55SYE', CAST(10.00 AS Decimal(18, 2)), N'Email Request Granted', NULL, NULL, CAST(N'2017-08-05T18:55:00' AS SmallDateTime))
INSERT [dbo].[StoreCredit] ([ID], [TypeID], [UserID], [CouponCode], [Amount], [Description], [Notes], [Expires], [DateCreated]) VALUES (14, 2, 10, N'JGPZM55SYE', CAST(-10.00 AS Decimal(18, 2)), N'Email Request Granted', NULL, NULL, CAST(N'2017-08-05T18:55:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[StoreCredit] OFF
SET IDENTITY_INSERT [dbo].[sys_Country] ON 

INSERT [dbo].[sys_Country] ([ID], [Country], [CountryName]) VALUES (1, N'US', N'United States of America')
SET IDENTITY_INSERT [dbo].[sys_Country] OFF
SET IDENTITY_INSERT [dbo].[sys_State] ON 

INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (1, N'AL', N'Alabama', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (2, N'AK', N'Alaska', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (3, N'AZ', N'Arizona', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (4, N'AR', N'Arkansas', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (5, N'CA', N'California', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (6, N'CO', N'Colorado', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (7, N'CT', N'Connecticut', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (8, N'DE', N'Delaware', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (9, N'FL', N'Florida', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (10, N'GA', N'Georgia', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (11, N'HI', N'Hawaii', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (12, N'ID', N'Idaho', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (13, N'IL', N'Illinois', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (14, N'IN', N'Indiana', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (15, N'IA', N'Iowa', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (16, N'KS', N'Kansas', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (17, N'KY', N'Kentucky', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (18, N'LA', N'Louisiana', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (19, N'ME', N'Maine', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (20, N'MD', N'Maryland', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (21, N'MA', N'Massachusetts', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (22, N'MI', N'Michigan', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (23, N'MN', N'Minnesota', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (24, N'MS', N'Mississippi', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (25, N'MO', N'Missouri', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (26, N'MT', N'Montana', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (27, N'NE', N'Nebraska', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (28, N'NV', N'Nevada', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (29, N'NH', N'New Hampshire', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (30, N'NJ', N'New Jersey', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (31, N'NM', N'New Mexico', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (32, N'NY', N'New York', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (33, N'NC', N'North Carolina', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (34, N'ND', N'North Dakota', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (35, N'OH', N'Ohio', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (36, N'OK', N'Oklahoma', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (37, N'OR', N'Oregon', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (38, N'PA', N'Pennsylvania', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (39, N'RI', N'Rhode Island', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (40, N'SC', N'South Carolina', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (41, N'SD', N'South Dakota', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (42, N'TN', N'Tennessee', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (43, N'TX', N'Texas', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (44, N'UT', N'Utah', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (45, N'VT', N'Vermont', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (46, N'VA', N'Virginia', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (47, N'WA', N'Washington', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (48, N'WV', N'West Virginia', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (49, N'WI', N'Wisconsin', 1)
INSERT [dbo].[sys_State] ([ID], [State], [StateName], [Country]) VALUES (50, N'WY', N'Wyoming', 1)
SET IDENTITY_INSERT [dbo].[sys_State] OFF
SET IDENTITY_INSERT [dbo].[Campaign] ON 

INSERT [dbo].[Campaign] ([ID], [Name], [Description], [Agency], [DailyBudget], [TotalBudget], [TotalDaysRun], [DateCreated]) VALUES (1, N'fb3f8sjg3b2', N'First Facebook ad campaign', N'Facebook', CAST(10.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), 7, CAST(N'2017-04-23T19:53:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Campaign] OFF
SET IDENTITY_INSERT [dbo].[EmailRequestConversion] ON 

INSERT [dbo].[EmailRequestConversion] ([ID], [IPAddress], [EmailAddress], [CreditAwarded], [AccountCreated], [DateCreated]) VALUES (10, N'::1', N'joelcdunn@gmail.com', CAST(10.00 AS Decimal(18, 2)), 1, CAST(N'2017-04-25T16:42:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[EmailRequestConversion] OFF
SET IDENTITY_INSERT [dbo].[EmailTemplate] ON 

INSERT [dbo].[EmailTemplate] ([ID], [Name], [Subject], [Html], [PlainText], [FromAddress], [FromDisplayName], [DateCreated]) VALUES (3, N'WelcomeCustomer', N'Welcome to PetAccent.com!', N'<html style="height: 100%;">
<body style="font-family: Arial, Helvetica, sans-serif;">
    <div style="min-height: 100%;
            width: 70%;
            min-width: 380px;
            min-height: 400px;
            border: 2px solid;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
            position: relative;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -webkit-border-bottom-left-radius: 7px;
            -webkit-border-bottom-right-radius: 7px;
            -moz-border-radius-topleft: 7px;
            -moz-border-radius-topright: 7px;
            -moz-border-radius-bottomleft: 7px;
            -moz-border-radius-bottomright: 7px;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            background-image: url(''http://www.petaccent.com/Content/Images/welcome-image-trans.jpg'');
            background-position: center center;
            background-repeat: no-repeat;">
        <div style="height: 40px; min-height: 40px;
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54); display: block;"></div>
        <div style="padding: 20px;">
            <img src="http://www.petaccent.com/Content/Images/heart_pets_trans_sm6.png" />
            <div style="top: 30%; padding-right: 10px; margin-bottom: 40px; margin-top: 60px;">
                Welcome, #####!<br /><br />
                Thank you for signing up with PetAccent.com! Your account has been granted a $10 credit for use any time you''d like. Stay tuned for product updates and news from the site! <br /><br />
                10% of every order goes to charity, forever.<br /><br />
                Every time you make a purchase on PetAccent.com 10% of your order''s proceeds will be sent to the charity of your choosing, selected from a list of
                our preferred animal charities at the time of checkout. We send out checks to these charities quarterly. Donating to charity is a part of our committment to helping animals in
                need and is not a temporary policy or promotional offer.<br /><br />
                Sincerely,<br />
                PetAccent.com<br />
                <a href="mailto:customerservice@petaccent.com">CustomerService@PetAccent.com</a>
            </div>            
            <div style="font-size: 9pt; float: right;">* Charity options are listed in the checkout section of <a href="https://www.petaccent.com">petaccent.com</a>.</div>
        </div>
        <div style="height: 25px; min-height: 25px;
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54); display: block;">
        </div>
    </div>
</body>
</html>', N'Welcome, #####!\n\n
                Thank you for signing up with PetAccent.com! Your account has been granted a $10 credit for use any time you''d like. Stay tuned for product updates and news from the site!\n\n
                10% of every order goes to charity, forever.\n\n
                Every time you make a purchase on PetAccent.com 10% of your order''s proceeds will be sent to the charity of your choosing, selected from a list of
                our preferred animal charities at the time of checkout. We send out checks to these charities quarterly. Donating to charity is a part of our committment to helping animals in
                need and is not a temporary policy or promotional offer.\n\n
                Sincerely,\n
                PetAccent.com\n
                CustomerService@PetAccent.com', N'CustomerService@PetAccent.com', N'Customer Service', CAST(N'2017-04-25T12:25:00' AS SmallDateTime))
INSERT [dbo].[EmailTemplate] ([ID], [Name], [Subject], [Html], [PlainText], [FromAddress], [FromDisplayName], [DateCreated]) VALUES (4, N'ForgotPassword', N'Password Request', N'<html style="height: 100%;">
<body style="font-family: Arial, Helvetica, sans-serif;">
    <div style="min-height: 100%;
            width: 70%;
            min-width: 380px;
            min-height: 380px;
            border: 2px solid;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
            position: relative;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -webkit-border-bottom-left-radius: 7px;
            -webkit-border-bottom-right-radius: 7px;
            -moz-border-radius-topleft: 7px;
            -moz-border-radius-topright: 7px;
            -moz-border-radius-bottomleft: 7px;
            -moz-border-radius-bottomright: 7px;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            background-image: url(''http://www.petaccent.com/Content/Images/welcome-image-trans.jpg'');
            background-position: center center;
            background-repeat: no-repeat;">

        <div style="height: 40px; min-height: 40px;             
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54); display: block;"></div>

        <div style="padding: 20px;">
            <img src="http://www.petaccent.com/Content/Images/heart_pets_trans_sm6.png" />
            <div style="top: 30%; padding-right: 10px; margin-bottom: 40px; margin-top: 60px;">
                Hi, #####!<br /><br />
                Here is your password. If you need to change your password please log into <a href="https://www.petaccent.com">https://www.petaccent.com</a> and navigate to your profile. If you did not request 
                your password please contact us at <a href="mailto:CustomerService@PetAccent.com">CustomerService@PetAccent.com</a> so that we can take action. Thank you!<br /><br />

                %%%%%<br /><br />

                Sincerely,<br />
                Customer Service, PetAccent.com
            </div>
        </div>
        <div style="height: 25px; min-height: 25px; margin-top: 40px;
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54); display: block; position: fixed; bottom: 0;">
        </div>
    </div>
</body>
</html>', N'Hi, #####!\n\n
                Here is your password. If you need to change your password please log into https://www.petaccent.com and navigate to your profile. If you did not request 
                your password please contact us at <a href="mailto:CustomerService@PetAccent.com">CustomerService@PetAccent.com</a> so that we can take action. Thank you!\n\n

                %%%%%\n\n

                Sincerely,\n
                Customer Service, PetAccent.com', N'CustomerService@PetAccent.com', N'Customer Service - Pet Accent!', CAST(N'2017-04-25T12:25:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[EmailTemplate] OFF
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (1, N'Created')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (2, N'Processing')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (3, N'Processed')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (4, N'Shipped')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (5, N'Cancelled')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (6, N'Returned')
INSERT [dbo].[OrderStatus] ([ID], [Status]) VALUES (7, N'Chargeback')
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
SET IDENTITY_INSERT [dbo].[PaymentMethod] ON 

INSERT [dbo].[PaymentMethod] ([ID], [Method]) VALUES (1, N'CreditCard')
INSERT [dbo].[PaymentMethod] ([ID], [Method]) VALUES (2, N'ACH')
SET IDENTITY_INSERT [dbo].[PaymentMethod] OFF
