﻿using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using BusinessLayer.Services;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SalesTemplate.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static EmailBlastService.Models.AwsFeedbackModel;

namespace EmailBlastService
{
    public class Program
    {
        private static readonly string _aws_SQS_Deliveries = ConfigurationManager.AppSettings["AWS_SQS_Deliveries"];
        private static readonly string _aws_SQS_Complaints = ConfigurationManager.AppSettings["AWS_SQS_Complaints"];
        private static readonly string _aws_SQS_Bounces = ConfigurationManager.AppSettings["AWS_SQS_Bounces"];
        private static AmazonSQSClient _awsClient;

        private int _deliveryCount;
        private int _complaintCount;
        private int _bounceCount;

        static void Main(string[] args)
        {
            bool goShip = false;
            bool goBlast = true;
            bool goClean = false;

            bool blastTest = false;

            ////////
            // Shipping Confirmation email blast
            ////////

            if (goShip)
            {
                var userSvc = new UserService();
                var emailSvc = new EmailService();
                
                var recList = userSvc.GetShippedOrders().Result;

                foreach (var recipient in recList)
                {
                    if (recipient.User != null)
                    { 
                        var user = userSvc.GetUserByID(recipient.User.ID);

                        if (user.Result == null)
                        {
                            var nUser = new DataLayer.Data.Entities.User()
                            {
                                EmailAddress = recipient.EmailAddress,
                                FirstName = recipient.ShippingName
                            };

                            emailSvc.SendShippingEmail(nUser, recipient.ID).Wait();
                        }
                        else
                        {
                            emailSvc.SendShippingEmail(user.Result, recipient.ID).Wait();
                        }
                    }
                    else
                    {
                        var nUser = new DataLayer.Data.Entities.User()
                        {
                            EmailAddress = recipient.EmailAddress,
                            FirstName = recipient.ShippingName
                        };

                        emailSvc.SendShippingEmail(nUser, recipient.ID).Wait();
                    }
                }
            }

            ////////
            // Send email blast
            ////////

            if (goBlast)
            {
                string EmailBlastName = "Blast1";

                if (blastTest)
                {
                    string html = "";
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("Authentication", "JoelD31:73jkSfjDs8yrHweEo65iSo498fj#w29dfj");
                        var response = client.GetAsync("http://localhost:64975/Emails/" + EmailBlastName).Result;

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            html = response.Content.ReadAsStringAsync().Result;
                        }
                    }

                    User user = new User();
                    EmailTemplate template = new EmailTemplate();
                    user.EmailAddress = "service.petaccent@gmail.com";
                    user.FirstName = "Joel";
                    user.LastName = "Dunn";
                    template.Html = html;
                    template.Subject = "Starting off with a BANG!";
                    template.FromDisplayName = "Pet Accent!";
                    template.FromAddress = "notifications@petaccent.com";

                    var result = EmailServiceProject.OutgoingMail.SendHttpEmail(user, template, true,
                        EncryptionService.Encrypt(DateTime.Now.ToString("yyyyMMddhhmmssfff") + "PetAccent.com####" + user.EmailAddress)).Result;
                    var complete = result.HttpStatusCode == System.Net.HttpStatusCode.OK ? 1 : 0;
                }
                else
                {
                    var userSvc = new UserService();
                    var emailSvc = new EmailService();
                    var templateSvc = new EmailTemplateService();

                    var recList = userSvc.GetUsersEmailBlastSubscribed().Result;
                    var emailList = emailSvc.GetEmailAddressList().Result;

                    string html = "";
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("Authentication", "JoelD311:73jkSfjDs8yrHweEo65iSo498fj#w29dfj");
                        var response = client.GetAsync("https://www.petaccent.com/Emails/" + EmailBlastName).Result;

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            html = response.Content.ReadAsStringAsync().Result;
                        }
                    }

                    User user = new User();
                    EmailTemplate template = new EmailTemplate();
                    template.Html = html;
                    template.Subject = "Starting off with a BANG!";
                    template.FromDisplayName = "Pet Accent!";
                    template.FromAddress = "notifications@petaccent.com";

                    foreach (var item in emailList)
                    {
                        user.EmailAddress = item;
                        var result = EmailServiceProject.OutgoingMail.SendHttpEmail(user, template, true,
                            EncryptionService.Encrypt(DateTime.Now.ToString("yyyyMMddhhmmssfff") + "PetAccent.com####" + user.EmailAddress)).Result;
                        var complete = result.HttpStatusCode == System.Net.HttpStatusCode.OK ? 1 : 0;
                    }

                    foreach (var recipient in recList)
                    {
                        user.EmailAddress = recipient.EmailAddress;
                        var result = EmailServiceProject.OutgoingMail.SendHttpEmail(recipient, template, true,
                            EncryptionService.Encrypt(DateTime.Now.ToString("yyyyMMddhhmmssfff") + "PetAccent.com####" + recipient.EmailAddress)).Result;
                        var complete = result.HttpStatusCode == System.Net.HttpStatusCode.OK ? 1 : 0;
                    }
                }
            }
            
            /////////
            // Unsubscribe Bounces and Complaints
            /////////

            if (goClean)
            {
                Console.WriteLine("Begin handling email feedback...");
                Program p = new Program();
                p.UnsubscribeBouncesAndComplaints();
            }
        }

        public void UnsubscribeBouncesAndComplaints()
        {
            var batchDel = new List<DeleteMessageBatchRequestEntry>();

            try
            {
                // DELIVERIES
                _awsClient = new AmazonSQSClient(ConfigurationManager.AppSettings["AWSAccessKey"],
                    ConfigurationManager.AppSettings["AWSSecretKey"], Amazon.RegionEndpoint.USWest2);

                for (int i = 0; i < (Convert.ToInt32(ConfigurationManager.AppSettings["AWS_SQS_GetMessageCount"]) / 10); i++)
                {
                    var msgs = GetAWSSQSMessages(_aws_SQS_Deliveries);

                    if (msgs.Count > 0)
                    {
                        _deliveryCount += msgs.Count;

                        foreach (var message in msgs)
                        {
                            batchDel.Add(new DeleteMessageBatchRequestEntry()
                            {
                                Id = message.MessageId,
                                ReceiptHandle = message.ReceiptHandle
                            });
                        }

                        DeleteBatch(batchDel, _aws_SQS_Deliveries);
                    }
                    else
                        break;
                }

                Console.WriteLine("TOTAL DELIVERIES: " + _deliveryCount);
            }
            catch (Exception ex)
            {
                // 
            }

            try
            {
                // COMPLAINTS
                _awsClient = new AmazonSQSClient(ConfigurationManager.AppSettings["AWSAccessKey"],
                    ConfigurationManager.AppSettings["AWSSecretKey"], Amazon.RegionEndpoint.USWest2);

                for (int i = 0; i < (Convert.ToInt32(ConfigurationManager.AppSettings["AWS_SQS_GetMessageCount"]) / 10); i++)
                {
                    var msgs = GetAWSSQSMessages(_aws_SQS_Complaints);

                    if (msgs.Count > 0)
                    {
                        _complaintCount += msgs.Count;
                        UnsubscribeFeedback(msgs, _aws_SQS_Complaints);
                    }                        
                    else
                        break;
                }

                Console.WriteLine("TOTAL COMPLAINTS: " + _complaintCount);
            }
            catch
            {
                // 
            }

            try
            {
                // BOUNCES
                _awsClient = new AmazonSQSClient(ConfigurationManager.AppSettings["AWSAccessKey"],
                    ConfigurationManager.AppSettings["AWSSecretKey"], Amazon.RegionEndpoint.USWest2);

                for (int i = 0; i < (Convert.ToInt32(ConfigurationManager.AppSettings["AWS_SQS_GetMessageCount"]) / 10); i++)
                {
                    var msgs = GetAWSSQSMessages(_aws_SQS_Bounces);

                    if (msgs.Count > 0)
                    {
                        _bounceCount += msgs.Count;
                        UnsubscribeFeedback(msgs, _aws_SQS_Bounces);
                    }                        
                    else
                        break;
                }

                Console.WriteLine("TOTAL BOUNCES: " + _bounceCount);
            }
            catch
            {
                // 
            }
        }

        private void UnsubscribeFeedback(List<Amazon.SQS.Model.Message> messages, string url)
        {
            var userSvc = new UserService();
            var emailSvc = new EmailService();
            var batchDel = new List<DeleteMessageBatchRequestEntry>();

            foreach (var message in messages)
            {
                dynamic notification = null;
                var outer = JsonConvert.DeserializeObject<AmazonSqsNotification>(message.Body);
                var jobject = JObject.Parse(outer.Message);

                if (jobject["notificationType"].ToString().ToLower() == "bounce")
                    notification = JsonConvert.DeserializeObject<AmazonSesBounceNotification>(outer.Message);
                else if (jobject["notificationType"].ToString().ToLower() == "complaint")
                    notification = JsonConvert.DeserializeObject<AmazonSesComplaintNotification>(outer.Message);

                if (notification != null && notification.NotificationType.ToLower() == "bounce")
                {
                    if (notification.Bounce.BounceType == "Permanent")
                    {
                        foreach (var recipient in notification.Bounce.BouncedRecipients)
                        {
                            var user = userSvc.GetUserByEmailAddress(GetEmailAddress(recipient.EmailAddress));

                            if (user != null)
                            {
                                user.EmailSubscribed = false;
                                userSvc.UpdateUser(user);                                
                            }

                            emailSvc.DeleteEmailRequestConversion(GetEmailAddress(recipient.EmailAddress));
                        }
                    }

                    batchDel.Add(new DeleteMessageBatchRequestEntry()
                    {
                        Id = message.MessageId,
                        ReceiptHandle = message.ReceiptHandle
                    });
                }
                else if (notification != null && notification.NotificationType.ToLower() == "complaint")
                {
                    foreach (var recipient in notification.Complaint.ComplainedRecipients)
                    {
                        var user = userSvc.GetUserByEmailAddress(GetEmailAddress(recipient.EmailAddress)).ConfigureAwait(false);

                        if (user != null)
                        {
                            user.EmailSubscribed = false;
                            userSvc.UpdateUser(user);
                        }
                    }

                    batchDel.Add(new DeleteMessageBatchRequestEntry()
                    {
                        Id = message.MessageId,
                        ReceiptHandle = message.ReceiptHandle
                    });
                }
            }

            DeleteBatch(batchDel, url);
        }

        private List<Amazon.SQS.Model.Message> GetAWSSQSMessages(string queueUrl)
        {
            var request = new ReceiveMessageRequest
            {
                AttributeNames = { "All" },
                MaxNumberOfMessages = 10,
                QueueUrl = queueUrl,
                VisibilityTimeout = (int)TimeSpan.FromMinutes(10).TotalSeconds,
                WaitTimeSeconds = (int)TimeSpan.FromSeconds(5).TotalSeconds
            };

            return _awsClient.ReceiveMessage(request).Messages;
        }

        private void DeleteBatch(List<DeleteMessageBatchRequestEntry> batch, string queueUrl)
        {
            if (batch != null)
                _awsClient.DeleteMessageBatch(new DeleteMessageBatchRequest(queueUrl, batch));
        }

        private string GetEmailAddress(string email)
        {
            string strRegex = @"[A-Za-z0-9_\-\+\.]+@[A-Za-z0-9\-\.]+\.([A-Za-z]{2,15})(?:\.[a-z]{2})?";
            Regex myRegex = new Regex(strRegex, RegexOptions.None);
            var matches = myRegex.Matches(email);

            if (matches.Count == 1)
                return matches[0].Value;
            else
                return email;
        }
    }
}
