﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using System.Linq;

namespace DataLayer.Repositories
{
    public class CampaignRepository : BaseRepository
    {
        public async Task<Campaign> GetCampaignByName(string name)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Campaigns.Where(x => x.Name == name)
                                          .FirstOrDefaultAsync()
                                          .ConfigureAwait(false);
            }
        }
    }
}
