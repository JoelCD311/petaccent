﻿using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using System;
using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace DataLayer.Repositories
{
    public class StoreCreditRepository : BaseRepository
    {
        public async Task<decimal> GetTotalAvailableCredits(int userID)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.StoreCredits
                                .Where(x => x.UserID == userID &&
                                       (x.Expires >= DateTime.Now || x.Expires == null))
                                .SumAsync(x => (decimal?)(x.Amount))
                                .ConfigureAwait(false) ?? 0;
            }            
        }

        public async Task WriteStoreCreditGrantedRecord(int? userID, int? typeID, decimal amount, string descr, string notes, string email = null, string couponCode = null)
        {
            using (var _db = new SalesTemplateContext())
            {
                _db.StoreCredits.Add(new Data.Entities.StoreCredit()
                {
                    Amount = amount,
                    DateCreated = DateTime.Now,
                    Description = descr,
                    Expires = null,
                    EmailAddress = email,
                    Notes = notes,
                    TypeID = typeID,
                    UserID = userID,
                    CouponCode = couponCode
                });
                try
                {
                    await _db.SaveChangesAsync().ConfigureAwait(false);
                }
                catch { }
            }
        }

        public async Task WriteStoreCreditUsedRecord(int userID, int? typeID, decimal amount, string descr, string notes, string coupon)
        {
            using (var _db = new SalesTemplateContext())
            {
                _db.StoreCredits.Add(new Data.Entities.StoreCredit()
                {
                    Amount = amount * (-1),
                    DateCreated = DateTime.Now,
                    Description = descr,
                    Expires = null,
                    Notes = notes,
                    TypeID = null,
                    UserID = userID,
                    CouponCode = coupon
                });
                try
                {
                    await _db.SaveChangesAsync().ConfigureAwait(false);
                }
                catch { }                
            }
        }

        public async Task WriteStoreCreditUsedRecord(List<StoreCredit> coupons)
        {
            if (coupons.Count > 0)
            {
                using (var _db = new SalesTemplateContext())
                {
                    _db.StoreCredits.AddRange(coupons);

                    try
                    {
                        await _db.SaveChangesAsync().ConfigureAwait(false);
                    }
                    catch { }
                }
            }
        }

        public async Task<StoreCreditType> GetStoreCreditTypeByName(string name)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.StoreCreditTypes
                                .Where(x => x.Type == name)
                                .FirstOrDefaultAsync()
                                .ConfigureAwait(false);
            }
        }

        public async Task<StoreCredit> CheckCouponCode(string coupon)
        {
            using (var _db = new SalesTemplateContext())
            {
                var coupons = _db.StoreCredits.Where(x => x.CouponCode == coupon).ToList();

                if (coupons.Sum(x => x.Amount) > 0)
                {
                    return await _db.StoreCredits
                                    .Where(x => x.CouponCode == coupon && x.Amount > 0)
                                    .FirstOrDefaultAsync()
                                    .ConfigureAwait(false);
                }
            }

            return null;
        }

        public async Task<bool> IsCouponCodeUnique(string code)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.StoreCredits
                                .Where(x => x.CouponCode == code)
                                .AnyAsync()
                                .ConfigureAwait(false);
            }
        }
    }
}
