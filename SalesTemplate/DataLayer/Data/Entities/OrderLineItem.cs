﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class OrderLineItem
    {
        public int ID { get; set; }
        [ForeignKey("Order")]
        public int OrderID { get; set; }
        [ForeignKey("Product")]
        public int ProductID { get; set; }
        [ForeignKey("ProductOption")]
        public int? ProductOptionID { get; set; }
        public int Quantity { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal SubTotal { get; set; }
        public System.DateTime DateCreated { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductOption ProductOption { get; set; }

        [NotMapped]
        public List<ProductOption> ProductOptions { get; set; }
    }
}
