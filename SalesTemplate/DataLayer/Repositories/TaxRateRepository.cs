﻿using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;

namespace DataLayer.Repositories
{
    public class TaxRateRepository : BaseRepository
    {
        public TaxRateRepository()
        {
            _db = new SalesTemplateContext();
        }

        public async Task<TaxRate> GetTaxRateByState(string state)
        {
            return await _db.TaxRates
                            .Where(x => x.State == state)
                            .SingleOrDefaultAsync()
                            .ConfigureAwait(false);
        }
    }
}
