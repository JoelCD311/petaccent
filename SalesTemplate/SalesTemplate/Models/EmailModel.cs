﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesTemplate.Models
{
    public class EmailModel : BaseModel
    {
        public EmailModel(bool populateBase = false) : base(populateBase) { }
        public EmailTemplate EmailTemplate { get; set; }
        public string Html { get; set; }
        public bool Promotion { get; set; }
        public string Password { get; set; }
        public string ReferralName { get; set; }
        public string ReferralEmail { get; set; }
        public Product Product { get; set; }
    }
}