USE [master]
GO
/****** Object:  Database [TruNoviceDB]    Script Date: 5/1/2017 12:02:36 PM ******/
CREATE DATABASE [TruNoviceDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TruNoviceDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\TruNoviceDB.mdf' , SIZE = 24576KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TruNoviceDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\TruNoviceDB_log.ldf' , SIZE = 84416KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TruNoviceDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TruNoviceDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TruNoviceDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TruNoviceDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TruNoviceDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TruNoviceDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TruNoviceDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [TruNoviceDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TruNoviceDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TruNoviceDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TruNoviceDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TruNoviceDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TruNoviceDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TruNoviceDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TruNoviceDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TruNoviceDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TruNoviceDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TruNoviceDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TruNoviceDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TruNoviceDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TruNoviceDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TruNoviceDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TruNoviceDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TruNoviceDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TruNoviceDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TruNoviceDB] SET  MULTI_USER 
GO
ALTER DATABASE [TruNoviceDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TruNoviceDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TruNoviceDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TruNoviceDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [TruNoviceDB]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Street1] [varchar](50) NOT NULL,
	[Street2] [varchar](50) NULL,
	[Street3] [varchar](50) NULL,
	[City] [varchar](50) NOT NULL,
	[State] [varchar](2) NOT NULL,
	[PostalCode] [varchar](10) NOT NULL,
	[Country] [varchar](2) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](20) NOT NULL,
 CONSTRAINT [PK_AddressType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Campaign](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[Agency] [varchar](50) NULL,
	[DailyBudget] [decimal](18, 2) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[TotalDaysRun] [int] NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Charity]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Charity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CharityName] [varchar](100) NOT NULL,
	[ContactFirstName] [varchar](50) NOT NULL,
	[ContactLastName] [varchar](50) NOT NULL,
	[MailingStreet1] [varchar](50) NOT NULL,
	[MailingStreet2] [varchar](50) NULL,
	[MailingStreet3] [varchar](50) NULL,
	[MailingCity] [varchar](50) NOT NULL,
	[MailingState] [varchar](2) NOT NULL,
	[MailingPostalCode] [varchar](10) NOT NULL,
	[MailingCountry] [varchar](2) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Charity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailRequestConversion]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailRequestConversion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](20) NOT NULL,
	[EmailAddress] [varchar](100) NOT NULL,
	[CreditAwarded] [decimal](18, 2) NOT NULL,
	[AccountCreated] [bit] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_EmailRequestConversion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[IPAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Subject] [varchar](100) NOT NULL,
	[Html] [varchar](max) NOT NULL,
	[PlainText] [varchar](max) NULL,
	[FromAddress] [varchar](100) NOT NULL,
	[FromDisplayName] [varchar](100) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[EmailAddress] [varchar](100) NOT NULL,
	[ShippingName] [varchar](100) NOT NULL,
	[ShippingStreet1] [varchar](50) NOT NULL,
	[ShippingStreet2] [varchar](50) NULL,
	[ShippingStreet3] [varchar](50) NULL,
	[ShippingCity] [varchar](50) NOT NULL,
	[ShippingState] [varchar](2) NOT NULL,
	[ShippingPostalCode] [varchar](10) NOT NULL,
	[ShippingCountry] [varchar](2) NOT NULL,
	[OrderSubTotal] [decimal](18, 2) NOT NULL,
	[StoreCreditsApplied] [decimal](18, 2) NOT NULL,
	[TaxTotal] [decimal](18, 2) NOT NULL,
	[ShippingCharge] [decimal](18, 2) NOT NULL,
	[OrderTotal] [decimal](18, 2) NOT NULL,
	[TotalToDonate] [decimal](18, 2) NOT NULL,
	[CharityID] [int] NOT NULL,
	[ShippingCost] [decimal](18, 2) NOT NULL,
	[ShippedDate] [smalldatetime] NULL,
	[TrackingNumber] [varchar](200) NULL,
	[PaymentMethodID] [int] NOT NULL,
	[OrderStatusID] [int] NOT NULL,
	[OrderDate] [smalldatetime] NOT NULL,
	[IsGift] [bit] NOT NULL,
	[PaymentConfirmation] [varchar](200) NOT NULL,
	[OrderID] [varchar](15) NOT NULL,
	[CampaignID] [int] NULL,
	[IPAddress] [varchar](20) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderLineItem]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLineItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TaxTotal] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_OrderLineItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderLineItemOption]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLineItemOption](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderLineItemID] [int] NOT NULL,
	[ProductOptionID] [int] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_OrderLineItemOptions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Method] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SKU] [varchar](50) NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[Price] [decimal](18, 2) NULL,
	[Cost] [decimal](18, 2) NULL,
	[Taxable] [bit] NOT NULL,
	[ShippingCharge] [decimal](18, 2) NULL,
	[ShippingCost] [decimal](18, 2) NULL,
	[QuantityAvailable] [int] NULL,
	[Active] [bit] NOT NULL,
	[Featured] [bit] NOT NULL,
	[Order] [int] NULL,
	[ImagePath] [varchar](500) NULL,
	[DomainID] [int] NOT NULL,
	[Category] [varchar](50) NULL,
	[SubCategory] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductDomain]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDomain](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_ProductDomain] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductOption]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductOption](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentOptionID] [int] NULL,
	[ProductID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[QuantityAvailable] [int] NULL,
	[Price] [decimal](18, 2) NULL,
	[ShortValue] [varchar](10) NULL,
	[ImagePath] [varchar](500) NULL,
 CONSTRAINT [PK_ProductOption] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StoreCredit]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreCredit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeID] [int] NULL,
	[UserID] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Notes] [varchar](500) NULL,
	[Expires] [smalldatetime] NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_StoreCredit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StoreCreditType]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreCreditType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_StoreCreditType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_Country]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Country] [nchar](2) NOT NULL,
	[CountryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_sys_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Country] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Country] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CountryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CountryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_State]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_State](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[State] [nchar](2) NOT NULL,
	[StateName] [nvarchar](50) NOT NULL,
	[Country] [int] NOT NULL,
 CONSTRAINT [PK_sys_State] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[StateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[StateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[State] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[State] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaxRate]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaxRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[State] [varchar](2) NOT NULL,
	[Rate] [decimal](4, 3) NOT NULL,
	[Type] [varchar](15) NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_TaxRates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 5/1/2017 12:02:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[EmailAddress] [varchar](100) NOT NULL,
	[Password] [varchar](1000) NOT NULL,
	[Salt] [uniqueidentifier] NOT NULL,
	[Phone] [numeric](18, 0) NULL,
	[Active] [bit] NOT NULL,
	[EmailSubscribed] [bit] NOT NULL,
	[PreferredCharityID] [int] NULL,
	[PaymentProcessorID] [varchar](50) NULL,
	[DateCreated] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_EmailRequestConversion_EmailAddress]    Script Date: 5/1/2017 12:02:36 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_EmailRequestConversion_EmailAddress] ON [dbo].[EmailRequestConversion]
(
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_EmailRequestConversion_IPAddress]    Script Date: 5/1/2017 12:02:36 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_EmailRequestConversion_IPAddress] ON [dbo].[EmailRequestConversion]
(
	[IPAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_EmailRequestConversion_IPEmail]    Script Date: 5/1/2017 12:02:36 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_EmailRequestConversion_IPEmail] ON [dbo].[EmailRequestConversion]
(
	[IPAddress] ASC,
	[EmailAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Order_OrderID]    Script Date: 5/1/2017 12:02:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_Order_OrderID] ON [dbo].[Order]
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_AddressType] FOREIGN KEY([Type])
REFERENCES [dbo].[AddressType] ([ID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_AddressType]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_UserID]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_CharityID] FOREIGN KEY([CharityID])
REFERENCES [dbo].[Charity] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_CharityID]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY([OrderStatusID])
REFERENCES [dbo].[OrderStatus] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PaymentMethod] FOREIGN KEY([PaymentMethodID])
REFERENCES [dbo].[PaymentMethod] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PaymentMethod]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_UserID]
GO
ALTER TABLE [dbo].[OrderLineItem]  WITH CHECK ADD  CONSTRAINT [FK_OrderLineItem_OrderID] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([ID])
GO
ALTER TABLE [dbo].[OrderLineItem] CHECK CONSTRAINT [FK_OrderLineItem_OrderID]
GO
ALTER TABLE [dbo].[OrderLineItem]  WITH CHECK ADD  CONSTRAINT [FK_OrderLineItem_ProductID] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[OrderLineItem] CHECK CONSTRAINT [FK_OrderLineItem_ProductID]
GO
ALTER TABLE [dbo].[OrderLineItemOption]  WITH CHECK ADD  CONSTRAINT [FK_OrderLineItemOption_OrderLineItemID] FOREIGN KEY([OrderLineItemID])
REFERENCES [dbo].[OrderLineItem] ([ID])
GO
ALTER TABLE [dbo].[OrderLineItemOption] CHECK CONSTRAINT [FK_OrderLineItemOption_OrderLineItemID]
GO
ALTER TABLE [dbo].[OrderLineItemOption]  WITH CHECK ADD  CONSTRAINT [FK_OrderLineItemOption_ProductOptionID] FOREIGN KEY([ProductOptionID])
REFERENCES [dbo].[ProductOption] ([ID])
GO
ALTER TABLE [dbo].[OrderLineItemOption] CHECK CONSTRAINT [FK_OrderLineItemOption_ProductOptionID]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductDomain] FOREIGN KEY([DomainID])
REFERENCES [dbo].[ProductDomain] ([ID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductDomain]
GO
ALTER TABLE [dbo].[ProductOption]  WITH CHECK ADD  CONSTRAINT [FK_ProductOption_ProductID] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[ProductOption] CHECK CONSTRAINT [FK_ProductOption_ProductID]
GO
ALTER TABLE [dbo].[StoreCredit]  WITH CHECK ADD  CONSTRAINT [FK_StoreCredit_Type] FOREIGN KEY([TypeID])
REFERENCES [dbo].[StoreCreditType] ([ID])
GO
ALTER TABLE [dbo].[StoreCredit] CHECK CONSTRAINT [FK_StoreCredit_Type]
GO
ALTER TABLE [dbo].[StoreCredit]  WITH CHECK ADD  CONSTRAINT [FK_StoreCredit_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[StoreCredit] CHECK CONSTRAINT [FK_StoreCredit_UserID]
GO
ALTER TABLE [dbo].[sys_State]  WITH CHECK ADD  CONSTRAINT [FK_sys_State_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[sys_Country] ([ID])
GO
ALTER TABLE [dbo].[sys_State] CHECK CONSTRAINT [FK_sys_State_Country]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_PreferredCharity] FOREIGN KEY([PreferredCharityID])
REFERENCES [dbo].[Charity] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_PreferredCharity]
GO
USE [master]
GO
ALTER DATABASE [TruNoviceDB] SET  READ_WRITE 
GO
