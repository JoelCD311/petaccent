-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: SalesTemplate
-- Source Schemata: SalesTemplate
-- Created: Wed Feb 08 21:58:18 2017
-- Workbench Version: 6.3.8
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema SalesTemplate
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `SalesTemplate` ;
CREATE SCHEMA IF NOT EXISTS `SalesTemplate` ;

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.Address
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`Address` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `UserID` INT NOT NULL,
  `Type` INT NOT NULL,
  `Street1` VARCHAR(50) NOT NULL,
  `Street2` VARCHAR(50) NULL,
  `Street3` VARCHAR(50) NULL,
  `City` VARCHAR(50) NOT NULL,
  `State` VARCHAR(2) NOT NULL,
  `PostalCode` VARCHAR(10) NOT NULL,
  `Country` VARCHAR(2) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_Address_AddressType`
    FOREIGN KEY (`Type`)
    REFERENCES `SalesTemplate`.`AddressType` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Address_UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `SalesTemplate`.`User` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.AddressType
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`AddressType` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.Charity
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`Charity` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `CharityName` VARCHAR(100) NOT NULL,
  `ContactFirstName` VARCHAR(50) NOT NULL,
  `ContactLastName` VARCHAR(50) NOT NULL,
  `MailingStreet1` VARCHAR(50) NOT NULL,
  `MailingStreet2` VARCHAR(50) NULL,
  `MailingStreet3` VARCHAR(50) NULL,
  `MailingCity` VARCHAR(50) NOT NULL,
  `MailingState` VARCHAR(2) NOT NULL,
  `MailingPostalCode` VARCHAR(10) NOT NULL,
  `MailingCountry` VARCHAR(2) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.EmailTemplate
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`EmailTemplate` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(50) NOT NULL,
  `Subject` VARCHAR(100) NOT NULL,
  `Html` LONGTEXT NOT NULL,
  `FromAddress` VARCHAR(100) NOT NULL,
  `FromDisplayName` VARCHAR(100) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.Order
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`Order` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `UserID` INT NULL,
  `EmailAddress` VARCHAR(50) NOT NULL,
  `ShippingName` VARCHAR(100) NOT NULL,
  `ShippingStreet1` VARCHAR(50) NOT NULL,
  `ShippingStreet2` VARCHAR(50) NULL,
  `ShippingStreet3` VARCHAR(50) NULL,
  `ShippingCity` VARCHAR(50) NOT NULL,
  `ShippingState` VARCHAR(2) NOT NULL,
  `ShippingPostalCode` VARCHAR(10) NOT NULL,
  `ShippingCountry` VARCHAR(2) NOT NULL,
  `OrderSubTotal` DECIMAL(18,2) NOT NULL,
  `StoreCreditsApplied` DECIMAL(18,2) NOT NULL,
  `TaxTotal` DECIMAL(18,2) NOT NULL,
  `ShippingCharge` DECIMAL(18,2) NOT NULL,
  `TotalToDonate` DECIMAL(18,2) NOT NULL,
  `OrderTotal` DECIMAL(18,2) NOT NULL,
  `CharityID` INT NOT NULL,
  `ShippingCost` DECIMAL(18,2) NOT NULL,
  `ShippedDate` DATETIME NULL,
  `TrackingNumber` VARCHAR(200) NULL,
  `PaymentMethodID` INT NOT NULL,
  `OrderStatusID` INT NOT NULL,
  `OrderDate` DATETIME NOT NULL,
  `PaymentConfirmation` VARCHAR(200) NOT NULL,
  `OrderID` VARCHAR(15) NOT NULL,
  `IPAddress` VARCHAR(15) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `AK_OrderID` (`OrderID` ASC),
  UNIQUE INDEX `IX_OrderID` (`OrderID` ASC),
  CONSTRAINT `FK_Order_CharityID`
    FOREIGN KEY (`CharityID`)
    REFERENCES `SalesTemplate`.`Charity` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Order_OrderStatus`
    FOREIGN KEY (`OrderStatusID`)
    REFERENCES `SalesTemplate`.`OrderStatus` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Order_PaymentMethod`
    FOREIGN KEY (`PaymentMethodID`)
    REFERENCES `SalesTemplate`.`PaymentMethod` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Order_UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `SalesTemplate`.`User` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.OrderLineItem
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`OrderLineItem` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `OrderID` INT NOT NULL,
  `ProductID` INT NOT NULL,
  `Quantity` INT NOT NULL,
  `TaxTotal` DECIMAL(18,2) NOT NULL,
  `SubTotal` DECIMAL(18,2) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_OrderLineItem_OrderID`
    FOREIGN KEY (`OrderID`)
    REFERENCES `SalesTemplate`.`Order` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_OrderLineItem_ProductID`
    FOREIGN KEY (`ProductID`)
    REFERENCES `SalesTemplate`.`Product` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.OrderStatus
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`OrderStatus` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Status` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.PaymentMethod
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`PaymentMethod` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Method` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.Product
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`Product` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `SKU` VARCHAR(50) NULL,
  `Name` VARCHAR(50) NOT NULL,
  `Description` VARCHAR(500) NULL,
  `Price` DECIMAL(18,2) NOT NULL,
  `Cost` DECIMAL(18,2) NOT NULL,
  `Taxable` TINYINT(1) NOT NULL,
  `ShippingCharge` DECIMAL(18,2) NULL,
  `ShippingCost` DECIMAL(18,2) NULL,
  `QuantityAvailable` INT NOT NULL,
  `Active` TINYINT(1) NOT NULL,
  `Featured` TINYINT(1) NOT NULL,
  `ImagePath` VARCHAR(500) NULL,
  `DomainID` INT NOT NULL,
  `Category` VARCHAR(50) NULL,
  `SubCategory` VARCHAR(50) NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_Product_ProductDomain`
    FOREIGN KEY (`DomainID`)
    REFERENCES `SalesTemplate`.`ProductDomain` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.ProductDomain
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`ProductDomain` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(50) NOT NULL,
  `Description` VARCHAR(500) NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.StoreCredit
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`StoreCredit` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `TypeID` INT NOT NULL,
  `UserID` INT NOT NULL,
  `Amount` DECIMAL(18,2) NOT NULL,
  `Description` VARCHAR(500) NOT NULL,
  `Notes` VARCHAR(500) NULL,
  `Expires` DATETIME NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_StoreCredit_Type`
    FOREIGN KEY (`TypeID`)
    REFERENCES `SalesTemplate`.`StoreCreditType` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_StoreCredit_UserID`
    FOREIGN KEY (`UserID`)
    REFERENCES `SalesTemplate`.`User` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.StoreCreditType
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`StoreCreditType` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.sys_Country
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`sys_Country` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Country` CHAR(2) CHARACTER SET 'utf8mb4' NOT NULL,
  `CountryName` VARCHAR(50) CHARACTER SET 'utf8mb4' NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `UQ__sys_Coun__E056F201FFC93395` (`CountryName` ASC),
  UNIQUE INDEX `UQ__sys_Coun__067B3009780D94F8` (`Country` ASC));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.sys_State
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`sys_State` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `State` CHAR(2) CHARACTER SET 'utf8mb4' NOT NULL,
  `StateName` VARCHAR(50) CHARACTER SET 'utf8mb4' NOT NULL,
  `Country` INT NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `UQ__sys_Stat__BA803DADF2CF5A6D` (`State` ASC),
  UNIQUE INDEX `UQ__sys_Stat__5547631562F22801` (`StateName` ASC),
  CONSTRAINT `FK_sys_State_Country`
    FOREIGN KEY (`Country`)
    REFERENCES `SalesTemplate`.`sys_Country` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.TaxRate
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`TaxRate` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `State` VARCHAR(2) NOT NULL,
  `Rate` DECIMAL(4,3) NOT NULL,
  `Type` VARCHAR(15) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`));

-- ----------------------------------------------------------------------------
-- Table SalesTemplate.User
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `SalesTemplate`.`User` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(50) NOT NULL,
  `LastName` VARCHAR(50) NOT NULL,
  `EmailAddress` VARCHAR(100) NOT NULL,
  `Password` VARCHAR(1000) NOT NULL,
  `Salt` VARCHAR(64) UNIQUE NOT NULL,
  `Phone` BIGINT NULL,
  `Active` TINYINT(1) NOT NULL,
  `EmailSubscribed` TINYINT(1) NOT NULL,
  `PreferredCharityID` INT NULL,
  `PaymentProcessorID` VARCHAR(50) NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `UQ__User__49A147405F371BB0` (`EmailAddress` ASC),
  CONSTRAINT `FK_User_PreferredCharity`
    FOREIGN KEY (`PreferredCharityID`)
    REFERENCES `SalesTemplate`.`Charity` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
SET FOREIGN_KEY_CHECKS = 1;
