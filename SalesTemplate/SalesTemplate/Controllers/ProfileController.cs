﻿using BusinessLayer.Models;
using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Data.Entities;

namespace SalesTemplate.Controllers
{
    [Authorize]
    public class ProfileController : BaseController
    {
        private const string _initialPassword = "***********";

        public async Task<ActionResult> Index()
        {
            var charitySvc = new CharityService();
            var systemSvc = new SystemService();
            var userSvc = new UserService();
            var sessionSvc = new SessionService();
            var storeSvc = new StoreCreditService();
            var model = new UpdateProfileViewModel(true);

            var user = User;
            var carTask = charitySvc.GetCharities();
            var coTask = systemSvc.GetCountries();
            var statesTask = systemSvc.GetStatesByCountry(1); // USA default
            var userTask = userSvc.GetUserByID(user.UserID);
            var cartTask = sessionSvc.GetShoppingCart();
            var credTask = storeSvc.GetTotalAvailableCredits(user.UserID);

            await Task.WhenAll(carTask, coTask, statesTask, userTask, cartTask, credTask).ConfigureAwait(false);

            var cardTask = GetCreditCards(userTask.Result.PaymentProcessorID);            

            var userData = userTask.Result;
            var cart = cartTask.Result;

            model.FirstName = User.FirstName;
            model.States = statesTask.Result;
            model.Countries = coTask.Result;            
            model.Charities = carTask.Result;
            model.EmailAddress = userData.EmailAddress;
            model.EmailSubscribed = userData.EmailSubscribed;
            model.ProfileFirstName = userData.FirstName;
            model.LastName = userData.LastName;
            model.Phone = userData.Phone;
            model.Cards = cardTask.Result;
            model.ShippingAddress = userData.Addresses.Where(x => x.AddressType.Type == "Shipping").FirstOrDefault();
            model.PreferredCharityID = Convert.ToInt32(userData.PreferredCharityID) == 0 ? 1 : Convert.ToInt32(userData.PreferredCharityID);
            model.CartTotal = cart.Order.OrderLineItems.Sum(x => x.Quantity);
            model.TotalRewardsAndCredits = credTask.Result;
            model.Password = _initialPassword;
            model.ConfirmPassword = _initialPassword;            

            if (model.ShippingAddress == null)
            {
                model.ShippingAddress = new Address();
                model.ShippingAddress.Country = "US";
            }

            model.FirstName = User.FirstName;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateProfile(UpdateProfileViewModel model)
        {
            try
            {
                var userSvc = new UserService();
                var user = await userSvc.GetUserByID(User.UserID).ConfigureAwait(false);

                user.EmailAddress = model.EmailAddress;
                user.EmailSubscribed = model.EmailSubscribed;
                user.FirstName = model.ProfileFirstName;
                user.LastName = model.LastName;
                user.EmailSubscribed = model.EmailSubscribed;
                user.Phone = model.Phone;
                user.PreferredCharityID = model.PreferredCharityID;
                user.Addresses = await mapAddress(user.Addresses, model.ShippingAddress).ConfigureAwait(false);

                if (!String.IsNullOrEmpty(model.DefaultCard))
                {
                    var paymentSvc = new PaymentService();
                    await paymentSvc.UpdateDefaultCard(user.PaymentProcessorID, model.DefaultCard).ConfigureAwait(false);
                }

                if ((model.Password == model.ConfirmPassword) && (model.Password != _initialPassword))
                {
                    user.Salt = Guid.NewGuid();
                    user.Password = EncryptionService.Encrypt(model.Password, user.Salt.ToString());
                }

                var success = await userSvc.UpdateUser(user).ConfigureAwait(false);

                if (success)
                {
                    return Json(new {
                        Success = true,
                        Message = "Your profile has been successfully updated"
                    });
                }
            }
            catch { }

            return Json(new
            {
                Success = false,
                Message = "Profile could not be updated. Please try again later"
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AddCard(CreditCardModel model)
        {
            try
            {
                var returnModel = new UpdateProfileViewModel();
                var paymentSvc = new PaymentService();
                returnModel.Cards = await paymentSvc.AddCreditCard(model, this.User.UserID).ConfigureAwait(false);

                if (returnModel.Cards != null)
                {
                    return Json(new
                    {
                        Success = true,
                        Html = this.RenderRazorViewToString(this, "_ExistingCards", returnModel)
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Html = ex.Message
                });
            }

            return Json(new { Success = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RemoveCard(string id)
        {
            try
            {
                var paymentSvc = new PaymentService();
                return Json(new {
                    Success = await paymentSvc.RemoveCreditCard(id, this.User.UserID).ConfigureAwait(false)
                });
            }
            catch {
                return Json(new {
                    Success = false
                });
            }
        }

        private async Task<ICollection<Address>> mapAddress(ICollection<Address> addresses, Address newAddress)
        {
            var sysSvc = new SystemService();

            var oldAddress = addresses.Where(x => x.AddressType.Type == "Shipping").FirstOrDefault();
            if (oldAddress != null)
            {
                var address = addresses.Where(x => x.AddressType.Type == "Shipping").FirstOrDefault();

                address.City = newAddress.City;
                address.Country = newAddress.Country;
                address.PostalCode = newAddress.PostalCode;
                address.State = newAddress.State;
                address.Street1 = newAddress.Street1;
                address.Street2 = newAddress.Street2;
                address.Street3 = newAddress.Street3;

                addresses.Where(x => x.AddressType.Type == "Shipping").Select(x => { x = address; return x; });
            }
            else
            {                
                var addrType = await sysSvc.GetAddressTypeByName("Shipping").ConfigureAwait(false);
                                
                addresses.Add(new Address()
                {
                    Type = addrType.ID,
                    City = newAddress.City,
                    Country = newAddress.Country,
                    State = newAddress.State,
                    PostalCode = newAddress.PostalCode,
                    Street1 = newAddress.Street1,
                    Street2 = newAddress.Street2,
                    Street3 = newAddress.Street3,
                    DateCreated = DateTime.Now,
                    UserID = User.UserID,
                    New = true
                });
            }

            return addresses;
        }
    }
}