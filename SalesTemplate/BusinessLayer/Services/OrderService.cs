﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using DataLayer.Data.Models.ViewModels;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace BusinessLayer.Services
{
    public class OrderService
    {
        private OrderRepository _repo { get; set; }

        public OrderService()
        {
            _repo = new OrderRepository();
        }
        public async Task<string> PlaceOrder(ShoppingCart cart, List<StoreCredit> coupons, string paymentConfirmation)
        {
            if (!String.IsNullOrEmpty(paymentConfirmation))
            {
                var sessionSvc = new SessionService();
                var orderID = String.Empty;
                int? userID = 0;

                if (cart.Order.User != null)
                    userID = cart.Order.User.ID;
                else
                    userID = null;

                orderID = await _repo.CreateOrder(new DataLayer.Data.Entities.Order()
                {
                    UserID = userID,
                    EmailAddress = cart.Order.EmailAddress,
                    ShippingName = cart.Order.ShippingName,
                    ShippingStreet1 = cart.Order.ShippingStreet1,
                    ShippingStreet2 = cart.Order.ShippingStreet2,
                    ShippingStreet3 = cart.Order.ShippingStreet3,
                    ShippingCity = cart.Order.ShippingCity,
                    ShippingState = cart.Order.ShippingState,
                    ShippingPostalCode = cart.Order.ShippingPostalCode,
                    ShippingCountry = cart.Order.ShippingCountry,
                    OrderSubTotal = cart.Order.OrderSubTotal,
                    StoreCreditsApplied = (cart.Order.ApplyCredits ? cart.Order.StoreCreditsApplicable : 0),
                    StoreCreditID = cart.Order.StoreCreditID,
                    TaxTotal = cart.Order.TaxTotal,
                    ShippingCharge = cart.Order.ShippingCharge,
                    TotalToDonate = cart.Order.TotalToDonate,
                    OrderTotal = cart.Order.OrderTotal,
                    CharityID = cart.Order.CharityID,
                    ShippingCost = cart.Order.ShippingCost,
                    ShippedDate = null,
                    TrackingNumber = null,
                    PaymentMethodID = cart.Order.PaymentMethod.ID,
                    OrderStatusID = cart.Order.OrderStatusID,
                    OrderDate = DateTime.Now,
                    IsGift = cart.Order.IsGift,
                    PaymentConfirmation = paymentConfirmation,
                    IPAddress = cart.Order.IPAddress,
                    CampaignID = cart.Order.CampaignID,
                    DateCreated = DateTime.Now
                }, cart.Order.OrderLineItems).ConfigureAwait(false);

                if (orderID.Length > 0)
                {
                    HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => _completeOrder(cart, coupons));
                }

                return orderID;
            }
            return "";
        }
        public async Task<Order> GetOrder(int orderdbid)
        {
            return await _repo.GetOrder(orderdbid).ConfigureAwait(false);
        }
        public async Task<Order> GetConfirmationEmailOrder(int orderdbid)
        {
            return await _repo.GetConfirmationEmailOrder(orderdbid).ConfigureAwait(false);
        }
        public async Task<List<OrderLineItem>> GetOrderLineItems(int orderdbid)
        {
            return await _repo.GetOrderLineItems(orderdbid).ConfigureAwait(false);
        }
        private async Task _completeOrder(ShoppingCart cart, List<StoreCredit> coupons)
        {
            // Reduce quantity of order items in Product/ProductOption table
            var productSvc = new ProductService();

            foreach (var item in cart.Order.OrderLineItems)
            {
                await productSvc.UpdateProductQuantity(item.ProductID, 
                    item.ProductOptions.Select(x => x.ID).ToArray(), ((-1) * item.Quantity))
                    .ConfigureAwait(false);
            }

            var creditSvc = new StoreCreditService();

            coupons = coupons.Select(x => { x.Amount = x.Amount * -1; return x; }).ToList();

            // Apply coupons
            await creditSvc.WriteStoreCreditUsedRecord(coupons).ConfigureAwait(false);
        }
    }
}
