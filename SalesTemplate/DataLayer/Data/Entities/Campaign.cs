﻿using System;
using System.Collections.Generic;

namespace DataLayer.Data.Entities
{
    public partial class Campaign
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Agency { get; set; }
        public decimal DailyBudget { get; set; }
        public decimal TotalBudget { get; set; }
        public int TotalDaysRun { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
