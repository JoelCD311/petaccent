﻿namespace BusinessLayer.Models
{
    public class CardPaymentResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string ConfirmationNumber { get; set; }
    }
}
