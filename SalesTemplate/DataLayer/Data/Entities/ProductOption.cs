﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class ProductOption
    {
        public int ID { get; set; }
        public int? ParentOptionID { get; set; }
        [ForeignKey("Product")]
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string ShortValue { get; set; }
        public int? QuantityAvailable { get; set; }
        public string ImagePath { get; set; }
        public decimal? Price { get; set; }
        [NotMapped]
        public decimal? UndiscountedPrice { get; set; }

        [JsonIgnore]
        public virtual Product Product { get; set; }
    }
}
