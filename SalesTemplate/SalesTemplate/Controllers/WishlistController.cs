﻿using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLayer.Data.Entities;

namespace SalesTemplate.Controllers
{
    [Authorize]
    public class WishlistController : BaseController
    {
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var model = new WishlistModel();
            var sessionSvc = new SessionService();
            var wishSvc = new WishlistService();
            int perPage = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultProductsPerPage"]);

            model.Wishlist = (User.Identity.IsAuthenticated ? 
                await wishSvc.GetWishlistRecords(User.UserID, perPage).ConfigureAwait(false) : 
                new List<Wishlist>());

            model.Show = perPage;

            if (model.Wishlist.Count > perPage)
                model.Wishlist.RemoveRange(Convert.ToInt32(model.Show), model.Wishlist.Count - Convert.ToInt32(model.Show));

            model.Skip = 0;
            model.CurrentCount = model.Wishlist.Count;
            model.DisplayType = "list";
            model.ShoppingCart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            model.FirstName = User.FirstName;
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> AddToWishlist(int productID, int? optionID)
        {
            if (User.Identity.IsAuthenticated)
            {
                var wishSvc = new WishlistService();
                var resp = await wishSvc.UpdateWishlist(User.UserID, productID, optionID).ConfigureAwait(false);
                return Json(new { Success = resp, Message = (resp ? "" : "Item already exists in Wishlist!")});
            }
            else
                return Json(false);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveFromWishlist(int productID, int? optionID)
        {
            if (User.Identity.IsAuthenticated)
            {
                var wishSvc = new WishlistService();
                return Json(await wishSvc.RemoveFromWishlist(User.UserID, productID, optionID).ConfigureAwait(false));
            }
            else
                return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]        
        public async Task<JsonResult> Sort(string show, string skip, string sortby, string category, string pricelow, string pricehigh, string gridtype)
        {
            var wishSvc = new WishlistService();
            string[] sortbyArr = sortby.Split(':');
            var model = new WishlistModel();
            var listAndCount = await wishSvc.GetWishlistRecords(User.UserID, (show == "all" ? 100000 : Convert.ToInt32(show)),
                (Convert.ToInt32(skip) - 1),
                Convert.ToDecimal(pricelow),
                Convert.ToDecimal(pricehigh),
                (sortbyArr[0] == "position" ? "Order" : sortbyArr[0]), sortbyArr[1],
                category).ConfigureAwait(false);

            model.Show = (show == "all" ? 100000 : Convert.ToInt32(show));
            model.Skip = Convert.ToInt32(skip);
            model.Wishlist = listAndCount.Item1;
            model.CurrentCount = listAndCount.Item2;
            model.DisplayType = gridtype;

            return Json(this.RenderRazorViewToString(this, "_Products", model));
        }
    }
}