﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Data.Entity;

namespace DataLayer.Repositories
{
    public class OrderRepository
    {
        public async Task<string> CreateOrder(Order order, ICollection<OrderLineItem> lineItems)
        {
            try
            {
                var guid = Guid.NewGuid();
                var length = guid.ToString().Replace("-", "").Length;
                var id = guid.ToString().Replace("-", "");
                order.OrderID = id.Substring(length - 11, 10).ToUpper();

                using (var _db = new SalesTemplateContext())
                {
                    _db.Orders.Add(order);

                    if (await _db.SaveChangesAsync() > 0)
                    {
                        foreach (var item in lineItems)
                        {
                            item.OrderID = order.ID;
                            item.DateCreated = DateTime.Now;
                            item.Product = null;                            

                            if (item.ProductOptions != null)
                            {
                                item.ProductOptionID = _getMostChildishOptionID(item);

                                foreach (var option in item.ProductOptions)
                                    _db.Entry(option).State = EntityState.Detached;
                            }
                        }

                        _db.OrderLineItems.AddRange(lineItems);
                        if (await _db.SaveChangesAsync().ConfigureAwait(false) > 0)
                            return order.OrderID;
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }

            return "";
        }
        public async Task<Order> GetOrder(int orderdbid)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return await _db.Orders.Include(x => x.OrderLineItems)
                                           .Include(x => x.User)
                                           .Include(x => x.OrderLineItems.Select(y => y.Product))
                                           .Where(x => x.ID == orderdbid).FirstOrDefaultAsync();
                }
            }
            catch
            {
                // silent
            }

            return null;
        }
        public async Task<Order> GetConfirmationEmailOrder(int orderdbid)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    var order = await _db.Orders.Include(x => x.OrderLineItems)
                                           .Include(x => x.User)
                                           .Include(x => x.OrderLineItems.Select(y => y.Product))
                                           .Include(x => x.OrderLineItems.Select(y => y.ProductOption))
                                           .Where(x => x.ID == orderdbid).FirstOrDefaultAsync();

                    foreach (var item in order.OrderLineItems)
                    {
                        item.ProductOptions = new List<ProductOption>();
                        item.ProductOptions.Add(item.ProductOption);

                        var parOpt = await _db.ProductOptions.Where(x => x.ID == item.ProductOption.ParentOptionID).FirstOrDefaultAsync();

                        if (parOpt != null)
                            item.ProductOptions.Add(parOpt);
                    }

                    return order;
                }
            }
            catch
            {
                // silent
            }

            return null;
        }
        public async Task<List<OrderLineItem>> GetOrderLineItems(int orderdbid)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return await _db.OrderLineItems
                                    .Include(x => x.ProductOption)
                                    .Where(x => x.OrderID == orderdbid)
                                    .ToListAsync();
                }
            }
            catch
            {
                // silent
            }

            return null;
        }
        private int? _getMostChildishOptionID(OrderLineItem item)
        {
            try
            {
                if (item.ProductOptions.Count > 0)
                {
                    var child = item.ProductOptions.Where(x => x.ParentOptionID != null).FirstOrDefault();

                    if (child == null)
                    {
                        var parent = item.ProductOptions.Where(x => x.ParentOptionID == null).FirstOrDefault();

                        if (parent != null)
                            return parent.ID;
                        else
                            return null;
                    }
                    else
                        return child.ID;
                }
            }
            catch
            {
                // silent
            }

            return null;
        }
    }
}
