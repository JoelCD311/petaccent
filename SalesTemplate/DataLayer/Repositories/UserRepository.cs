﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace DataLayer.Repositories
{
    public class UserRepository
    {
        public async Task<User> GetUserByID(int id)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Users
                                .Include(x => x.Orders)
                                .Include(x => x.Addresses)
                                .Include(x => x.Addresses.Select(y => y.AddressType))
                                .Where(x => x.ID == id)
                                .SingleOrDefaultAsync()
                                .ConfigureAwait(false);
            }
        }

        public async Task<User> GetUserByEmailAddress(string emailAddress)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Users
                                .Include(x => x.Orders)
                                .Include(x => x.Addresses)
                                .Where(x => x.EmailAddress == emailAddress)
                                .SingleOrDefaultAsync()
                                .ConfigureAwait(false);
            }
        }

        public async Task<int> CreateUser(string firstName, string lastName, string emailAddress, string encryptedPassword, Guid salt)
        {
            using (var _db = new SalesTemplateContext())
            {
                var user = new User()
                {
                    Active = true,
                    DateCreated = DateTime.Now,
                    EmailAddress = emailAddress,
                    FirstName = firstName,
                    LastName = lastName,
                    Password = encryptedPassword,
                    Salt = salt,
                    EmailSubscribed = true,
                    PaymentProcessorID = null,
                    PreferredCharityID = null
                };

                _db.Users.Add(user);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                return user.ID;
            }
        }

        public async Task<bool> UpdateUser(User user)
        {
            using (var _db = new SalesTemplateContext())
            {
                _db.Users.Attach(user);
                _db.Entry(user).State = EntityState.Modified;

                foreach (var item in user.Addresses)
                {
                    if (!String.IsNullOrEmpty(item.Street1) && !String.IsNullOrEmpty(item.City) && !String.IsNullOrEmpty(item.State) && 
                        !String.IsNullOrEmpty(item.PostalCode) && !String.IsNullOrEmpty(item.Country))
                    {
                        _db.Entry(item).State = (item.New ? EntityState.Added : EntityState.Modified);
                    }
                }                    

                return (await _db.SaveChangesAsync().ConfigureAwait(false) > 0 ? true : false);
            }
        }

        public async Task<bool> EmailAddressInUse(string emailAddress)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Users.Where(x => x.EmailAddress == emailAddress)
                                      .AnyAsync()
                                      .ConfigureAwait(false);
            }
        }

        public async Task<List<User>> GetUsersEmailBlastSubscribed()
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Users.Where(x => x.EmailSubscribed)
                                      .ToListAsync()
                                      .ConfigureAwait(false);
            }
        }

        public async Task<List<Order>> GetShippedOrders(int date = 0)
        {
            // day - 1 == yesterday
            // day = 0 == today

            var beginDate = DateTime.Today.AddDays(date);
            var endDate = DateTime.Today.AddDays(date + 1);

            using (var _db = new SalesTemplateContext())
            {
                var status = await _db.OrderStatuses.Where(x => x.Status == "Shipped").FirstOrDefaultAsync();
                return await _db.Orders.Include(x => x.User)
                                       .Where(x => x.ShippedDate >= beginDate && x.ShippedDate < endDate &&
                                                   x.OrderStatusID == status.ID)
                                       .ToListAsync();
            }
        }

        //public void UpdateUser(User user)
        //{
        //    _db.Users.Attach(user);
        //    _db.Entry(user).State = EntityState.Modified;

        //    // update single column for performance
        //    //_db.Users.Attach(user);
        //    //var entry = _db.Entry(user);
        //    //entry.Property(e => e.Property).IsModified = true;
        //}

        //public async Task<int> Save()
        //{
        //    return await _db.SaveChangesAsync();
        //}
    }
}
