﻿using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SalesTemplate.App_Start
{
    public static class DatabaseConfig
    {
        private static readonly bool UseMySql = Convert.ToBoolean(ConfigurationManager.AppSettings["UseMySql"]);
        public static void RegisterConfiguration()
        {
            if (UseMySql)
            {
                DbConfiguration.SetConfiguration(new MySqlEFConfiguration());
                new DbExecutionConfig();
            }                
        }
    }

    public class DbExecutionConfig : DbConfiguration
    {
        private static readonly bool UseMySql = Convert.ToBoolean(ConfigurationManager.AppSettings["UseMySql"]);
        public DbExecutionConfig()
        {
            if (UseMySql)
                SetExecutionStrategy(MySqlProviderInvariantName.ProviderName, () => new MySqlExecutionStrategy());
        }
    }
}