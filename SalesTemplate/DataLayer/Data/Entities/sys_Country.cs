﻿namespace DataLayer.Data.Entities
{
    public partial class sys_Country
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public string CountryName { get; set; }
    }
}
