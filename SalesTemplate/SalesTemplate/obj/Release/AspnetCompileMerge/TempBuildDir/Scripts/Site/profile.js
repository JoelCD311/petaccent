﻿$('[name="Phone"]').mask("(999) 999-9999");
$(document).on('click', '.profile-remove-card', function () {
    var element = $(this);
    $('#profile-add-card').hide();
    $('.wait-spinner-add-card').show();
    $.post('/Profile/RemoveCard', getRemoveCardData($(this)), function (result) {
        removeCardResult(result, element);
    });
});
$('#email-subscribed').on('click', function () {    
    if ($(this).attr('data-selected') == 'false') {
        $(this).addClass('btn-success');
        $(this).removeClass('btn-danger');
        $(this).attr('data-selected', 'true')
    }
    else if ($(this).attr('data-selected') == 'true') {
        $(this).addClass('btn-danger');
        $(this).removeClass('btn-success');
        $(this).attr('data-selected', 'false')
    }
});
$('#save-changes').on('click', function () {

    $(this).hide();
    $('.wait-spinner-save-changes').show();

    if (validateSave()) {
        $('.save-profile-text').hide();
        $('.save-profile-wait').fadeIn();
        $.post('/Profile/UpdateProfile', getSaveProfileData(), saveProfileResult);        
    }
    else {
        alertify.error('Please correct any errors above.');
    }
});
$('#profile-add-card').on('click', function () {
    if ($('#profile-card-new').is(':visible')) {
        addCard();
    }
    else {
        $('#profile-card-new').slideDown();
        $('#profile-add-card-cancel').fadeIn();
    }
});
$('#profile-add-card-cancel').on('click', function () {
    closeClearedNewCardForm();
});
$('input').on('focus', function () {
    removeBasicInputError($(this));
});
$('input').on('keydown', function () {
    removeBasicInputError($(this));
});
$('input:password').on('focus', function () {
    removeNonTextError($(this));
});
$('input:password').on('keydown', function () {
    removeNonTextError($(this));
});
$('select').on('change', function () {
    removeNonTextError($(this));
});
function closeClearedNewCardForm() {
    $('#profile-card-new').slideUp();
    $('#profile-add-card-cancel').hide();
    $('[name="NameOnCard"]').val('');
    $('[name="PAN"]').val('');
    $('[name="expMonth"]').val('');
    $('[name="expYear"]').val('');
    $('[name="CardAddressZip"]').val('');
    $('[name="CardCvc"]').val('');

    $('.pin-input, .pin-dd').each(function () {
        removeBasicInputError($(this));
    });
}
function addCard() {
    if (validateNewCard()) {
        $('#profile-add-card').hide();
        $('.wait-spinner-add-card').show();
        $('.add-card-text').hide();
        $('.add-card-wait').fadeIn();
        $.post('/Profile/AddCard', getAddCardData(), addCardResult);
    }
}
function getRemoveCardData(elem) {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        id: elem.closest('tr').attr('id')
    }
}
function getAddCardData() {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        NameOnCard: $('[name="NameOnCard"]').val(),
        Number: $('[name="PAN"]').val(),
        AddressZip: $('[name="CardAddressZip"]').val(),
        ExpirationMonth: $('[name="expMonth"]').val(),
        ExpirationYear: $('[name="expYear"]').val(),
        Cvc: $('[name="CardCvc"]').val()
    }
}
function removeCardResult(result, elem) {
    if (result.Success) {
        elem.closest('tr').remove();
        if ($('.cart-table tr').length == 1) {
            $('#profile-cards-existing').hide();
        }
    }
    $('.wait-spinner-add-card').hide();
    $('#profile-add-card').show();
}
function addCardResult(result) {
    if (result.Success) {
        $('#profile-cards-existing').html(result.Html);
        $('#profile-cards-existing').show();
        closeClearedNewCardForm();
    }
    else {
        alertify.error(result.Html);
    }
    $('.wait-spinner-add-card').hide();
    $('#profile-add-card').show();    
}
function saveProfileResult(result) {
    if (result.Success) {
        alertify.success(result.Message);
    }
    else {
        alertify.error(result.Message);
    }
    $('.wait-spinner-save-changes').hide();
    $('#save-changes').show();    
}
function getSaveProfileData() {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        ProfileFirstName: $('[name="ProfileFirstName"]').val(),
        LastName: $('[name="LastName"]').val(),
        EmailAddress: $('[name="EmailAddress"]').val(),
        EmailSubscribed: $('#email-subscribed').prop('data-selected'),
        Phone: $('[name="Phone"]').val().replace(/\(/, '').replace(/\)/, '').replace(/ /, '').replace(/-/, ''),
        Password: $('[name="Password"]').val(),
        ConfirmPassword: $('[name="ConfirmPassword"]').val(),
        PreferredCharityID: $('.charities-dropdown').val(),
        DefaultCard: $('.profile-default-card:checked').closest('tr').attr('id'),
        EmailSubscribed: $('#email-subscribed').attr('data-selected'),
        ShippingAddress: {
            Street1: $('[name="ShippingAddress.Street1"]').val(),
            Street2: $('[name="ShippingAddress.Street2"]').val(),
            City: $('[name="ShippingAddress.City"]').val(),
            State: $('[name="ShippingAddress.State"]').val(),
            PostalCode: $('[name="ShippingAddress.PostalCode"]').val(),
            Country: $('[name="ShippingAddress.Country"]').val()
        }
    }
}
function validateNewCard() {

    var valid = true;
    var nameOnCard = $('[name="NameOnCard"]');
    var pan = $('[name="PAN"]');
    var expMonth = $('[name="expMonth"]');
    var expYear = $('[name="expYear"]');
    var cardZip = $('[name="CardAddressZip"]');
    var cardCvc = $('[name="CardCvc"]');

    if (nameOnCard.val() == "") {
        valid = false;
        nameOnCard.closest('div').addClass('has-error');
        nameOnCard.next().addClass('glyphicon-remove');
    }

    if (pan.val() == "") {
        valid = false;
        pan.closest('div').addClass('has-error');
        pan.next().addClass('glyphicon-remove');
    }

    if (expMonth.val() == "") {
        valid = false;
        expMonth.closest('div').addClass('has-error');
        expMonth.next().addClass('glyphicon-remove');
    }

    if (expYear.val() == "") {
        valid = false;
        expYear.closest('div').addClass('has-error');
        expYear.next().addClass('glyphicon-remove');
    }

    if (cardZip.val() == "") {
        valid = false;
        cardZip.closest('div').addClass('has-error');
        cardZip.next().addClass('glyphicon-remove');
    }
    else {
        if (!isValidPostalCode(cardZip.val())) {
            valid = false;
            cardZip.closest('div').addClass('has-error');
            cardZip.next().addClass('glyphicon-remove');
        }
    }

    if (cardCvc.val() == "") {
        valid = false;
        cardCvc.closest('div').addClass('has-error');
        cardCvc.next().addClass('glyphicon-remove');
    }

    return valid;
}
function validateSave() {

    var valid = true;
    var firstName = $('[name="ProfileFirstName"]');
    var lastName = $('[name="LastName"]');
    var email = $('[name="EmailAddress"]');
    var password = $('[name="Password"]');
    var rePassword = $('[name="ConfirmPassword"]');

    var street1 = $('[name="ShippingAddress.Street1"]');
    var street2 = $('[name="ShippingAddress.Street2"]');
    var city = $('[name="ShippingAddress.City"]');
    var state = $('[name="ShippingAddress.State"]');
    var postalcode = $('[name="ShippingAddress.PostalCode"]');
    var country = $('[name="ShippingAddress.Country"]');
    
    if (firstName.val() == '') {
        valid = false;
        addBasicInputError(firstName);
    }

    if (lastName.val() == '') {
        valid = false;
        addBasicInputError(lastName);
    }

    if (email.val() == '') {
        valid = false;
        addBasicInputError(email);
    }

    if (password.val() == '') {
        valid = false;
        addNonTextError(password);
    }

    if (rePassword.val() == '') {
        valid = false;
        addNonTextError(rePassword);
    } 

    if (password.val() != '' && rePassword.val() != '' && (password.val() != rePassword.val())) {
        valid = false;
        alertify.error('Passwords do not match');
    }

    if (street1.val() == '') {
        valid = false;
        addBasicInputError(street1);
    }

    if (city.val() == '') {
        valid = false;
        addBasicInputError(city);
    }

    if (state.val() == '0' || state.val() == '') {
        valid = false;
        addBasicInputError(state);
    }

    if (postalcode.val() == '') {
        valid = false;
        addBasicInputError(postalcode);
    }

    if (country.val() == '') {
        valid = false;
        addBasicInputError(country);
    }

    return valid;
}