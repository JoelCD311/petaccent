﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;

namespace BusinessLayer.Services
{
    public class UserService
    {
        private UserRepository _repo { get; set; }

        public UserService()
        {
            _repo = new UserRepository();
        }

        #region CRUD Operations

        public async Task<bool> EmailAddressInUse(string emailAddress)
        {
            return await _repo.EmailAddressInUse(emailAddress).ConfigureAwait(false);
        }

        public async Task<User> GetUserByID(int id)
        {
            return await _repo.GetUserByID(id).ConfigureAwait(false);
        }

        public async Task<User> GetUserByEmailAddress(string username)
        {
            return await _repo.GetUserByEmailAddress(username).ConfigureAwait(false);
        }

        public async Task<List<Order>> GetShippedOrders()
        {
            return await _repo.GetShippedOrders().ConfigureAwait(false);
        }

        public async Task<bool> UpdateUser(User user)
        {
            return await _repo.UpdateUser(user).ConfigureAwait(false);
        }

        public async Task<List<User>> GetUsersEmailBlastSubscribed()
        {
            return await _repo.GetUsersEmailBlastSubscribed().ConfigureAwait(false);
        }

        public async Task<int> CreateUser(User user)
        {
            var salt = Guid.NewGuid();
            return await _repo.CreateUser(user.FirstName, user.LastName, user.EmailAddress,
                EncryptionService.Encrypt(user.Password, salt.ToString()), salt).ConfigureAwait(false);
        }

        #endregion

        #region Functional Operations

        public async Task<HttpCookie> LoginUser(User user, bool rememberUser)
        {
            var prodSvc = new ProductService();
            
            var domain = await prodSvc.GetProductDomain(
                Convert.ToInt32(ConfigurationManager.AppSettings["ProductDomainID"])
                ).ConfigureAwait(false);

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, "." + domain.Name.Replace(" ", "") + "StoreTicket",
                DateTime.Now, DateTime.Now.AddMinutes(Convert.ToInt64(ConfigurationManager.AppSettings["LoginTimeout"])),
                rememberUser, JsonConvert.SerializeObject(
                    new
                    {
                        UserID = user.ID,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        EmailAddress = user.EmailAddress
                    })
                );

            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            return new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
        }

        public async Task<int> RegisterUser(User user, string welcomeEmailHtml)
        {
            var emailSvc = new EmailService();            

            var emailReqConv = await emailSvc.IsEmailInEmailRequestConversion(user.EmailAddress)
                .ConfigureAwait(false);

            int userID = await CreateUser(user).ConfigureAwait(false);

            if (emailReqConv.Item1)
            {
                var storeSvc = new StoreCreditService();
                var creditType = await storeSvc.GetStoreCreditTypeByName("Reward").ConfigureAwait(false);

                var task1 = storeSvc.WriteStoreCreditGrantedRecord(userID, creditType.ID, 
                    Convert.ToDecimal(ConfigurationManager.AppSettings["CurrentPromotionAmount"]), 
                    "Email Request Granted", null);

                var task2 = emailSvc.UpdateEmailRequestConversion(user.EmailAddress, true);

                await Task.WhenAll(task1, task2).ConfigureAwait(false);
            }

            if (userID > 0)
                await _welcomeUser(user, welcomeEmailHtml).ConfigureAwait(false);

            return userID;
        }

        private async Task _welcomeUser(User user, string welcomeEmailHtml)
        {
            var emailSvc = new EmailService();
            await emailSvc.SendWelcomeEmail(user, welcomeEmailHtml).ConfigureAwait(false);
        }

        #endregion
    }
}
