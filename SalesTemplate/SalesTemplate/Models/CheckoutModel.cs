﻿using BusinessLayer.Models;
using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SalesTemplate.Models
{
    public class CheckoutModel : BaseModel
    {
        public List<sys_Country> Countries { get; set; }
        public List<sys_State> States { get; set; }
        public List<Charity> Charities { get; set; }
        public List<CreditCardModel> Cards { get; set; }
        public int Charity { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string FinalMessage { get; set; }
        public bool OrderSuccess { get; set; }

        public CheckoutModel(bool populateBase = true) : base(populateBase)
        {
            this.UsedCoupons = new List<StoreCredit>();
        }
    }
}