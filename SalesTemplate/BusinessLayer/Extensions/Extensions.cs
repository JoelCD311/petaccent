﻿using System.Web;

namespace BusinessLayer.Extensions
{
    public static class HttpRequestBaseExtensions
    {
        public static string GetClientIpAddress(this HttpRequestBase request)
        {
            return request.Headers["X-Cluster-Client-Ip"] ?? request.UserHostAddress;
        }
    }
}
