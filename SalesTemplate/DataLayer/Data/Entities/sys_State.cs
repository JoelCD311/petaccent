﻿namespace DataLayer.Data.Entities
{
    public partial class sys_State
    {
        public int ID { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public int Country { get; set; }
    }
}
