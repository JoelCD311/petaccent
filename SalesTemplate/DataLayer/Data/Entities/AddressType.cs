﻿using System.Collections.Generic;

namespace DataLayer.Data.Entities
{
    public partial class AddressType
    {
        public AddressType()
        {
            this.Addresses = new HashSet<Address>();
        }

        public int ID { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
