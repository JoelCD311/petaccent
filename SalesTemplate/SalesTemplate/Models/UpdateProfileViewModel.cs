﻿using BusinessLayer.Models;
using DataLayer.Data.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SalesTemplate.Models
{
    public class UpdateProfileViewModel : BaseModel
    {
        public UpdateProfileViewModel(bool populateBase) : base(populateBase)
        {
            ShippingAddress = new Address();
        }

        public UpdateProfileViewModel() : base(false)
        {
            ShippingAddress = new Address();
        }

        public int ID { get; set; }
        public string ProfileFirstName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Nullable<decimal> Phone { get; set; }
        public System.DateTime DateCreated { get; set; }
        public bool EmailSubscribed { get; set; }
        public int PreferredCharityID { get; set; }
        public string DefaultCard { get; set; }
        public decimal TotalRewardsAndCredits { get; set; }
        public Address ShippingAddress { get; set; }
        public List<CreditCardModel> Cards { get; set; }
        public List<sys_State> States { get; set; }
        public List<sys_Country> Countries { get; set; }
        public List<Charity> Charities { get; set; }
    }
}