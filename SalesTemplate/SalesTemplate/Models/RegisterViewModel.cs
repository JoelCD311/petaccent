﻿namespace SalesTemplate.Models
{
    public class RegisterViewModel : BaseModel
    {
        public RegisterViewModel(bool populateBase = true) : base(populateBase) { }

        public string LastName { get; set; }
        public string EmailAddress { get; set; }
    }
}