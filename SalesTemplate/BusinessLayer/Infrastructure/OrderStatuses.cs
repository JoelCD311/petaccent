﻿namespace BusinessLayer.Infrastructure
{
    public enum OrderStatuses
    {
        Created = 1,
        Processing = 2,
        Processed = 3,
        Shipped = 4,
        Cancelled = 5,
        Returned = 6,
        Chargeback = 7
    }
}
