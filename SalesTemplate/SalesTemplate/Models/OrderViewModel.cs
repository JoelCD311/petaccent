﻿using DataLayer.Data.Models.ViewModels;

namespace SalesTemplate.Models
{
    public class OrderInfoModel
    {
        public string ShippingName { get; set; }
        public string ShippingStreet1 { get; set; }
        public string ShippingStreet2 { get; set; }
        public string ShippingStreet3 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingCountryFull { get; set; }
        public string ShippingNotes { get; set; }
        public bool IsGift { get; set; }
        public string EmailAddress { get; set; }
        public BillingInfoModel BillingInfo { get; set; }
        public int CharityID { get; set; }        
    }
}