﻿using BusinessLayer.Extensions;
using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    public class AboutController : BaseController
    {
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            if (Request.Cookies[RememberUsernameCookieName] != null)
                ViewBag.EmailAddress = Request.Cookies[RememberUsernameCookieName].Value;

            var sessionSvc = new SessionService();
            var model = new AboutModel();

            var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            model.CartTotal = cart.Order.OrderLineItems.Sum(x => x.Quantity);
            model.FirstName = User.FirstName;
            model.ReCatpchaSiteKey = ConfigurationManager.AppSettings["ReCaptcha:SiteKey"];
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> SendAboutMessage(string name, string email, string subject, string message, string grr)
        {
            var emailSvc = new EmailService();
            if (await emailSvc.VerifyEmailAddress(email).ConfigureAwait(false))
            {
                if (await _verifyReCaptcha(grr, Convert.ToBoolean(ConfigurationManager.AppSettings["ReCaptchaTestMode"])
                            ? "127.0.0.1" : Request.GetClientIpAddress()).ConfigureAwait(false))
                {
                    await emailSvc.SendAboutPageEmail(name, email, subject, message).ConfigureAwait(false);
                    return Json(new { Success = true, ErrorMsg = String.Empty });
                }
            }            

            return Json(new { Success = false, ErrorMsg = "Invalid email address" });
        }

        private async Task<bool> _verifyReCaptcha(string verf, string ip)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("secret", ConfigurationManager.AppSettings["ReCaptcha:SecretKey"]),
                    new KeyValuePair<string, string>("response", verf),
                    new KeyValuePair<string, string>("remoteip", ip)
                });

                var response = await client.PostAsync("https://www.google.com/recaptcha/api/siteverify", content).ConfigureAwait(false);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var strResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecaptchaResponse>(strResult);
                    return result.success;
                }
            }

            return false;
        }

        private class RecaptchaResponse
        {
            public bool success { get; set; }
            public DateTime challenge_ts { get; set; }
            public string hostname { get; set; }
        }
    }
}