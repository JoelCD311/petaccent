﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using DataLayer.Helpers.Models;

namespace BusinessLayer.Services
{
    public class ProductService
    {
        private ProductRepository _repo { get; set; }

        public ProductService()
        {
            _repo = new ProductRepository();
        }

        public async Task<Product> GetProductByID(int productID)
        {
            return await _repo.GetProductByID(productID)
                .ConfigureAwait(false);
        }

        public async Task<List<Product>> GetFeaturedProducts()
        {
            return await _repo.GetFeaturedProducts()
                .ConfigureAwait(false);
        }

        public async Task<List<Product>> GetNonFeaturedProducts()
        {
            return await _repo.GetNonFeaturedProducts()
                .ConfigureAwait(false);
        }

        public async Task<List<Product>> GetAllProducts()
        {
            var products = await _repo.GetAllProducts().ConfigureAwait(false);

            foreach (var product in products)
            {
                var props = DataLayer.Helpers.Helpers.FilterOptionSelection(product, product.ProductOptions.Where(x => x.ParentOptionID != null).Any() ?
                    product.ProductOptions.Where(x => x.ParentOptionID != null).FirstOrDefault() :
                    product.ProductOptions.FirstOrDefault());                 

                product.Price = props.Price;
                product.UndiscountedPrice = props.UndiscountedPrice;
                product.ImagePath = props.ImagePath;
            }

            return products;
        }

        public async Task<List<Product>> GetProductsByCategory(string category)
        {
            return await _repo.GetProductsByCategory(category)
                .ConfigureAwait(false);
        }

        public async Task<Tuple<List<Product>, int>> GetProducts(int count = 6, int page = 0, decimal pricelow = 0, decimal pricehigh = 1000, string column = "", string order = "", string category = "")
        {
            page = (page < 0 ? 0 : page); // make sure page is >= 0
           
            return await _repo.GetProducts(Convert.ToInt32(ConfigurationManager.AppSettings["ProductDomainID"]),
                count, page, pricelow, pricehigh, column, order, category).ConfigureAwait(false);
        }

        public async Task<ProductDomain> GetProductDomain(int domainID)
        {
            var _repo = new ProductDomainRepository();
            return await _repo.GetDomainNameAndDescByID(domainID).ConfigureAwait(false);
        }

        public async Task UpdateProductQuantity(int prodID, int[] childOptionIDs, int quantity)
        {
            await _repo.UpdateProductQuantity(prodID, childOptionIDs, quantity).ConfigureAwait(false);
        }

        public async Task<List<ProductOption>> GetProductOptions(int? optID)
        {
            return await _repo.GetProductOptions(optID).ConfigureAwait(false);
        }

        public async Task<List<ProductOption>> GetProductChildOptionList(int prodID, int optID)
        {
            return await _repo.GetProductChildOptionList(prodID, optID).ConfigureAwait(false);
        }

        public async Task<OptionSelection> GetUpdatedOptionSelection(int productID, int optionID)
        {
            var prodSvc = new ProductService();
            var product = await prodSvc.GetProductByID(productID).ConfigureAwait(false);
            var option = product.ProductOptions.Where(x => x.ID == optionID).FirstOrDefault();
            return DataLayer.Helpers.Helpers.FilterOptionSelection(product, option);
        }
    }
}
