﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesTemplate.Models;
using BusinessLayer.Services;
using System.Threading.Tasks;
using DataLayer.Data.Models;
using DataLayer.Data.Models.ViewModels;
using BusinessLayer.Models;
using DataLayer.Data.Entities;
using BusinessLayer.Infrastructure;
using BusinessLayer.Extensions;
using System.Web.Hosting;

namespace SalesTemplate.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class CheckoutController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var model = new CheckoutModel(true);
            var charitySvc = new CharityService();
            var systemSvc = new SystemService();
            var userSvc = new UserService();
            var paymentSvc = new PaymentService();

            var sessionSvc = new SessionService();
            var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);

            if (cart.Order.OrderLineItems.Count == 0)
                return RedirectToAction("Index", "Products");

            var user = User;
            var carTask = charitySvc.GetCharities();
            var coTask = systemSvc.GetCountries();
            var statesTask = systemSvc.GetStatesByCountry(1); // USA default
            var userTask = userSvc.GetUserByID(user.UserID);

            var calcTask = paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(), user.UserID);

            await Task.WhenAll(carTask, coTask, statesTask, calcTask, userTask).ConfigureAwait(false); 

            model.Charities = carTask.Result;
            model.Charity = (cart.Order.CharityID == 0 ? 1 : cart.Order.CharityID);
            model.States = statesTask.Result;
            model.Countries = coTask.Result;
            model.ShoppingCart = calcTask.Result;
            model.UsedCoupons = sessionSvc.CouponCodes();
            model.FirstName = User.FirstName;

            if (user.Identity.IsAuthenticated)
            {
                model.ShoppingCart.Order.UserID = user.UserID;
                model.Cards = await GetCreditCards(userTask.Result.PaymentProcessorID).ConfigureAwait(false);
                model.ShoppingCart.Order = getShippingInfo(model.ShoppingCart.Order, userTask.Result);
                model.ShoppingCart.Order.ShippingName = (user.FirstName + " " + user.LastName);
                model.ShoppingCart.Order.EmailAddress = user.EmailAddress;
            }
            else
            {
                model.ShoppingCart.Order = getShippingInfo(model.ShoppingCart.Order);
                model.ShoppingCart.Order.UserID = 0;
                model.Cards = new List<CreditCardModel>();
                model.ShoppingCart.Order.ShippingName = String.Empty;
                model.ShoppingCart.Order.EmailAddress = String.Empty;
            }

            model.ShoppingCart.Order.ShippingNotes = String.Empty;
            model.ShoppingCart.Order.BillingInfo = new BillingInfoModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> PlaceOrder(OrderInfoModel order)
        {
            try
            {
                var paymentSvc = new PaymentService();
                var sessionSvc = new SessionService();
                var userSvc = new UserService();
                var model = new CheckoutModel();

                // Calculate
                var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);

                cart = await paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(),
                    (User.Identity.IsAuthenticated ? User.UserID : 0)).ConfigureAwait(false);
                sessionSvc.UpdateShoppingCart(cart);

                cart.Order.StoreCreditsApplied = (cart.Order.ApplyCredits || sessionSvc.CouponCodes().Count > 0) ? cart.Order.StoreCreditsApplicable : 0;
                cart = MapOrderViewModelToCart(order, cart);
                cart.Order.User = await userSvc.GetUserByID(Convert.ToInt32(cart.Order.UserID)).ConfigureAwait(false);
                cart.Order.OrderStatusID = (int)OrderStatuses.Created;

                // Get Payment
                var payment = await paymentSvc.OneTimePayment(cart.Order)
                    .ConfigureAwait(false);

                if (payment.Success)
                {
                    // Get CampaignID/name if exists
                    if (Request.Cookies[CampaignNameCookieName] != null)
                    {
                        var cookie = Request.Cookies[CampaignNameCookieName] as HttpCookie;
                        var parts = cookie.Value.Split(':');

                        var campSvc = new CampaignService();
                        var campaign = await campSvc.GetCampaignByName(parts[0]).ConfigureAwait(false);

                        if (campaign.ID == 0)
                            cart.Order.CampaignID = null;
                        else
                            cart.Order.CampaignID = campaign.ID;
                    }

                    // Create order
                    var orderSvc = new OrderService();
                    cart.Order.IPAddress = Request.GetClientIpAddress();
                    cart.Order.PaymentMethod = await paymentSvc.GetPaymentMethod("CreditCard").ConfigureAwait(false);
                    var orderID = await orderSvc.PlaceOrder(cart, sessionSvc.CouponCodes(), payment.ConfirmationNumber).ConfigureAwait(false);

                    cart.Order.OrderID = orderID;
                    model.OrderSuccess = !String.IsNullOrEmpty(orderID);
                    model.FinalMessage = (String.IsNullOrEmpty(orderID) ? "<span style='color: red'>We're sorry, we could not complete your order at this time. Please try again later.</span>" :
                                "<b>Thank you for your order!</b> You will receive an email receipt and once your order has shipped you will receive a shipping confirmation email. Thanks again and have a great day!");
                    model.ShoppingCart = cart;

                    if (!String.IsNullOrEmpty(orderID))
                    {
                        // clear coupon codes from session
                        sessionSvc.ClearCouponCodes();

                        // save card if elected
                        if (order.BillingInfo.SaveCard && cart.Order.User != null && User.Identity.IsAuthenticated)
                        {
                            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => _saveCard(cart.Order.BillingInfo, cart.Order.User.ID));
                        }

                        // send conf email
                        await _sendConfirmEmail(cart.Order.User, cart).ConfigureAwait(false);
                    }

                    return Json(
                        new OrderPlacementResponse()
                        {
                            Success = model.OrderSuccess,
                            OrderCompleteHtml = RenderRazorViewToString(this, "_OrderComplete", model)
                        }
                    );
                }
                else
                {
                    return Json(
                        new OrderPlacementResponse()
                        {
                            OrderID = "",
                            Success = false,
                            Message = payment.ErrorMessage
                        }
                    );
                }
            }
            catch (Exception ex) { }

            return null;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Empty()
        {
            var sessionSvc = new SessionService();
            sessionSvc.UpdateShoppingCart(new ShoppingCart() { Order = new Order() });
            sessionSvc.UpdateShoppingCartTotalItems(0);

            if (Request.Cookies[CampaignNameCookieName] != null)
            {
                var cookie = Request.Cookies[CampaignNameCookieName] as HttpCookie;
                var parts = cookie.Value.Split(':');
                return RedirectToAction("Index", "Home", new { ac = parts[0] });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private Order getShippingInfo(Order order, User user)
        {
            var shipAddr = user.Addresses.Where(x => x.AddressType.Type == "Shipping").FirstOrDefault();

            if (shipAddr != null)
            {
                order.ShippingStreet1 = shipAddr.Street1;
                order.ShippingStreet2 = shipAddr.Street2;
                order.ShippingStreet3 = shipAddr.Street3;
                order.ShippingCity = shipAddr.City;
                order.ShippingState = shipAddr.State;
                order.ShippingPostalCode = shipAddr.PostalCode;
                order.ShippingCountry = shipAddr.Country;
            }

            return order;
        }
        private Order getShippingInfo(Order order)
        {

            order.ShippingStreet1 = String.Empty;
            order.ShippingStreet2 = String.Empty;
            order.ShippingStreet3 = String.Empty;
            order.ShippingCity = String.Empty;
            order.ShippingState = String.Empty;
            order.ShippingPostalCode = String.Empty;
            order.ShippingCountry = String.Empty;

            return order;
        }
        private async Task _saveCard(BillingInfoModel billingInfo, int userID)
        {
            var paymentSvc = new PaymentService();
            await paymentSvc.AddCreditCard(new CreditCardModel()
            {
                NameOnCard = billingInfo.NameOnCard,
                Number = billingInfo.PAN,
                Cvc = billingInfo.CardCvc,
                ExpirationYear = billingInfo.CardExpirationYear,
                ExpirationMonth = billingInfo.CardExpirationMonth
            }, userID).ConfigureAwait(false);
        }
        private async Task _sendConfirmEmail(User user, ShoppingCart cart)
        {
            var model = new EmailModel();
            var charSvc = new CharityService();
            var emailSvc = new EmailService();
            var prodSvc = new ProductService();

            model.ShoppingCart = cart;
            model.ShoppingCart.Order.CharityID = (model.ShoppingCart.Order.CharityID == 0 ? 1 : model.ShoppingCart.Order.CharityID);
            model.ShoppingCart.Order.Charity = await charSvc.GetCharityByID(model.ShoppingCart.Order.CharityID).ConfigureAwait(false);
            model.FirstName = User.Identity.IsAuthenticated ? User.FirstName : cart.Order.IsGift ? "Customer" : cart.Order.ShippingName;

            if (user == null)
            {
                user = new DataLayer.Data.Entities.User();
                user.FirstName = User.Identity.IsAuthenticated ? User.FirstName : cart.Order.IsGift ? "Customer" : cart.Order.ShippingName;
                user.EmailAddress = cart.Order.EmailAddress;
            }

            foreach (var item in model.ShoppingCart.Order.OrderLineItems)
                item.Product = await prodSvc.GetProductByID(item.ProductID).ConfigureAwait(false);

            await emailSvc.SendOrderConfirmationEmail(user, 
                this.RenderRazorViewToString(this, "~/Views/Emails/OrderConfirmation.cshtml", model), 
                cart.Order.IsGift);
        }
    }
}