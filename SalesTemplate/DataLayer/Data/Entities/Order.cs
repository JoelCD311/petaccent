﻿using DataLayer.Data.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class Order
    {
        public Order()
        {
            this.OrderLineItems = new HashSet<OrderLineItem>();            
        }

        public int ID { get; set; }
        [ForeignKey("User")]
        public int? UserID { get; set; }
        public string ShippingName { get; set; }
        public string ShippingStreet1 { get; set; }
        public string ShippingStreet2 { get; set; }
        public string ShippingStreet3 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingCountry { get; set; }
        [NotMapped]
        public string ShippingCountryFull { get; set; }
        [MaxLength(100)]
        public string ShippingNotes { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal OrderSubTotal { get; set; }
        public decimal TaxTotal { get; set; }
        [NotMapped]
        public decimal StoreCreditsAvailable { get; set; }
        [NotMapped]
        public decimal StoreCreditsApplicable { get; set; }
        public decimal StoreCreditsApplied { get; set; }
        [NotMapped]
        public bool ApplyCredits { get; set; }
        public int? StoreCreditID { get; set; }
        public decimal OrderTotal { get; set; }
        public System.DateTime OrderDate { get; set; }
        public bool IsGift { get; set; }
        [ForeignKey("PaymentMethod")]
        public int PaymentMethodID { get; set; }
        public decimal TotalToDonate { get; set; }
        [ForeignKey("Charity")]
        public int CharityID { get; set; }
        public decimal ShippingCharge { get; set; }
        public System.Nullable<System.DateTime> ShippedDate { get; set; }
        public string TrackingNumber { get; set; }
        public string IPAddress { get; set; }
        public string EmailAddress { get; set; }
        [ForeignKey("OrderStatus")]
        public int OrderStatusID { get; set; }
        public string OrderID { get; set; }
        public int? CampaignID { get; set; }
        public string PaymentConfirmation { get; set; }
        public System.DateTime DateCreated { get; set; }
        
        [NotMapped]
        public BillingInfoModel BillingInfo { get; set; }
        [NotMapped]
        public List<string> CouponCodes { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<OrderLineItem> OrderLineItems { get; set; }        
        public virtual Charity Charity { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }
    }
}
