﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class Product
    {
        public Product()
        {
            this.ProductOptions = new HashSet<ProductOption>();
        }

        public int ID { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        [NotMapped]
        public decimal? UndiscountedPrice { get; set; }
        public decimal? Cost { get; set; }
        public bool Taxable { get; set; }
        public decimal? ShippingCharge { get; set; }
        public decimal? ShippingCost { get; set; }
        public int? QuantityAvailable { get; set; }
        public bool Active { get; set; }
        public bool Featured { get; set; }
        public int? Order { get; set; }
        public string ImagePath { get; set; }

        [ForeignKey("ProductDomain")]
        public int DomainID { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public System.DateTime DateCreated { get; set; }

        [JsonIgnore]
        public virtual ProductDomain ProductDomain { get; set; }
        public virtual ICollection<ProductOption> ProductOptions { get; set; }
    }
}
