﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;

namespace SalesTemplate.Infrastructure
{
    public class SalesPrincipal : ISalesPrincipal
    {
        public SalesPrincipal()
        {
            var identity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;

            this.UserID = Convert.ToInt32(identity.Claims.Where(x => x.Type == "UserID").Select(x => x.Value).FirstOrDefault());
            this.FirstName = identity.Claims.Where(x => x.Type == "FirstName").Select(x => x.Value).FirstOrDefault();
            this.LastName = identity.Claims.Where(x => x.Type == "LastName").Select(x => x.Value).FirstOrDefault();
            this.EmailAddress = identity.Claims.Where(x => x.Type == "EmailAddress").Select(x => x.Value).FirstOrDefault();
            this.Identity = Thread.CurrentPrincipal.Identity;
        }
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public IIdentity Identity { get; set; }
        public bool IsInRole(string role)
        {
            // implement later if needed
            return false;
        }
    }
}