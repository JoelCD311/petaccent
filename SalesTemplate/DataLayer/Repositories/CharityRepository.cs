﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using System.Linq;

namespace DataLayer.Repositories
{
    public class CharityRepository : BaseRepository
    {
        public async Task<List<Charity>> GetAllCharities()
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Charities.OrderBy(x => x.CharityName)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
            }            
        }

        public async Task<Charity> GetCharityByID(int id)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Charities.Where(x => x.ID == id)
                                          .FirstOrDefaultAsync()
                                          .ConfigureAwait(false);
            }
        }
    }
}
