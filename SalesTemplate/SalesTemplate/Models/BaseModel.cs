﻿using BusinessLayer.Services;
using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace SalesTemplate.Models
{
    public class BaseModel
    {
        public List<Product> Products { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public int CartTotal { get; set; }
        public ShoppingCart ShoppingCart { get; set; }
        public List<StoreCredit> UsedCoupons { get; set; }
        public bool Redirect { get; set; }
        public string RedirectTo { get; set; }
        public decimal CurrentPromoAmount { get; set; }

        public BaseModel(bool populate = false)
        {
            if (populate)
            {
                var prodSvc = new ProductService();
                var sessionSvc = new SessionService();
                this.Products = prodSvc.GetAllProducts().Result;
                this.ShoppingCart = sessionSvc.GetShoppingCart().Result;
            }

            CurrentPromoAmount = Convert.ToDecimal(ConfigurationManager.AppSettings["CurrentPromotionAmount"]);
        }
    }
}