﻿using DataLayer.Data.Entities;
using MySql.Data.Entity;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace DataLayer.Data.Contexts
{
    public class SalesTemplateContext : DbContext
    {
        private static readonly bool UseMySql = Convert.ToBoolean(ConfigurationManager.AppSettings["UseMySql"]);

        public SalesTemplateContext()
            : base((UseMySql ? "MySqlSalesTemplateContext" : "MSSQLSalesTemplateContext"))
        {
            Database.SetInitializer<SalesTemplateContext>(null);
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // to change schema
            //modelBuilder.Entity<EmailDelivery>().ToTable("EmailDelivery", "reporting");
            //modelBuilder.Entity<EmailFeedback>().ToTable("EmailFeedback", "reporting");            

            modelBuilder.Entity<Address>().ToTable("Address");
            modelBuilder.Entity<AddressType>().ToTable("AddressType");
            modelBuilder.Entity<Campaign>().ToTable("Campaign");
            modelBuilder.Entity<Charity>().ToTable("Charity");
            modelBuilder.Entity<EmailTemplate>().ToTable("EmailTemplate");
            modelBuilder.Entity<EmailRequestConversion>().ToTable("EmailRequestConversion");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<OrderLineItem>().ToTable("OrderLineItem");
            modelBuilder.Entity<OrderStatus>().ToTable("OrderStatus");
            modelBuilder.Entity<PaymentMethod>().ToTable("PaymentMethod");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<ProductOption>().ToTable("ProductOption");
            modelBuilder.Entity<ProductDomain>().ToTable("ProductDomain");
            modelBuilder.Entity<StoreCredit>().ToTable("StoreCredit");
            modelBuilder.Entity<StoreCreditType>().ToTable("StoreCreditType");
            modelBuilder.Entity<TaxRate>().ToTable("TaxRate");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Wishlist>().ToTable("Wishlist");
            modelBuilder.Entity<sys_Country>().ToTable("sys_Country");
            modelBuilder.Entity<sys_State>().ToTable("sys_State");

            base.OnModelCreating(modelBuilder);
        }

        public bool ExistsLocally<T>(T entity) where T : class
        {
            return Set<T>().Local.Any(e => e == entity);
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Charity> Charities { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<EmailRequestConversion> EmailRequestConversions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLineItem> OrderLineItems { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductOption> ProductOptions { get; set; }
        public DbSet<ProductDomain> ProductDomains { get; set; }
        public DbSet<StoreCredit> StoreCredits { get; set; }
        public DbSet<StoreCreditType> StoreCreditTypes { get; set; }
        public DbSet<TaxRate> TaxRates { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Wishlist> WishListRecords { get; set; }
        public DbSet<sys_State> States { get; set; }
        public DbSet<sys_Country> Countries { get; set; }
    }
}
