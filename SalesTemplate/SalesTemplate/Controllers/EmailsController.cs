﻿using BusinessLayer.Services;
using DataLayer.Data.Entities;
using SalesTemplate.Filters;
using SalesTemplate.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    public class EmailsController : BaseController
    {
        //[AllowAnonymous]
        //public async Task<ActionResult> Shipping()
        //{
        //    var orderService = new OrderService();
        //    var model = new EmailModel(true);
        //    model.FirstName = "Joel";
        //    model.Promotion = true;
        //    model.ShoppingCart.Order = await orderService.GetConfirmationEmailOrder(34).ConfigureAwait(false);
        //    model.ShoppingCart.Order.TrackingNumber = "ASDF2342ASDF456";

        //    return View("ShippingConfirmation", model);
        //}

        //[AllowAnonymous]
        //public async Task<ActionResult> Order(int orderid)
        //{
        //    var orderService = new OrderService();
        //    var model = new EmailModel(true);
        //    model.FirstName = "Joel";
        //    model.Promotion = true;
        //    model.ShoppingCart.Order = await orderService.GetConfirmationEmailOrder(orderid).ConfigureAwait(false);

        //    return View("OrderConfirmation", model);
        //}

        //[AllowAnonymous]
        //public async Task<ActionResult> Password()
        //{
        //    var orderService = new OrderService();
        //    var model = new EmailModel(true);
        //    model.FirstName = "Joel";
        //    model.Promotion = true;
        //    model.Password = "Texas817!";

        //    return View("ForgotPassword", model);
        //}

        //[AllowAnonymous]
        //public async Task<ActionResult> Welcome()
        //{
        //    var orderService = new OrderService();
        //    var model = new EmailModel(true);
        //    model.FirstName = "Joel";
        //    model.Promotion = true;
        //    model.ShoppingCart.Order = await orderService.GetConfirmationEmailOrder(34).ConfigureAwait(false);

        //    return View("Welcome", model);
        //}

        //[AllowAnonymous]
        //public async Task<ActionResult> Friend()
        //{
        //    var productService = new ProductService();
        //    var model = new EmailModel(true);
        //    model.Product = await productService.GetProductByID(22).ConfigureAwait(false);
        //    model.ReferralName = "Joel Dunn";
        //    model.ReferralEmail = "joelcdunn@gmail.com";
        //    model.Promotion = true;

        //    return View("~/Views/Products/_SentFromFriend.cshtml", model);
        //}

        //[AllowAnonymous]
        //public ActionResult Welcome()
        //{
        //    return View();
        //}

        //[AllowAnonymous]
        //public async Task<ActionResult> OrderConfirmation()
        //{
        //    var model = new ConfirmationModel();
        //    var sessionSvc = new SessionService();
        //    var charSvc = new CharityService();
        //    var prodSvc = new ProductService();
        //    model.Cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
        //    model.Cart.Order.CharityID = (model.Cart.Order.CharityID == 0 ? 1 : model.Cart.Order.CharityID);
        //    model.Cart.Order.Charity = await charSvc.GetCharityByID(model.Cart.Order.CharityID).ConfigureAwait(false);
        //    model.FirstName = User.FirstName;

        //    foreach (var item in model.Cart.Order.OrderLineItems)
        //    {
        //        item.Product = await prodSvc.GetProductByID(item.ProductID).ConfigureAwait(false);
        //    }

        //    return View(model);
        //}

        [SecureBasic]
        [AllowAnonymous]
        public async Task<string> Blast1()
        {
            return this.RenderRazorViewToString(this, "~/Views/Emails/EmailBlasts/Blast1.cshtml", null);
        }

        [SecureBasic]
        [AllowAnonymous]
        public async Task<string> ShippingConfirmation(int id)
        {
            var model = new ConfirmationModel();
            var orderSvc = new OrderService();
            var charSvc = new CharityService();
            var prodSvc = new ProductService();

            model.Cart = new DataLayer.Data.Models.ShoppingCart();

            var task1 = orderSvc.GetOrder(id);
            var task2 = orderSvc.GetOrderLineItems(id);

            await Task.WhenAll(task1, task2).ConfigureAwait(false);

            model.Cart.Order = task1.Result;
            model.Cart.Order.OrderLineItems = (ICollection<OrderLineItem>)task2.Result;
            model.Cart.Order.Charity = await charSvc.GetCharityByID(model.Cart.Order.CharityID).ConfigureAwait(false);
            model.FirstName = model.Cart.Order.User == null ? model.Cart.Order.ShippingName : model.Cart.Order.User.FirstName + " " + model.Cart.Order.User.LastName;

            return RenderRazorViewToString(this, "~/Views/Emails/ShippingConfirmation.cshtml", model);
        }
    }
}