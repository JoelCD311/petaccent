﻿using BusinessLayer.Models;
using BusinessLayer.Services;
using DataLayer.Data.Entities;
using DataLayer.Models;
using SalesTemplate.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    public class RegisterController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            var sessionSvc = new SessionService();
            var model = new RegisterViewModel();
            var regData = sessionSvc.RegistrationData();
            model.FirstName = regData.FirstName;
            model.LastName = regData.LastName;
            model.EmailAddress = regData.EmailAddress;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(RegistrationViewModel model)
        {
            try
            {
                if (!IsValidEmailAddress(model.EmailAddress))
                {
                    return Json(new RegistrationResponse()
                    {
                        Success = false,
                        ErrorMessage = "Invalid email address. Please try again"
                    });
                }

                if (await EmailInUse(model.EmailAddress).ConfigureAwait(false))
                {
                    return Json(new RegistrationResponse()
                    {
                        Success = false,
                        ErrorMessage = "Email address already in use."
                    });
                }

                if (!await VerifyEmailAddress(model.EmailAddress).ConfigureAwait(false))
                {
                    return Json(new RegistrationResponse()
                    {
                        Success = false,
                        ErrorMessage = "Invalid email address. Please try again"
                    });
                }

                var userSvc = new UserService();
                var newUser = new User()
                {
                    FirstName = model.FirstName.Trim(),
                    LastName = model.LastName.Trim(),
                    EmailAddress = model.EmailAddress.ToLower().Trim(),
                    Password = model.Password.Trim()
                };

                string html = _getWelcomeEmailHtml(newUser);                
                var newUserID = await userSvc.RegisterUser(newUser, html).ConfigureAwait(false);

                if (newUserID > 0)
                {
                    Response.Cookies.Add(await userSvc.LoginUser(new User()
                    {
                        ID = newUserID,
                        FirstName = model.FirstName.Trim(),
                        LastName = model.LastName.Trim(),
                        EmailAddress = model.EmailAddress.ToLower().Trim(),
                    }, false).ConfigureAwait(false));

                    if (!Request.IsAjaxRequest())
                    {
                        Response.Redirect("/Home");
                    }

                    return Json(new RegistrationResponse()
                    {
                        Success = true,
                        ErrorMessage = String.Empty
                    });
                }
            }
            catch
            {
                // silent 
            }

            return Json(new RegistrationResponse()
            {
                Success = false,
                ErrorMessage = "We are unable to process your request. Please try again later"
            });
        }

        private string _getWelcomeEmailHtml(User user)
        {
            var model = new EmailModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.EmailAddress
            };

            return RenderRazorViewToString(this, "~/Views/Emails/Welcome.cshtml", model);
        }
    }
}