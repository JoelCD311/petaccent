﻿using BusinessLayer.Models;
using BusinessLayer.Services;
using DataLayer.Data.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SalesTemplate.Models
{
    public class CartModel : BaseModel
    {
        public List<Charity> Charities { get; set; }
        public int Charity { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public CartModel(bool populateBase = true) : base(populateBase)
        {
            if (populateBase)
            {
                var chtySvc = new CharityService();
                this.Charities = chtySvc.GetCharities().Result;
            }
            this.UsedCoupons = new List<StoreCredit>();
        }
    }
}