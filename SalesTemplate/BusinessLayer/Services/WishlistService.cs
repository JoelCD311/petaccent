﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace BusinessLayer.Services
{
    public class WishlistService
    {
        private WishlistRepository _repo { get; set; }

        public WishlistService()
        {
            _repo = new WishlistRepository();
        }

        public async Task<bool> UpdateWishlist(int userID, int productID, int? optionID)
        {
            return await _repo.UpdateWishlist(userID, productID, optionID).ConfigureAwait(false);
        }
        
        public async Task<bool> RemoveFromWishlist(int userID, int productID, int? optionID)
        {
            return await _repo.RemoveFromWishlist(userID, productID, optionID).ConfigureAwait(false);
        }

        public async Task<List<Wishlist>> GetWishlistRecords(int userID, int count)
        {
            var wishlist = await _repo.GetWishlistRecords(userID, count).ConfigureAwait(false);

            foreach (var wish in wishlist)
            {
                var props = DataLayer.Helpers.Helpers.FilterOptionSelection(wish.Product, wish.Product.ProductOptions.Where(x => x.ParentOptionID != null).Any() ?
                    wish.Product.ProductOptions.Where(x => x.ParentOptionID != null).FirstOrDefault() :
                    wish.Product.ProductOptions.FirstOrDefault());

                wish.Product.Price = props.Price;
                wish.Product.UndiscountedPrice = props.UndiscountedPrice;
                wish.Product.ImagePath = props.ImagePath;
            }

            return wishlist;
        }

        public async Task<Tuple<List<Wishlist>, int>> GetWishlistRecords(int userID, int count = 6, int page = 0, decimal pricelow = 0, decimal pricehigh = 1000, string column = "", string order = "", string category = "")
        {
            page = (page < 0 ? 0 : page); // make sure page is >= 0

            return await _repo.GetWishlistRecords(userID, count, page, pricelow, pricehigh, column, order, category).ConfigureAwait(false);
        }
    }
}
