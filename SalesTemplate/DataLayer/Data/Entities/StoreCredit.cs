﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class StoreCredit
    {
        public int ID { get; set; }

        [ForeignKey("User")]
        public int? UserID { get; set; }

        public string EmailAddress { get; set; }

        [ForeignKey("Type")]
        public int? TypeID { get; set; }
        public string CouponCode { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> Expires { get; set; }        
        public System.DateTime DateCreated { get; set; }

        public virtual AddressType Type { get; set; }
        public virtual User User { get; set; }
    }
}
