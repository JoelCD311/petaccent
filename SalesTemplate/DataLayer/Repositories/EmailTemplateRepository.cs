﻿using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;

namespace DataLayer.Repositories
{
    public class EmailTemplateRepository : BaseRepository
    {
        public EmailTemplateRepository()
        {
            _db = new SalesTemplateContext();
        }

        public async Task<EmailTemplate> GetTemplateByName(string templateName)
        {
            return await _db.EmailTemplates
                            .Where(x => x.Name == templateName)
                            .SingleOrDefaultAsync()
                            .ConfigureAwait(false);
        }

        public async Task<EmailTemplate> GetTemplateByID(int id)
        {
            return await _db.EmailTemplates
                            .Where(x => x.ID == id)
                            .SingleOrDefaultAsync()
                            .ConfigureAwait(false);
        }
    }
}
