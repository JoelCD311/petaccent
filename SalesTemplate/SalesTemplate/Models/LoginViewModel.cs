﻿namespace SalesTemplate.Models
{
    public class LoginViewModel : BaseModel
    {
        public LoginViewModel() : base(true) { }
        public LoginViewModel(bool populateBase = true) : base(populateBase) { }

        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }        
    }
}