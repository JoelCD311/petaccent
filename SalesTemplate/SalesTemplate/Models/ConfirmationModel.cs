﻿using DataLayer.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesTemplate.Models
{
    public class ConfirmationModel : BaseModel
    {
        public ShoppingCart Cart { get; set; }
    }
}