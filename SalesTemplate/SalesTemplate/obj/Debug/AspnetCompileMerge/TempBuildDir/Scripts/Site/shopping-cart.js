﻿$(document).on('ready', function () {
    if ($('#hdnApplyCredits').val() == 'True') {
        $('#chkbxUserCredits').prop('checked', true);
    }

    if ($('#hdnAuthenticated').val() == 'True') {
        if ($('#hdnCardCount').val() > 0) {
            $('.payment-info-existing').show();
        }
        else {
            $('.payment-info-new').show();
        }

        $('.order-btn-container').show();
    }

    updateCartTotal();
});
$('#guest').on('click', function () {
    $('.continue-prompt').fadeOut('fast', function () {
        $('.shipping-info, .billing-info, .payment-info-new, .order-btn-container').fadeIn('fast');
    });
});
$('#chkbxDifferentCard').on('click', function () {
    if ($(this).is(':checked')) {
        $('.payment-info-existing').fadeOut(function () {
            $('.payment-info-new').fadeIn();
            $('html, body').animate({
                scrollTop: $(window).scrollTop() + 250
            })
        });
    }
    else {
        $('.payment-info-new').fadeOut(function () {
            $('.payment-info-existing').fadeIn();
        });
    }
});
$('#confirm-modal').on('hidden.bs.modal', function (e) {
    $(this).data('bs.modal').options.keyboard = true;
    $(this).data('bs.modal').options.backdrop = true;
    $('button.close', $(this)).show();
});
$('#chkbx-sas').on('click', function () {
    if ($(this).is(':checked')) {
        $('.billing-address').fadeOut('fast');
    }
    else {
        $('.billing-address').fadeIn('fast');
    }
});
$('.cart-right').on('click', '#chkbxUserCredits', function (e) {
    e.stopImmediatePropagation();

    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: '/Cart/ApplyCredits',
        data: {
            __RequestVerificationToken: token,
            applyCredits: $('#chkbxUserCredits').is(':checked')
        },
        type: 'POST',
        success: function (result) {
            updateCartPage(result);
            updateCartTotal();
        }
    });

});
$('.cart-left').on('click', '.delete-item', function (e) {
    e.stopImmediatePropagation();

    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: '/Cart/UpdateAndCalculateShoppingCart',
        data: {
            __RequestVerificationToken: token,
            applyCredits: $('#chkbxUserCredits').is(':checked'),
            productID: $(this).attr('id'),
            quantity: 0,
            optionID: 0
        },
        type: 'POST',
        success: function (result) {
            updateCartPage(result);
            updateCartTotalCartPage();
        }
    });
});
$('.cart-left').on('click', '.update-cart', function () {
    var id = $(this).prev('div').find('.cart-quantity').attr('id');
    var quantity = $(this).prev('div').find('.cart-quantity').val();
    var optionID = $(this).parent().parent().siblings('.cart-product').find('.options').attr('options-data');
    if (id && id != '') {
        updateCart(id, quantity, optionID);
        updateCartTotalCartPage();
    }
});
$('.cart-left').on('keydown', '.cart-quantity', function (e) {
    if (e.keyCode == 13) {
        var id = $(this).attr('id');
        var quantity = $(this).val();
        var optionID = $(this).parent().parent().parent().siblings('.cart-product').find('.options').attr('options-data');
        if (id && id != '') {
            updateCart(id, quantity, optionID);
            updateCartTotalCartPage();
        }            
    }
});
$(document).on('click', '#already-login', function () {
    if ($('#hdnAuthenticated').val() == 'False') {
        $('body').detach('#register-modal');
        //$('.promise-banner-main').fadeOut();
        $('#login-modal').appendTo('body');
        $('#login-modal').modal('show');

        if ($('#liEmailAddress').val() != '') {
            $('#liPassword').focus();
        }
        else {
            $('#liEmailAddress').focus();
        }  
    }
});
$(document).on('click', '#place-order', function () {
    if (validate() && !cardExists()) {
        $('#order-total').html($('#hdnOrderTotal').val());
        $('#shippingName').html($('[name="ShoppingCart.Order.ShippingName"]').val());
        $('#shippingStreet1').html($('[name="ShoppingCart.Order.ShippingStreet1"]').val());
        $('#shippingStreet2').html($('[name="ShoppingCart.Order.ShippingStreet2"]').val());
        $('#shippingCityStateZip').html($('[name="ShoppingCart.Order.ShippingCity"]').val() + ', ' + 
            $('[name="State"]').val() + ', ' + 
            $('[name="ShoppingCart.Order.ShippingPostalCode"]').val());
        $('#shippingCountry').html($('[name="Country"]').text());
        $('#confirm-modal').on('shown.bs.modal', function () {
            $(this).css('z-index', '100000')
        }).on('hidden', function () {
            $(this).css('z-index', '0')
        }).modal('show');
    }
});
$(document).on('click', '#btn-confirm-place-order', function () {    
    showWait(true);
    
    var nameOnCard, pan, expMonth, expYear, cardZip, cardCvc = '';
    var cardID = 0;

    if ($('.payment-info-existing').is(':visible')) {
        cardID = $('[name="ddCards"]').val();
    }
    else {
        nameOnCard = $('[name="ShoppingCart.Order.BillingInfo.NameOnCard"]').val();
        pan = $('[name="ShoppingCart.Order.BillingInfo.PAN"]').val();
        expMonth = $('[name="expMonth"]').val();
        expYear = $('[name="expYear"]').val();
        cardZip = $('[name="ShoppingCart.Order.BillingInfo.CardAddressZip"]').val();
        cardCvc = $('[name="ShoppingCart.Order.BillingInfo.CardCvc"]').val();
    }

    var name = $('[name="ShoppingCart.Order.ShippingName"]').val();
    var street1 = $('[name="ShoppingCart.Order.ShippingStreet1"]').val();
    var street2 = $('[name="ShoppingCart.Order.ShippingStreet2"]').val();
    var city = $('[name="ShoppingCart.Order.ShippingCity"]').val();
    var state = $('[name="State"]').val();
    var zip = $('[name="ShoppingCart.Order.ShippingPostalCode"]').val();
    var country = $('[name="Country"]').val();
    var emailAddress = ($('.payment-info-existing').is(':visible') ? 
        $('[name="ShoppingCart.Order.EmailAddress"]').val() : 
        $('#emailAddressNew').val());
    var charity = $('.selected-charity').val();
    var saveCard = $('#chkbxSaveCard').is(':checked');
    var isGift = $('#chkbxIsGift').is(':checked');
    
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();

    $.ajax({
        url: '/Cart/PlaceOrder',
        data: {
            __RequestVerificationToken: token,
            order: {
                ShippingName: name,
                ShippingStreet1: street1,
                ShippingStreet2: street2,
                ShippingCity: city,
                ShippingState: state,
                ShippingPostalCode: zip,
                ShippingCountry: country,
                IsGift: isGift,
                EmailAddress: emailAddress,
                CharityID: charity,
                BillingInfo: {
                    CardID: cardID,
                    CardAddressZip: cardZip,
                    CardCvc: cardCvc,
                    CardExpirationMonth: expMonth,
                    CardExpirationYear: expYear,
                    NameOnCard: nameOnCard,
                    PAN: pan,
                    SaveCard: saveCard
                }
            }
        },
        type: 'POST',
        success: function (result) {
                
            $('.confirm-modal-footer').css('height', '');                
            $('.wait-modal-body').hide();
            $('.complete-modal-body').show();

            if (result.Success) {
                $('#btn-confirm-place-order').hide();
                $('#btn-complete-order').show();
                $('.modal-title').html('Success!');
                $('.order-message').html(result.Message + '<div style="margin-top: 15px;">' +
                    '<div style="font-weight: 600; margin-bottom: 5px;">Order Number</div>' +
                    '<h1>' + result.OrderID + '</h1></div>');
            }
            else {
                $('#btn-confirm-place-order').hide();
                $('#btn-order-fail').show();

                $('#confirm-modal').data('bs.modal').options.keyboard = true;
                $('#confirm-modal').data('bs.modal').options.backdrop = true;
                $('#confirm-modal button.close').show();

                $('#confirm-modal').on('hidden.bs.modal', function () {
                    showNormal();
                });

                $('.modal-title').html('Error :(');
                $('.order-message').html(result.Message);
            }
        }
    });
});
$(document).on('click', '#btn-complete-order', function () {
    window.location = '/Cart/Empty';
});
$(document).on('click', '#btn-order-fail', function () {
    $('#confirm-modal').modal('hide');
});
$(document).on('click', '#chkbxSaveCard', function () {
    if ($(this).is(':checked')) {
        if (cardExists()) {
            $(this).notify('This card is already saved!', { elementPosition: 'top center' });
            $(this).prop('checked', false);
        }
    }
});
$(document).on('blur', '.cardinfo', function () {
    var elem = $('#chkbxSaveCard');
    if (elem.is(':checked')) {
        if (cardExists()) {
            elem.notify('This card is already saved!', { elementPosition: 'top center' });
            elem.prop('checked', false);
        }
    }
});
function showWait() {        
    $('#confirm-modal').data('bs.modal').options.keyboard = false;
    $('#confirm-modal').data('bs.modal').options.backdrop = 'static';
    $('#confirm-modal button.close').hide();
    $('.confirm-modal-footer').css('height', '50px')
    $('#btn-confirm-place-order').hide();
    $('.confirm-modal-body').hide();
    $('.wait-modal-body').show();
    $('.order-message').html('');
    $('.modal-title').html('Please Wait...');
}
function showNormal() {
    $('.confirm-modal-footer').css('height', '')
    $('#btn-confirm-place-order').show();
    $('#btn-order-fail').hide();
    $('#btn-complete-order').hide();
    $('.confirm-modal-body').show();
    $('.wait-modal-body').hide();
    $('.order-message').html('');
    $('.modal-title').html('Confirm Order');
}
function updateCart(id, quantity, optionID) {
    
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: '/Cart/UpdateAndCalculateShoppingCart',
        data: {
            __RequestVerificationToken: token,
            applyCredits: $('#chkbxUserCredits').is(':checked'),
            productID: id,
            quantity: quantity,
            optionID: optionID
        },
        type: 'POST',
        success: function (result) {
            updateCartPage(result, id);
            updateCartTotalCartPage();
        }
    });
}
function updateCartPage(result, id) {
    if (result.FullPage) {
        $('.cart-layout-container').html(result.FullPage); 
    }
    else {
        if (result.ListHtml && result.TotalsHtml) {
            $('.cart-left').html(result.ListHtml);
            $('#totalsDiv').html(result.TotalsHtml);
        }
        else if (result.TotalsHtml && !result.ListHtml) {

            $('#totalsDiv').html(result.TotalsHtml);

            if (result.ErrorMsg) {
                var txtboxQuan = $('.cart-left').find('#' + id + '.cart-quantity');
                var outer = txtboxQuan.parent().prev('div');
                txtboxQuan.val(result.QuantityAvailable);
                outer.notify(result.ErrorMsg, { elementPosition: 'top center' });
            }
        }
    }
}
function validate() {
    var charity = ($('.charities-dropdown').prop('selected-id') == '' ? $('#hdnDefaultCharity').val() : $('.charities-dropdown').prop('selected-id'));
    var valid = true;
    var name = $('[name="ShoppingCart.Order.ShippingName"]');
    var street1 = $('[name="ShoppingCart.Order.ShippingStreet1"]');
    var city = $('[name="ShoppingCart.Order.ShippingCity"]');
    var state = $('[name="State"]');
    var zip = $('[name="ShoppingCart.Order.ShippingPostalCode"]');
    var country = $('[name="Country"]');

    if (name.val() == "") {
        valid = false;
        name.closest('div').addClass('has-error');
        name.next().addClass('glyphicon-remove');
    }

    if (street1.val() == "") {
        valid = false;
        street1.closest('div').addClass('has-error');
        street1.next().addClass('glyphicon-remove');
    }

    if (city.val() == "") {
        valid = false;
        city.closest('div').addClass('has-error');
        city.next().addClass('glyphicon-remove');
    }

    if (state.val() == "0") {
        valid = false;
        state.closest('div').addClass('has-error');
        state.next().addClass('glyphicon-remove');
    }

    if (zip.val() == "") {
        valid = false;
        zip.closest('div').addClass('has-error');
        zip.next().addClass('glyphicon-remove');
    }
    else {
        if (!isValidPostalCode(zip.val())) {
            zip.closest('div').addClass('has-error');
            zip.next().addClass('glyphicon-remove');
        }
    }

    if (country.val() == "") {
        valid = false;
        country.closest('div').addClass('has-error');
        country.next().addClass('glyphicon-remove');
    }

    if ($('.payment-info-new').is(':visible')) {

        var nameOnCard = $('#nameOnCard');
        var pan = $('#pan');
        var expMonth = $('#expMonth');
        var expYear = $('#expYear');
        var cardZip = $('#zip');
        var cardCvc = $('#cvc');
        var emailAddress = $('#emailAddress');

        if (nameOnCard.val() == "") {
            valid = false;
            nameOnCard.closest('div').addClass('has-error');
            nameOnCard.next().addClass('glyphicon-remove');
        }

        if (pan.val() == "") {
            valid = false;
            pan.closest('div').addClass('has-error');
            pan.next().addClass('glyphicon-remove');
        }

        if (expMonth.val() == "") {
            valid = false;
            expMonth.closest('div').addClass('has-error');
            expMonth.next().addClass('glyphicon-remove');
        }

        if (expYear.val() == "") {
            valid = false;
            expYear.closest('div').addClass('has-error');
            expYear.next().addClass('glyphicon-remove');
        }

        if (cardZip.val() == "") {
            valid = false;
            cardZip.closest('div').addClass('has-error');
            cardZip.next().addClass('glyphicon-remove');
        }
        else {
            if (!isValidPostalCode(cardZip.val())) {
                valid = false;
                cardZip.closest('div').addClass('has-error');
                cardZip.next().addClass('glyphicon-remove');
            }
        }

        if (cardCvc.val() == "") {
            valid = false;
            cardCvc.closest('div').addClass('has-error');
            cardCvc.next().addClass('glyphicon-remove');
        }

        if (emailAddress.val() == "") {
            valid = false;
            emailAddress.closest('div').addClass('has-error');
            emailAddress.next().addClass('glyphicon-remove');
        }
    }
    else {
        var cards = $('[name="ddCards"]');
        var emailAddress = $('[name="ShoppingCart.Order.EmailAddress"]');

        if (cards.val() == 0) {
            valid = false;
            cards.closest('div').addClass('has-error');
            cards.next().addClass('glyphicon-remove');
        }

        if (emailAddress.val() == "") {
            valid = false;
            emailAddress.closest('div').addClass('has-error');
            emailAddress.next().addClass('glyphicon-remove');
        }
    }        

    return valid;
}
function cardExists() {
    var nameOnCard = $('[name="ShoppingCart.Order.BillingInfo.NameOnCard"]').val();
    var pan = $('[name="ShoppingCart.Order.BillingInfo.PAN"]').val();
    var expMonth = $('[name="expMonth"]').val();
    var expYear = $('[name="expYear"]').val();
    var cardZip = $('[name="ShoppingCart.Order.BillingInfo.CardAddressZip"]').val();
    var cardCvc = $('[name="ShoppingCart.Order.BillingInfo.CardCvc"]').val();
    var that = $(this);
    var retval = false;
    if (nameOnCard != '' && pan != '' && expMonth != '' && expYear != '' && cardZip != '' && cardCvc != '') {
        $('#ddCards option').each(function () {
            var org = this.text.toUpperCase();
            var comp = nameOnCard.trim().toUpperCase();
            if (~org.indexOf(nameOnCard.trim().toUpperCase()) &&
                ~org.indexOf(pan.trim().substr(pan.length - 4)) &&
                ~org.indexOf(("0" + expMonth.trim()).slice(-2) + '/' + expYear.trim().substr(expYear.trim().length - 2))) {
                retval = true;
                return;
            }
        });
    }
    
    return retval;
}
function updateCartTotalCartPage() {
    var totalItemsInCart = $('#hdnCartTotal').val();

    if (totalItemsInCart > 0) {
        $('.cart-count').html(totalItemsInCart);
    }
    else {
        $('.cart-count').html('');
    }
}
