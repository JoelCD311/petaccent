﻿using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace DataLayer.Data.Models
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {

        }

        public ShoppingCart(Order order)
        {
            this.Order = order;
            this.Order.CouponCodes = new List<string>();
        }
        public Order Order { get; set; }
    }
}
