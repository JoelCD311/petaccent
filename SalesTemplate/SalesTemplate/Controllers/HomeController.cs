﻿using BusinessLayer.Extensions;
using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    [AllowAnonymous]    
    public class HomeController : BaseController
    {
        public async Task<ActionResult> Index(string ac = null)
        {
            var model = new HomeModel();
            var sessionSvc = new SessionService();

            if (Request.Cookies[RememberUsernameCookieName] != null)
                ViewBag.EmailAddress = Request.Cookies[RememberUsernameCookieName].Value;

            checkCampaignCookie(ac);

            var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            model.FirstName = User.FirstName;
            model.CartTotal = cart.Order.OrderLineItems.Sum(x => x.Quantity);
            model.ShoppingCart = cart;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> EmailRequested()
        {
            var emailSvc = new EmailService();
            var requested = await emailSvc.IsIPInEmailRequestConversion(Request.GetClientIpAddress())
                .ConfigureAwait(false);
            return Json(requested);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GatherEmail(string email)
        {
            try
            {
                var emailSvc = new EmailService();

                if (IsValidEmailAddress(email))
                {
                    var task1 = EmailInUse(email);
                    var task2 = EmailInReqConv(email, Request.GetClientIpAddress());
                    await Task.WhenAll(task1, task2).ConfigureAwait(false);

                    if (!task1.Result && task2.Result.Item2 != "email")
                    {
                        if (await VerifyEmailAddress(email).ConfigureAwait(false))
                        {
                            var reward = Convert.ToDecimal(ConfigurationManager.AppSettings["CurrentPromotionAmount"]);
                            var storeCreditSvc = new StoreCreditService();

                            var type = await storeCreditSvc.GetStoreCreditTypeByName("Coupon").ConfigureAwait(false);
                            var coupon = await storeCreditSvc.CreateCouponCode().ConfigureAwait(false);
                            var recordTask = emailSvc.RecordEmailAddress(email, Request.GetClientIpAddress(), (task2.Result.Item2 == "ipaddress" ? 0 : reward));

                            Task creditTask = null;
                            if (!task2.Result.Item1)
                                creditTask = storeCreditSvc.WriteStoreCreditGrantedRecord(null, type.ID, reward, "Email Request Granted", "", email, coupon);

                            if (creditTask != null)
                                await Task.WhenAll(recordTask, creditTask).ConfigureAwait(false);
                            else
                                await Task.WhenAll(recordTask).ConfigureAwait(false);

                            if (task2.Result.Item2 != "ipaddress")
                            {
                                return Json(new
                                {
                                    Success = true,
                                    Message = "Thank you! Your $" + ConfigurationManager.AppSettings["CurrentPromotionAmount"] + " coupon has been created!",
                                    CouponCode = coupon
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    Success = true,
                                    Message = "Thank you! We will keep you updated with our latest updates and products.",
                                    CouponCode = String.Empty
                                });
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                Success = false,
                                Message = "Invalid email address"
                            });
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = "We already have your email address on file! We will keep you updated with our latest updates and products. Thank you!"
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "Invalid email address"
                    });
                }
            }
            catch (Exception ex)
            {
                string test = "";
            }

            return null;
        }

        [HttpGet]
        public async Task<ActionResult> Error()
        {
            var sessionSvc = new SessionService();
            var model = new BaseModel();
            model.ShoppingCart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            return View("Error", model);
        }

        private void checkCampaignCookie(string ac)
        {
            if (!String.IsNullOrEmpty(ac))
            {
                if (Request.Cookies[CampaignNameCookieName] != null)
                {
                    var cookie = Request.Cookies[CampaignNameCookieName] as HttpCookie;
                    cookie.Value = ac + DateTime.Now.ToString(":yyyyMMddhhmmfff");
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    // add cookie to track conversion of actual sales
                    var cookie = new HttpCookie(CampaignNameCookieName);
                    cookie.Value = ac + DateTime.Now.ToString(":yyyyMMddhhmmfff");
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(cookie);
                }
            }
            else
            {
                // make sure there's no tracking cookie
                if (Request.Cookies[CampaignNameCookieName] != null)
                {
                    var cookie = Request.Cookies[CampaignNameCookieName] as HttpCookie;
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);
                }
            }
        }
    }    
}