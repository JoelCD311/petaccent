﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using System.Configuration;

namespace DataLayer.Repositories
{
    public class WishlistRepository : BaseRepository
    { 
        public async Task<List<Wishlist>> GetWishlistRecords(int userID, int count)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return await _db.WishListRecords
                                    .AsNoTracking()
                                    .Include(x => x.Product)
                                    .Include(x => x.Product.ProductOptions)
                                    .Where(x => x.UserID == userID)                                   
                                    .Take(count)
                                    .ToListAsync();
                }
            }
            catch
            {
                // silent
            }

            return new List<Wishlist>();
        }
        public async Task<Tuple<List<Wishlist>, int>> GetWishlistRecords(int userID, int count = 6, int page = 0,
            decimal pricelow = 0, decimal pricehigh = 1000, string column = "", string order = "", string category = "")
        {
            try
            {
                Task<List<Wishlist>> task1;
                Task<int> task2;
                var _db = new SalesTemplateContext();
                var _db2 = new SalesTemplateContext();

                if (category != "" && category != "all")
                {
                    task1 = Task.Run(() =>
                    {
                        return _db.WishListRecords
                                  .Include(x => x.Product)
                                  .Include(x => x.Product.ProductOptions)
                                  .Where(x => 
                                    (x.UserID == userID) &&
                                    (x.Product.SubCategory == category) &&
                                    (x.Product.Price != null && x.Product.Price >= pricelow && x.Product.Price <= pricehigh ||
                                        x.Product.ProductOptions.Any(y => y.Price >= pricelow) && x.Product.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.Product.QuantityAvailable > 0 || x.Product.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                  .OrderBy("Product." + column + " " + order)
                                  .Skip(count * page)
                                  .Take(count)
                                  .ToList();
                    });

                    task2 = Task.Run(() =>
                    {
                        return _db2.WishListRecords
                                   .AsNoTracking()
                                   .Where(x => x.UserID == userID &&
                                    (x.Product.SubCategory == category) &&
                                    (x.Product.Price != null && x.Product.Price >= pricelow && x.Product.Price <= pricehigh ||
                                        x.Product.ProductOptions.Any(y => y.Price >= pricelow) && x.Product.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.Product.QuantityAvailable > 0 || x.Product.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                   .Count();
                    });
                }
                else
                {
                    task1 = Task.Run(() =>
                    {
                        return _db.WishListRecords
                                  .Include(x => x.Product)
                                  .Include(x => x.Product.ProductOptions)
                                  .Where(x =>
                                    (x.UserID == userID) &&
                                    (x.Product.Price != null && x.Product.Price >= pricelow && x.Product.Price <= pricehigh ||
                                        x.Product.ProductOptions.Any(y => y.Price >= pricelow) && x.Product.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.Product.QuantityAvailable > 0 || x.Product.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                  .OrderBy("Product." + column + " " + order)
                                  .Skip(count * page)
                                  .Take(count)
                                  .ToList();
                    });

                    task2 = Task.Run(() =>
                    {
                        return _db2.WishListRecords
                                   .AsNoTracking()
                                   .Where(x => x.UserID == userID &&
                                    (x.Product.Price != null && x.Product.Price >= pricelow && x.Product.Price <= pricehigh ||
                                        x.Product.ProductOptions.Any(y => y.Price >= pricelow) && x.Product.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.Product.QuantityAvailable > 0 || x.Product.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                   .Count();
                    });
                }

                await Task.WhenAll(task1, task2).ConfigureAwait(false);

                _db.Dispose();
                _db2.Dispose();

                return new Tuple<List<Wishlist>, int>(task1.Result, task2.Result);
            }
            catch (Exception ex)
            {
                string test = "";
            }

            return new Tuple<List<Wishlist>, int>(new List<Wishlist>(), 0);
        }

        public async Task<bool> UpdateWishlist(int userID, int productID, int? optionID)
        {
            try
            {
                optionID = optionID == 0 ? null : optionID;
                using (_db = new SalesTemplateContext())
                {
                    var wishlist = new Wishlist()
                    {
                        DateCreated = DateTime.Now,
                        OptionID = optionID,
                        ProductID = productID,
                        Quantity = null,
                        UserID = userID
                    };

                    if (await _db.WishListRecords
                                 .Where(x => x.ProductID == productID && x.OptionID == optionID)
                                 .FirstOrDefaultAsync() == null)
                    {
                        _db.WishListRecords.Add(wishlist);
                    }

                    return await _db.SaveChangesAsync().ConfigureAwait(false) > 0 ? true : false;
                }
            }
            catch
            {
                // silent
            }

            return false;
        }

        public async Task<bool> RemoveFromWishlist(int userID, int productID, int? optionID)
        {
            try
            {
                optionID = optionID == 0 ? null : optionID;
                using (_db = new SalesTemplateContext())
                {
                    var record = await _db.WishListRecords
                                    .Where(x => x.ProductID == productID && x.OptionID == optionID)
                                    .FirstOrDefaultAsync();

                    if (record != null)
                    {
                        _db.WishListRecords.Remove(record);
                        return await _db.SaveChangesAsync().ConfigureAwait(false) > 0 ? true : false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch
            {
                // silent
            }

            return false;
        }
    }
}
