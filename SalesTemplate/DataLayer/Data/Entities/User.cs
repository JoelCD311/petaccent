﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class User
    {
        public User()
        {
            this.Addresses = new HashSet<Address>();
            this.Orders = new HashSet<Order>();
        }

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public Guid Salt { get; set; }
        public Nullable<decimal> Phone { get; set; }
        public bool Active { get; set; }
        public bool EmailSubscribed { get; set; }
        [ForeignKey("Charity")]
        public Nullable<int> PreferredCharityID { get; set; }
        public string PaymentProcessorID { get; set; }
        public System.DateTime DateCreated { get; set; }        

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual Charity Charity { get; set; }
    }
}
