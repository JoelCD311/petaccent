﻿using DataLayer.Repositories;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class TaxService
    {
        public async Task<decimal> GetTaxRateByState(string state)
        {
            var taxRateRepo = new TaxRateRepository();
            var taxRate = await taxRateRepo.GetTaxRateByState(state)
                .ConfigureAwait(false);

            if (taxRate != null)
                return taxRate.Rate;
            else
                return 0;
        }
    }
}
