﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using System.Configuration;

namespace DataLayer.Repositories
{
    public class ProductRepository : BaseRepository
    {
        private int _domainID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProductDomainID"]);
        
        public async Task<Product> GetProductByID(int id)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return DataLayer.Helpers.Helpers.FilterProductsAndOptions(await _db.Products
                                    .Include(x => x.ProductOptions)
                                    .Where(x => x.ID == id &&
                                           x.DomainID == _domainID)
                                    .ToListAsync()
                                    .ConfigureAwait(false)).FirstOrDefault();
                }
            }
            catch
            {

            }

            return new Product();
        }

        public async Task<List<Product>> GetFeaturedProducts()
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return DataLayer.Helpers.Helpers.FilterProductsAndOptions(await _db.Products
                                    .Include(x => x.ProductOptions)
                                    .Where(x => x.Featured &&
                                            x.DomainID == _domainID)
                                    .OrderBy(x => x.Order != null ? x.Order : 999999999)
                                    .ToListAsync()
                                    .ConfigureAwait(false));
                }
            }
            catch
            {
                // silent
            }

            return new List<Product>();
        }

        public async Task<List<Product>> GetNonFeaturedProducts()
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return DataLayer.Helpers.Helpers.FilterProductsAndOptions(await _db.Products
                                    .Include(x => x.ProductOptions)
                                    .Where(x => !x.Featured &&
                                            x.DomainID == _domainID)
                                    .OrderBy(x => x.Order != null ? x.Order : 999999999)
                                    .ToListAsync()
                                    .ConfigureAwait(false));
                }
            }
            catch
            {
                // silent
            }

            return new List<Product>();
        }

        public async Task<List<Product>> GetAllProducts()
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return DataLayer.Helpers.Helpers.FilterProductsAndOptions(await _db.Products
                                    .Include(x => x.ProductOptions)
                                    .Where(x => x.DomainID == _domainID)
                                    .OrderBy(x => x.Order != null ? x.Order : 999999999)
                                    .ToListAsync()
                                    .ConfigureAwait(false));
                }
            }
            catch (Exception ex)
            {
                // silent
            }

            return new List<Product>();
        }

        public async Task<List<Product>> GetProductsByCategory(string category)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    return DataLayer.Helpers.Helpers.FilterProductsAndOptions(await _db.Products
                                    .Include(x => x.ProductOptions)
                                    .Where(x => (x.Category == category ||
                                           x.Category == null) &&
                                           x.DomainID == _domainID)
                                    .OrderBy(x => x.Order ?? null)
                                    .ToListAsync()
                                    .ConfigureAwait(false));
                }
            }
            catch
            {
                // silent
            }

            return new List<Product>();
        }

        public async Task<Tuple<List<Product>, int>> GetProducts(int domain, int count = 6, int page = 0,
            decimal pricelow = 0, decimal pricehigh = 1000, string column = "", string order = "", string category = "")
        {
            try
            {
                Task<List<Product>> task1;
                Task<int> task2;
                var _db = new SalesTemplateContext();
                var _db2 = new SalesTemplateContext();

                if (category != "" && category != "all")
                {
                    task1 = _db.Products
                            .AsNoTracking()
                            .Include(x => x.ProductOptions)
                            .Where(x => x.DomainID == domain &&
                                    (x.SubCategory == category) &&
                                    (x.Price != null && x.Price >= pricelow && x.Price <= pricehigh ||
                                        x.ProductOptions.Any(y => y.Price >= pricelow) && x.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.QuantityAvailable > 0 || x.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                            .OrderBy(column + " " + order)
                            .Skip(count * page)
                            .Take(count)
                            .ToListAsync();

                    task2 = _db2.Products
                            .AsNoTracking()
                            .Include(x => x.ProductOptions)
                            .Where(x => x.DomainID == domain &&
                                    (x.SubCategory == category) &&
                                    (x.Price != null && x.Price >= pricelow && x.Price <= pricehigh ||
                                        x.ProductOptions.Any(y => y.Price >= pricelow) && x.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.QuantityAvailable > 0 || x.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                            .CountAsync();
                }
                else
                {
                    task1 = _db.Products
                                .AsNoTracking()
                                .Include(x => x.ProductOptions)
                                .Where(x => x.DomainID == domain &&
                                    ((x.Price >= pricelow && x.Price <= pricehigh) ||
                                        x.ProductOptions.Any(y => y.Price >= pricelow) && x.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.QuantityAvailable > 0 || x.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                .OrderBy(column + " " + order)
                                .Skip(count * page)
                                .Take(count)
                                .ToListAsync();

                    task2 = _db2.Products
                                .AsNoTracking()
                                .Include(x => x.ProductOptions)
                                .Where(x => x.DomainID == domain &&
                                    ((x.Price >= pricelow && x.Price <= pricehigh) ||
                                        x.ProductOptions.Any(y => y.Price >= pricelow) && x.ProductOptions.Any(y => y.Price <= pricehigh)) &&
                                    (x.QuantityAvailable > 0 || x.ProductOptions.Any(a => a.QuantityAvailable > 0)))
                                .CountAsync();
                }

                await Task.WhenAll(task1, task2).ConfigureAwait(false);

                _db.Dispose();
                _db2.Dispose();

                return new Tuple<List<Product>, int>(DataLayer.Helpers.Helpers.FilterProductsAndOptions(task1.Result), task2.Result);
            }
            catch (Exception ex)
            {
                return new Tuple<List<Product>, int>(new List<Product>(), 0);
            }
        }

        public async Task UpdateProductQuantity(int prodID, int[] childOptionIDs, int quantity)
        {
            try
            {
                // childOptionID is the most childest option if the option has a tree of parents
                using (var _db = new SalesTemplateContext())
                {
                    var prod = await _db.Products
                                        .Where(x => x.ID == prodID)
                                        .FirstOrDefaultAsync()
                                        .ConfigureAwait(false);

                    if (childOptionIDs.Length > 0)
                    {
                        var option = new ProductOption();

                        if (childOptionIDs.Length == 2)
                        {
                            option = await _db.ProductOptions
                                     .Where(x => childOptionIDs.Contains(x.ID) && x.ParentOptionID != null)
                                     .FirstOrDefaultAsync()
                                     .ConfigureAwait(false);
                        }
                        else if (childOptionIDs.Length == 1)
                        {
                            option = await _db.ProductOptions
                                     .Where(x => childOptionIDs.Contains(x.ID))
                                     .FirstOrDefaultAsync()
                                     .ConfigureAwait(false);
                        }

                        option.QuantityAvailable = (option.QuantityAvailable + quantity);

                        _db.ProductOptions.Attach(option);
                        _db.Entry(option).State = EntityState.Modified;
                        await _db.SaveChangesAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        prod.QuantityAvailable = (prod.QuantityAvailable + quantity);

                        _db.Products.Attach(prod);
                        _db.Entry(prod).State = EntityState.Modified;
                        await _db.SaveChangesAsync().ConfigureAwait(false);
                    }
                }
            }
            catch
            {
                // silent
            }
        }

        public async Task UpdateProduct(Product product)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    _db.Products.Attach(product);
                    _db.Entry(product).State = EntityState.Modified;
                    await _db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch
            {
                // silent
            }
        }

        public async Task<List<ProductOption>> GetProductOptions(int? optID)
        {
            try
            {
                if (optID != null)
                {
                    var list = new List<ProductOption>();
                    using (var _db = new SalesTemplateContext())
                    {
                        var child = await _db.ProductOptions.Where(x => x.ID == optID).FirstOrDefaultAsync();

                        if (child != null)
                        {
                            list.Add(child);
                            var parent = await _db.ProductOptions.Where(x => x.ID == child.ParentOptionID).FirstOrDefaultAsync();

                            if (parent != null)
                            {
                                list.Add(parent);
                                return list;
                            }
                            else
                                return list;
                        }
                        else
                            return new List<ProductOption>();
                    }
                }
                else
                    return new List<ProductOption>();
            }
            catch
            {
                // silent
            }

            return new List<ProductOption>();
        }

        public async Task<List<ProductOption>> GetProductChildOptionList(int prodID, int? parOptID)
        {
            try
            {
                using (var _db = new SalesTemplateContext())
                {
                    if (parOptID.HasValue)
                    {
                        return await _db.ProductOptions
                                        .Where(x => x.ProductID == prodID &&
                                                    x.ParentOptionID == parOptID &&
                                                    x.QuantityAvailable > 0)
                                        .ToListAsync().ConfigureAwait(false);
                    }
                }
            }
            catch
            {
                // silent
            }

            return null;
        }        
    }
}
