﻿var GLB_TOKEN = $('input[name="__RequestVerificationToken"]', $('#__AjaxAntiForgeryForm')).val();
var glb_elem;
var code = '';
var stf_product_name = '';

$(document).on('click', '.prod-opts', function () {
    window.location = '/Products'
});
$(document).on('click', '.delete-product-ss', function (e) {
    e.stopPropagation();
    glb_elem = $(this);
    $.post('/Cart/UpdateAndCalculateShoppingCart', getSSDeleteProductData(), setSnapshot);
    return false;
});
$(document).on('click', '.cart-checkout', function () {    
    window.location = '/Checkout';
});
$(document).on('focus', '.coupon-code', function () {
    $(this).val('');
    $('.coupon-code').removeClass('text-field-error');
    $('.coupon-code').parent().removeClass('has-error');
    $('.coupon-code').next().removeClass('glyphicon-remove');
});
$(document).on('click', '.coupon-btn', function () {
    glb_elem = $(this);
    if ($('.coupon-code').val() != '') {
        if (code != $('.coupon-code').val()) {
            $.post('/Cart/ApplyCouponCode', getCouponData(), updateTotals);
            code = $('.coupon-code').val();
        }
        else {
            alertify.error('Please try a different coupon');
        }
    }
    else {
        $('.coupon-code').addClass('text-field-error');
        $('.coupon-code').parent().addClass('has-error');
        $('.coupon-code').next().addClass('glyphicon-remove');
    }
});
$(document).on('keypress', '.coupon-code', function (e) {
    if (e.keyCode == 13) {
        glb_elem = $(this);
        if (code != glb_elem.val()) {
            if (glb_elem.val() != '') {
                $.post('/Cart/ApplyCouponCode', getCouponData(), updateTotals);
                code = glb_elem.val();
            }
            else {
                $('.coupon-code').addClass('text-field-error');
                $('.coupon-code').parent().addClass('has-error');
                $('.coupon-code').next().addClass('glyphicon-remove');
            }
        }
        else {
            alertify.error('Please try a different coupon');
        }
    }
});
$(document).on('click', '.applied-coupon-code', function () {
    glb_elem = $(this);
    $.post('/Cart/RemoveCouponCode', getRemoveCouponData(), updateTotalsRemoveCoupon);
});
$(document).on('change', '.charities-dropdown', function () {
    $.post('/Cart/SetSelectedCharity', { __RequestVerificationToken: GLB_TOKEN, charityID: $(this).val() });
});
$(document).on('keydown, keypress', '.zip', function (e) {
    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode != 45)) {
        e.preventDefault();
        return false;
    }
});
$(document).on('keydown, keypress', '.numeric', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        e.preventDefault();
        return false;
    }
});
$(document).on('click', '.login-logout', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    logout();
});
$(document).on('click', '.send-to-friend', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    stf_product_name = $(this).attr('data-product-name');
    sendToFriend($(this).attr('data-product-id'));
});
$(document).on('focus', '.ajs-body .stf-input', function (e) {
    removeBasicInputError($(this));
});
$(document).on('focus', '.ajs-body #erEmailAddress', function (e) {
    removeBasicInputError($(this));
});
$(document).on('keypress', '.ajs-body #erEmailAddress', function (e) {
    if (e.keyCode == 13) {
        if (validateEmailRequest($(this))) {
            recordEmail($(this), true);
        }
    }
});
$('#email-subscribe-form-ft').on('submit', function (e) {
    e.preventDefault();
    var email = $('.email-subscribe-footer');
    if (validateEmailRequest(email)) {
        recordEmail(email, false);
    }
});
var erDialog;
function requestCustomerEmail() {
    $.post('/Home/EmailRequested', { __RequestVerificationToken: GLB_TOKEN }, function (result) {
        if (!result) {
            erDialog = alertify.dialog('alert').set({
                transition: 'zoom',
                defaultFocus: 'cancel',
                pinnable: false,
                moveBounded: true,
                title: 'Get $' + $('[name="hdnCurrentPromoAmount"]').val() + ' Off Your Order!',
                closable: true,
                label: 'Give me my $' + $('[name="hdnCurrentPromoAmount"]').val() + '!',
                message: $('.email-request-html').html(),
                onok: function (result) {
                    if (validateEmailRequest($('.ajs-body #erEmailAddress'))) {
                        recordEmail($('.ajs-body #erEmailAddress'), true);
                    }
                    return false;
                }
            }).show();
            
        }
    });
}
function recordEmail(email, isDialog) {

    var button;
    
    if (isDialog) {
        button = $('.ajs-button.ajs-ok');
        
        button.html($('.wait-spinner-record-email').html());        
        button.removeClass('ajs-ok');
        button.removeClass('ajs-button');
        button.addClass('ajs-wait-button');
    }

    $.ajax({
        url: '/Home/GatherEmail',
        type: 'POST',
        data: {
            __RequestVerificationToken: GLB_TOKEN,
            email: email.val()
        },
        success: function (result) {
            
            if (result.Success) {

                $('.coupon').html(result.CouponCode);

                alertify.success(result.Message);
                sessionStorage.totclka = 1;
                sessionStorage.totclkh = 1;

                if (result.CouponCode != '') {

                    if (isDialog) {

                        erDialog.set({
                            message: $('.email-request-success').html(),
                            label: 'OK',
                            onok: function () {
                                alertify.closeAll();
                            }
                        });

                        button.removeClass('ajs-wait-button');
                        button.addClass('ajs-button');
                        button.addClass('ajs-ok');
                    }
                    else {

                        erDialog = alertify.dialog('alert').set({
                            transition: 'zoom',
                            defaultFocus: 'cancel',
                            pinnable: false,
                            moveBounded: true,
                            title: 'Get $' + $('[name="hdnCurrentPromoAmount"]').val() + ' Off Your Order!',
                            closable: true,
                            message: $('.email-request-success').html(),
                            label: 'OK',
                            onok: function () {
                                alertify.closeAll();
                            }
                        }).show();

                    }
                }

                email.val('');
            }
            else {
                alertify.error(result.Message);
            }
        }
    });
}
function sendToFriend(prodID) {
    alertify.dialog('confirm').set({
        transition: 'zoom',
        defaultFocus: 'cancel',
        pinnable: false,
        moveBounded: true,
        title: ('Send "' + (stf_product_name == '' ? 'item' : stf_product_name) + '" to a Friend!'),
        closable: false,
        message: $('#stf-html').html(),
        onok: function () {
            if (validateSTF()) {   
                
                $.post('/Products/SendToFriend', getSendToFriendData(prodID), function (result) {
                    if (result) {
                        alertify.success('Product suggestion has been sent!');
                    }
                });

                removeBasicInputError($('.ajs-body #stfRefName'));
                removeBasicInputError($('.ajs-body #stfName'));
                removeBasicInputError($('.ajs-body #stfEmail'));

                $('.ajs-body #stfName').val('');
                $('.ajs-body #stfEmail').val('');
            }
            else {
                return false;
            }
        },
        oncancel: function () {
            removeBasicInputError($('.ajs-body #stfRefName'));
            removeBasicInputError($('.ajs-body #stfName'));
            removeBasicInputError($('.ajs-body #stfEmail'));  
            $('.ajs-body #stfName').val('');
            $('.ajs-body #stfEmail').val('');
        }
    }).show();
}
function validateSTF() {
    var valid = true;
    var refname = $('.ajs-body #stfRefName');
    var name = $('.ajs-body #stfName');
    var email = $('.ajs-body #stfEmail');

    if (refname.val() == '') {
        valid = false;
        addBasicInputError(refname);
    }

    if (name.val() == '') {
        valid = false;
        addBasicInputError(name);
    }

    if (email.val() == '') {
        valid = false;
        addBasicInputError(email);
    }
    else {
        if (!isValidEmailAddress(email.val())) {
            valid = false;
            addBasicInputError(email);
        }
    }

    return valid;
}
function isValidPostalCode(code) {
    var postalExp = new RegExp('^[0-9]{5}(?:-[0-9]{4})?$');
    if (postalExp.test(code)) {
        return true;
    }
    return false;
}
function updateTotals(result) {
    if (result.ErrorMsg == '') {
        $('.order-totals').html(result.Totals);
        alertify.success('Coupon code has been applied!');
        code = '';
        $('.coupon-code').val('');
    }
    else {
        alertify.error(result.ErrorMsg);
    }
}
function updateTotalsRemoveCoupon(result) {
    $('.order-totals').html(result.Totals);
    alertify.error(result.Message);
    code = '';
    $('.coupon-code').val('');
}
function setSnapshot(result) {    
    $('.cart-count').html(result.CartCount);
    $('.shopping-cart-content').html(result.Snapshot);

    if ($('.order-totals').attr('class') != undefined) {
        $('.order-totals').html(result.Totals);
    }

    if (result.ErrorMsg != '' && result.ErrorMsg != undefined) {
        alertify.error(result.ErrorMsg);
    }
    else {
        alertify.success('Your shopping cart has been updated!');
    }
}
function getSSDeleteProductData() {

    return {
        __RequestVerificationToken: GLB_TOKEN,
        applyCredits: false,
        quantify: false,
        productID: glb_elem.parent().attr('data-product-id'),        
        optionID: glb_elem.parent().attr('data-option-id'),
        quantity: 0
    }
}
function getCouponData() {
    var productID = glb_elem.closest('.product[data-product-id]').attr('data-product-id');
    var optionID = glb_elem.val();
    return {
        __RequestVerificationToken: GLB_TOKEN,
        coupon: $('.coupon-code').val()
    }
}
function getRemoveCouponData() {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        coupon: glb_elem.attr('data-coupon-code')
    }
}
function getSendToFriendData(prodID) {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        productID: prodID,
        name: $('.ajs-body #stfName').val(),
        refName: $('.ajs-body #stfRefName').val(),
        refEmail: $('.ajs-body #stfRefEmail').val(),
        email: $('.ajs-body #stfEmail').val()
    }
}
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
function addBasicInputError(elem) {
    elem.closest('div').addClass('has-error');
    elem.next().addClass('glyphicon-remove');
}
function addNonTextError(elem) {
    elem.css('border', '1px solid #a94442');
    elem.closest('div').addClass('has-error');
    elem.next().addClass('glyphicon-remove');
}
function addErrorPasswordTopInput(elem) {
    elem.css('border', '1px solid #a94442');
    elem.closest('div').addClass('has-error');
    elem.next().addClass('glyphicon-remove');
}
function removeNonTextError(elem) {
    elem.css('border', '1px solid lightgray');
    elem.closest('div').removeClass('has-error');
    elem.next().removeClass('glyphicon-remove');
}
function removeErrorTopInput(elem) {
    if (elem == undefined) {
        $('.top-input').each(function () {
            $(this).closest('div').removeClass('has-error');
            $(this).next().removeClass('glyphicon-remove');
            $(this).css('border', '1px solid #9f98ad');
        });
    }
    else {
        elem.closest('div').removeClass('has-error');
        elem.next().removeClass('glyphicon-remove');
        elem.css('border', '1px solid #9f98ad');
    }
}
function removeBasicInputError(elem) {
    elem.closest('div').removeClass('has-error');
    elem.next().removeClass('glyphicon-remove');
}
function logout() {    
    if ($('[name="hdnIsAuthenticated"]').val() == 'True') {
        $.get('/Login/Logout', logoutResult);
    }
}
function logoutResult() {
    sessionStorage.totclkh = 1;
    sessionStorage.totclka = 1;
    window.location = '/Home';
}
function validateEmailRequest(email) {
    
    var valid = true;

    if (email.val() == '') {
        valid = false;
        addBasicInputError(email);
    }
    else {
        if (!isValidEmailAddress(email.val())) {
            valid = false;
            addBasicInputError(email);
            alertify.error('Invalid Email Address');
        }
    }

    return valid;
}