﻿using BusinessLayer.Models;
using BusinessLayer.Services;
using DataLayer.Data.Models;
using DataLayer.Data.Models.ViewModels;
using SalesTemplate.Filters;
using SalesTemplate.Infrastructure;
using SalesTemplate.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    [Secure]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    public class BaseController : Controller
    {
        protected readonly string RememberUsernameCookieName = "PetAccentRememberUsername";
        protected readonly string CampaignNameCookieName = "PetAccentCvrs";

        protected async Task<List<CreditCardModel>> GetCreditCards(string processorID)
        {
            var pmtSrv = new PaymentService();
            return await pmtSrv.GetCreditCards(processorID).ConfigureAwait(false);
        }

        protected virtual new SalesPrincipal User
        {
            get { return new SalesPrincipal(); }
        }

        [AllowAnonymous]
        public new JsonResult Json(object data)
        {
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        protected string RenderRazorViewToString(Controller controller, string viewName, object model)
        {
            try
            {
                if (string.IsNullOrEmpty(viewName))
                    viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

                controller.ViewData.Model = model;

                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                // silent
            }

            return String.Empty;
        }

        protected async Task<bool> VerifyEmailAddress(string email)
        {
            var emailSvc = new EmailService();
            return await emailSvc.VerifyEmailAddress(email).ConfigureAwait(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> UpdateProductPane(int productID, int optionID)
        {
            var prodSvc = new ProductService();
            var selection = await prodSvc.GetUpdatedOptionSelection(productID, optionID).ConfigureAwait(false);
            string viewPath = String.Empty;

            if (this.ControllerContext.RouteData.Values["controller"].ToString() == "Cart")
                viewPath = "~/Views/Shared/_ChildOptionList.cshtml";
            else if (this.ControllerContext.RouteData.Values["controller"].ToString() == "Products")
                viewPath = "~/Views/Products/_ChildOptionList.cshtml";

                return Json(new
            {
                ChildList = (selection.Options != null ?
                    this.RenderRazorViewToString(this, viewPath, selection.Options) : ""),
                ImagePath = selection.ImagePath,
                Price = selection.Price,
                UndiscountedPrice = selection.UndiscountedPrice
            });
        }

        protected bool IsValidEmailAddress(string email)
        {
            try
            {
                new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected async Task<bool> EmailInUse(string email)
        {
            var userSvc = new UserService();
            return await userSvc.EmailAddressInUse(email).ConfigureAwait(false);
        }

        protected async Task<Tuple<bool, string>> EmailInReqConv(string email, string ipAddress)
        {
            var emailSvc = new EmailService();
            return await emailSvc.IsEmailInEmailRequestConversion(email, ipAddress).ConfigureAwait(false);
        }

        protected ShoppingCart MapOrderViewModelToCart(OrderInfoModel model, ShoppingCart cart)
        {
            // Card info
            cart.Order.BillingInfo = new BillingInfoModel()
            {
                CardAddressZip = model.BillingInfo.CardAddressZip,
                CardCvc = model.BillingInfo.CardCvc,
                CardExpirationMonth = model.BillingInfo.CardExpirationMonth,
                CardExpirationYear = model.BillingInfo.CardExpirationYear,
                NameOnCard = model.BillingInfo.NameOnCard,
                PAN = model.BillingInfo.PAN,
                CardID = model.BillingInfo.CardID
            };

            // Shipping info
            cart.Order.ShippingName = model.ShippingName;
            cart.Order.ShippingStreet1 = model.ShippingStreet1;
            cart.Order.ShippingStreet2 = model.ShippingStreet2;
            cart.Order.ShippingStreet3 = model.ShippingStreet3;
            cart.Order.ShippingCity = model.ShippingCity;
            cart.Order.ShippingState = model.ShippingState;
            cart.Order.ShippingPostalCode = model.ShippingPostalCode;
            cart.Order.ShippingCountry = model.ShippingCountry;
            cart.Order.ShippingCountryFull = model.ShippingCountryFull;
            cart.Order.ShippingNotes = model.ShippingNotes;
            cart.Order.IsGift = model.IsGift;
            cart.Order.EmailAddress = model.EmailAddress;
            cart.Order.CharityID = model.CharityID;

            return cart;
        }
    }
}