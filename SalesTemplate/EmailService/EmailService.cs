﻿using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using DataLayer.Data.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EmailServiceProject
{
    public static class OutgoingMail
    {
        public static async Task<SendRawEmailResponse> SendHttpEmail(User user, EmailTemplate template, bool isBlast, string unsubscribeHash = null)
        {
            MailMessage mailMessage = new MailMessage();
            RawMessage rawMessage = new RawMessage();
            var retval = new SendRawEmailResponse();

            mailMessage.From = new MailAddress(template.FromAddress, template.FromDisplayName);
            mailMessage.To.Add(new MailAddress(user.EmailAddress, user.FirstName + " " + user.LastName));
            mailMessage.Subject = template.Subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (isBlast)
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(unsubscribeHash);
                mailMessage.Headers.Add("List-Unsubscribe", ConfigurationManager.AppSettings["AWS_SQS_Unsubscribe"] + System.Convert.ToBase64String(plainTextBytes));
                template.Html = template.Html.Replace("#####", System.Convert.ToBase64String(plainTextBytes));
            }                

            if (!String.IsNullOrEmpty(template.PlainText))
            {
                // Important: Mime standard dictates that text version must come first 
                var textPart = AlternateView.CreateAlternateViewFromString(template.PlainText, null, "text/plain");
                textPart.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
                mailMessage.AlternateViews.Add(textPart);
                mailMessage.IsBodyHtml = false;
                mailMessage.Body = template.PlainText;
            }            

            var htmlPart = AlternateView.CreateAlternateViewFromString(template.Html, System.Text.Encoding.UTF8, "text/html");
            htmlPart.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
            mailMessage.AlternateViews.Add(htmlPart);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = template.Html;

            using (MemoryStream memoryStream = GetRawMessage(mailMessage))
            {
                rawMessage.Data = memoryStream;
            }

            SendRawEmailRequest rawRequest = new SendRawEmailRequest();
            rawRequest.RawMessage = rawMessage;

            var recipients = new List<string>();
            recipients.Add(user.EmailAddress);

            rawRequest.Destinations = recipients;
            rawRequest.Source = template.FromAddress;

            Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USWest2;
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(
                ConfigurationManager.AppSettings["AWSAccessKey"],
                ConfigurationManager.AppSettings["AWSSecretKey"],
                REGION);

            try
            {
                retval = await client.SendRawEmailAsync(rawRequest).ConfigureAwait(false);
            }
            catch
            {
                // silent
            }

            return retval;
        }

        public static async Task<SendRawEmailResponse> SendHttpEmail(User user, string html, string plainText, string fromAddress, string displayName, 
            string subject, bool isGift, bool isBlast, string unsubscribeHash = null)
        {
            MailMessage mailMessage = new MailMessage();
            RawMessage rawMessage = new RawMessage();
            var retval = new SendRawEmailResponse();

            mailMessage.From = new MailAddress(fromAddress, displayName);
            mailMessage.To.Add(new MailAddress(user.EmailAddress, !String.IsNullOrEmpty(user.LastName) ? (user.FirstName + " " + user.LastName) : isGift ? "Customer" : user.FirstName));
            mailMessage.Subject = subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;
            mailMessage.Bcc.Add(new MailAddress("service.petaccent@gmail.com"));

            if (isBlast)
                mailMessage.Headers.Add("List-Unsubscribe", ConfigurationManager.AppSettings["AWS_SQS_Unsubscribe"] + unsubscribeHash);

            if (!String.IsNullOrEmpty(plainText))
            {
                // Important: Mime standard dictates that text version must come first 
                var textPart = AlternateView.CreateAlternateViewFromString(plainText, null, "text/plain");
                textPart.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
                mailMessage.AlternateViews.Add(textPart);
                mailMessage.IsBodyHtml = false;
                mailMessage.Body = plainText;
            }            

            var htmlPart = AlternateView.CreateAlternateViewFromString(html, System.Text.Encoding.UTF8, "text/html");
            htmlPart.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
            mailMessage.AlternateViews.Add(htmlPart);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = html;

            using (MemoryStream memoryStream = GetRawMessage(mailMessage))
            {
                rawMessage.Data = memoryStream;
            }

            SendRawEmailRequest rawRequest = new SendRawEmailRequest();
            rawRequest.RawMessage = rawMessage;

            var recipients = new List<string>();
            recipients.Add(user.EmailAddress);

            rawRequest.Destinations = recipients;
            rawRequest.Source = fromAddress;

            Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USWest2;
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(
                ConfigurationManager.AppSettings["AWSAccessKey"],
                ConfigurationManager.AppSettings["AWSSecretKey"],
                REGION);

            try
            {
                retval = await client.SendRawEmailAsync(rawRequest).ConfigureAwait(false);
            }
            catch
            {
                // silent
            }

            return retval;
        }

        public static async Task<SendRawEmailResponse> SendHttpEmailToPetAccent(string name, string emailAddress, string subject, string message)
        {
            MailMessage mailMessage = new MailMessage();
            RawMessage rawMessage = new RawMessage();
            StringBuilder builder = new StringBuilder();
            var retval = new SendRawEmailResponse();

            builder.Append("Name: " + name + "<br/>");
            builder.Append("Email Address: " + emailAddress + "<br/>");
            builder.Append("Subject: " + subject + "<br/>");
            builder.Append("Message: " + message + "<br/><br/>");
            
            mailMessage.From = new MailAddress(emailAddress, "Customer from site");
            mailMessage.To.Add(new MailAddress("customerservice@petaccent.com"));
            mailMessage.Subject = subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;
            mailMessage.Bcc.Add(new MailAddress("service.petaccent@gmail.com"));

            var htmlPart = AlternateView.CreateAlternateViewFromString(builder.ToString(), System.Text.Encoding.UTF8, "text/html");
            htmlPart.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
            mailMessage.AlternateViews.Add(htmlPart);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = builder.ToString();

            using (MemoryStream memoryStream = GetRawMessage(mailMessage))
            {
                rawMessage.Data = memoryStream;
            }

            SendRawEmailRequest rawRequest = new SendRawEmailRequest();
            rawRequest.RawMessage = rawMessage;

            var recipients = new List<string>();
            recipients.Add(emailAddress);

            rawRequest.Destinations = recipients;
            rawRequest.Source = emailAddress;

            Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USWest2;
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(
                ConfigurationManager.AppSettings["AWSAccessKey"],
                ConfigurationManager.AppSettings["AWSSecretKey"],
                REGION);

            try
            {
                retval = await client.SendRawEmailAsync(rawRequest).ConfigureAwait(false);
            }
            catch
            {
                // silent
            }

            return retval;
        }

        private static MemoryStream GetRawMessage(this MailMessage self)
        {
            BindingFlags Flags = BindingFlags.Instance | BindingFlags.NonPublic;
            Type MailWriter = typeof(SmtpClient).Assembly.GetType("System.Net.Mail.MailWriter");
            ConstructorInfo MailWriterConstructor = MailWriter.GetConstructor(Flags, null, new[] { typeof(Stream) }, null);
            MethodInfo CloseMethod = MailWriter.GetMethod("Close", Flags);
            MethodInfo SendMethod = typeof(MailMessage).GetMethod("Send", Flags);

            /// <summary>
            /// A little hack to determine the number of parameters that we
            /// need to pass to the SaveMethod.
            /// </summary>
            bool IsRunningInDotNetFourPointFive = SendMethod.GetParameters().Length == 3;

            var result = new MemoryStream();
            var mailWriter = MailWriterConstructor.Invoke(new object[] { result });
            SendMethod.Invoke(self, Flags, null, IsRunningInDotNetFourPointFive ? new[] { mailWriter, true, true } : new[] { mailWriter, true }, null);
            result = new MemoryStream(result.ToArray());
            CloseMethod.Invoke(mailWriter, Flags, null, new object[] { }, null);
            return result;
        }

        private static void SendStandardHttpEmail(string SUBJECT, string BODY, string FROM, string TO)
        {
            Destination destination = new Destination();
            destination.ToAddresses = (new List<string>() { TO });

            Content subject = new Content(SUBJECT);
            Content textBody = new Content(BODY);
            Body body = new Body(textBody);

            Message message = new Message(subject, body);

            SendEmailRequest request = new SendEmailRequest(FROM, destination, message);

            Amazon.RegionEndpoint REGION = Amazon.RegionEndpoint.USWest2;

            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(
                ConfigurationManager.AppSettings["AWSAccessKey"],
                ConfigurationManager.AppSettings["AWSSecretKey"],
                REGION);

            try
            {
                client.SendEmail(request);
            }
            catch
            {
                // silent
            }
        }

        private static void EmailSmtp()
        {
            string SMTP_USERNAME = ConfigurationManager.AppSettings["AWS_SES_Smtp_Username"];
            string SMTP_PASSWORD = ConfigurationManager.AppSettings["AWS_SES_Smtp_Password"];
            string SMTP_HOST = ConfigurationManager.AppSettings["AWS_SES_Smtp_Host"];
            int SMTP_PORT = Convert.ToInt32(ConfigurationManager.AppSettings["AWS_SES_Smtp_Port"]);

            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(SMTP_HOST, SMTP_PORT))
            {
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                client.EnableSsl = true;

                try
                {
                    client.Send("FROM", "TO", "SUBJECT", "BODY");
                }
                catch
                {
                    // silent
                }
            }
        }
    }
}