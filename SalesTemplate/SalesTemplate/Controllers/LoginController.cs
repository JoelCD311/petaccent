﻿using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EmailServiceProject;
using DataLayer.Models;

namespace SalesTemplate.Controllers
{
    public class LoginController : BaseController
    {        
        protected readonly string ForgotPasswordEmailTemplateName = ConfigurationManager.AppSettings["ForgotPasswordEmailTemplateName"];

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(bool Redirect = false, string RedirectTo = "")
        {
            var model = new LoginViewModel();
            model.FirstName = User.FirstName;
            model.Redirect = Redirect;
            model.RedirectTo = RedirectTo;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Login(LoginViewModel model)
        {
            try
            {
                model.EmailAddress = model.EmailAddress.Trim();
                model.Password = model.Password.Trim();

                var userSvc = new UserService();
                var user = await userSvc.GetUserByEmailAddress(model.EmailAddress)
                    .ConfigureAwait(false);

                if (user != null)
                {
                    if (user.Active &&
                    EncryptionService.Encrypt(model.Password, user.Salt.ToString())
                        == user.Password)
                    {
                        Response.Cookies.Add(
                            await userSvc.LoginUser(user, model.RememberMe).ConfigureAwait(false)
                            );

                        if (model.RememberMe)
                        {
                            var cookie = new HttpCookie(this.RememberUsernameCookieName);
                            cookie.Expires = DateTime.Now.AddDays(10);
                            cookie.Value = model.EmailAddress;
                            Response.Cookies.Add(cookie);
                        }
                        else
                        {
                            var cookie = Request.Cookies[RememberUsernameCookieName];

                            if (cookie != null)
                            {
                                cookie.Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies.Add(cookie);
                            }
                        }

                        return Json(true);
                    }
                }                
            }
            catch { }

            return Json(false);
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JavaScriptResult Register(RegistrationViewModel model)
        {
            model.FirstName = model.FirstName.Trim();
            model.LastName = model.LastName.Trim();
            model.EmailAddress = model.EmailAddress.Trim();

            var sessionSvc = new SessionService();
            sessionSvc.RegistrationData(model);
            return JavaScript("window.location = '/Register'");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ForgotPassword(string username)
        {
            try
            {
                if (IsValidEmailAddress(username))
                {
                    if (await EmailInUse(username).ConfigureAwait(false))
                    {
                        var emailSvc = new EmailService();
                        var userSvc = new UserService();
                        var model = new EmailModel();

                        var user = await userSvc.GetUserByEmailAddress(username).ConfigureAwait(false);
                        model.Password = EncryptionService.Decrypt(user.Password, user.Salt.ToString());
                        model.FirstName = user.FirstName;
                        model.LastName = user.LastName;

                        await emailSvc.SendForgotPasswordEmail(user, 
                            this.RenderRazorViewToString(this, "~/Views/Emails/ForgotPassword.cshtml", model));
                    }
                }
            }
            catch
            {
                // silent 
            }

            return Json("If your email address exists in our database an email has been sent. It could take several minutes to reach your email account. Don't forget to check your spam folder. Thank you!");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task Logout()
        {
            try
            {
                //await unApplyCredits().ConfigureAwait(false);
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                //Session.RemoveAll();
                //Session.Abandon();
            }
            catch (Exception ex)
            {
                // silent
            }
        }
        
        private async Task unApplyCredits()
        {
            // Turn back off the application of credits
            if (HttpContext.Session != null)
            {
                var sessionSvc = new SessionService();
                var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);

                if (cart != null)
                {
                    cart.Order.ApplyCredits = false;
                    sessionSvc.UpdateShoppingCart(cart);
                    sessionSvc.ClearCouponCodes();
                }
            }
        }        
    }
}