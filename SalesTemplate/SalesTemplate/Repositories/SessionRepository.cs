﻿using DataLayer.Data.Models;
using System.Web;
using System.Web.SessionState;

namespace SalesTemplate.Repositories
{
    public class SessionRepository
    {
        private HttpSessionState _session;
        public SessionRepository()
        {
            _session = HttpContext.Current.Session;
        }
        public ShoppingCart Cart
        {
            get
            {
                return _session["ShoppingCart"] as ShoppingCart;
            }
            set
            {
                _session["ShoppingCart"] = value;
            }
        }
    }
}