﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class EmailRequestConversion
    {
        public int ID { get; set; }
        public string IPAddress { get; set; }
        public string EmailAddress { get; set; }
        public decimal CreditAwarded { get; set; }
        public bool AccountCreated { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
