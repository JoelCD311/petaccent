﻿using System.Collections.Generic;

namespace DataLayer.Data.Entities
{
    public partial class StoreCreditType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
}
