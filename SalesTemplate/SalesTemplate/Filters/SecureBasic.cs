﻿using BusinessLayer.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SalesTemplate.Filters
{
    public class SecureBasic : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext context)
        {
            var creds = context.RequestContext.HttpContext.Request.Headers.GetValues("Authentication");

            if (creds != null)
            {
                var parts = creds[0].Split(':');
                if (parts[0] != null)
                {
                    if (parts[0] != "JoelD311" || parts[1] != "73jkSfjDs8yrHweEo65iSo498fj#w29dfj")
                    {
                        context.Result = new HttpStatusCodeResult(401);
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        context.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    context.Result = new HttpStatusCodeResult(401);
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
            }
            else
            {
                context.Result = new HttpStatusCodeResult(401);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
        }
    }
}