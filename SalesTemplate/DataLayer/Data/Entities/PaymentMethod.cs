﻿namespace DataLayer.Data.Entities
{
    public partial class PaymentMethod
    {
        public int ID { get; set; }
        public string Method { get; set; }
    }
}
