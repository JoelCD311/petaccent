﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailBlastService.Models
{
    public class AwsFeedbackModel
    {
        /// <summary>Represents the bounce or complaint notification stored in Amazon SQS.</summary>
        public class AmazonSqsNotification
        {
            public string Type { get; set; }
            public string Message { get; set; }
            public string SubscribeURL { get; set; }
        }

        /// <summary>Represents an Amazon SES bounce notification.</summary>
        public class AmazonSesBounceNotification
        {
            public string NotificationType { get; set; }
            public AmazonSesBounce Bounce { get; set; }
            public AmazonSesMail Mail { get; set; }
        }

        public class AmazonSqsDeliveryNotification
        {
            public string NotificationType { get; set; }
            public AmazonSqsMail Mail { get; set; }
            public AmazonSqsDelivery Delivery { get; set; }
        }

        public class AmazonSqsMail
        {
            public string MessageID { get; set; }
            public string SourceARN { get; set; }
            public string Source { get; set; }
            public List<string> Destination { get; set; }
            public DateTime Timestamp { get; set; }
        }

        public class AmazonSqsDelivery
        {
            public DateTime TimeStamp { get; set; }
            public string SMTPResponse { get; set; }
            public string ReportingMTA { get; set; }
            public List<string> Recipients { get; set; }
        }

        public class AmazonSesMail
        {
            public string Source { get; set; }
            public string MessageID { get; set; }
            public string SourceARN { get; set; }
            public dynamic Headers { get; set; }
        }

        /// <summary>Represents meta data for the bounce notification from Amazon SES.</summary>
        public class AmazonSesBounce
        {
            public string BounceType { get; set; }
            public string BounceSubType { get; set; }
            public DateTime Timestamp { get; set; }
            public string FeedbackID { get; set; }
            public List<AmazonSesBouncedRecipient> BouncedRecipients { get; set; }
        }

        /// <summary>Represents the email address of recipients that bounced
        /// when sending from Amazon SES.</summary>
        public class AmazonSesBouncedRecipient
        {
            public string EmailAddress { get; set; }
        }

        /// <summary>Represents an Amazon SES complaint notification.</summary>
        public class AmazonSesComplaintNotification
        {
            public string NotificationType { get; set; }
            public AmazonSesComplaint Complaint { get; set; }
            public AmazonSesMail Mail { get; set; }
        }

        /// <summary>Represents the email address of individual recipients that complained 
        /// to Amazon SES.</summary>
        public class AmazonSesComplainedRecipient
        {
            public string EmailAddress { get; set; }
        }

        /// <summary>Represents meta data for the complaint notification from Amazon SES.</summary>
        public class AmazonSesComplaint
        {
            public List<AmazonSesComplainedRecipient> ComplainedRecipients { get; set; }
            public DateTime Timestamp { get; set; }
            public string FeedbackID { get; set; }
            public string UserAgent { get; set; }
            public string ComplaintFeedbackType { get; set; }
        }
    }
}