﻿using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;

namespace DataLayer.Repositories
{
    public class ProductDomainRepository : BaseRepository
    {
        public ProductDomainRepository()
        {
            _db = new SalesTemplateContext();
        }

        public async Task<ProductDomain> GetDomainNameAndDescByID(int id)
        {
            return await _db.ProductDomains
                            .Where(x => x.ID == id)
                            .SingleOrDefaultAsync()
                            .ConfigureAwait(false);
        }
    }
}
