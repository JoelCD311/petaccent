﻿using System.Web.Optimization;

namespace SalesTemplate
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/jquery.easing.1.3.js",
                        "~/Scripts/jquery.flexisel.js",
                        "~/Scripts/jquery.transit.js",
                        "~/Scripts/jquery.jcountdown.js",
                        "~/Scripts/jquery.jPages.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/tab.js",
                        "~/Scripts/responsiveslides.min.js",
                        "~/Scripts/jquery.elevateZoom-3.0.8.min.js",
                        "~/Scripts/jquery.themepunch.plugins.min.js",
                        "~/Scripts/jquery.themepunch.revolution.min.js",
                        "~/Scripts/jquery.scrollTo-1.4.2-min.js",
                        "~/Scripts/main.js",
                        "~/Scripts/alertify.min.js",
                        "~/Scripts/modernizr.js",
                        "~/Scripts/auto-complete.min.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/content/alertifyjs/alertify.min.css",
                      "~/content/alertifyjs/themes/default.min.css",
                      "~/Content/css/ie.style.css",
                      "~/Content/css/theme-style.css",
                      "~/Content/css/auto-complete.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
