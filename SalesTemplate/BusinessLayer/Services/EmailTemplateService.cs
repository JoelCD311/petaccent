﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class EmailTemplateService
    {
        private EmailTemplateRepository _repo { get; set; }

        public EmailTemplateService()
        {
            _repo = new EmailTemplateRepository();
        }

        public async Task<EmailTemplate> GetTemplateByName(string templateName)
        {
            return await _repo.GetTemplateByName(templateName)
                .ConfigureAwait(false);
        }
    }
}
