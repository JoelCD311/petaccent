﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class Address
    {
        public int ID { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        [ForeignKey("AddressType")]
        public int Type { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public System.DateTime DateCreated { get; set; }
        [NotMapped]
        public bool New { get; set; }

        public virtual AddressType AddressType { get; set; }
        public virtual User User { get; set; }
    }
}
