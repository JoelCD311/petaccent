﻿namespace BusinessLayer.Models
{
    public class OrderPlacementResponse
    {
        public bool Success { get; set; }
        public string OrderID { get; set; }
        public string Message { get; set; }
        public string OrderCompleteHtml { get; set; }
    }
}
