﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer.Services
{
    public class CampaignService
    {
        private CampaignRepository _repo { get; set; }

        public CampaignService()
        {
            _repo = new CampaignRepository();
        }

        public async Task<Campaign> GetCampaignByName(string name)
        {
            return await _repo.GetCampaignByName(name).ConfigureAwait(false);
        }
    }
}
