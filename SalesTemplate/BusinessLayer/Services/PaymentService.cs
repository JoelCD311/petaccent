﻿using System;
using System.Threading.Tasks;
using Stripe;
using System.Globalization;
using BusinessLayer.Models;
using DataLayer.Repositories;
using System.Configuration;
using DataLayer.Data.Models.ViewModels;
using System.Linq;
using DataLayer.Data.Models;
using System.Collections.Generic;
using DataLayer.Data.Entities;

namespace BusinessLayer.Services
{
    public class PaymentService
    {
        public PaymentService()
        {
            _stripe_api_key = Convert.ToBoolean(ConfigurationManager.AppSettings["TestEnv"]) ? 
                ConfigurationManager.AppSettings["Stripe:KeyDev"] : 
                ConfigurationManager.AppSettings["Stripe:KeyProd"];
        }

        #region Add Payment Vehicles

        private readonly string _stripe_api_key;

        public async Task<List<CreditCardModel>> AddCreditCard(CreditCardModel model, int userID)
        {
            var userSvc = new UserService();
            var user = await userSvc.GetUserByID(userID).ConfigureAwait(false);
                
            if (user != null)
            {
                var customerService = new StripeCustomerService(_stripe_api_key);
                StripeCustomer stripeCustomer = null;

                if (!String.IsNullOrEmpty(user.PaymentProcessorID))
                {
                    try
                    {
                        stripeCustomer = await customerService.GetAsync(user.PaymentProcessorID).ConfigureAwait(false);

                        var card = new StripeCardCreateOptions();
                        card.SourceCard = new SourceCard()
                        {
                            Number = model.Number,
                            ExpirationYear = model.ExpirationYear,
                            ExpirationMonth = model.ExpirationMonth,
                            AddressCountry = model.AddressCountry,
                            AddressLine1 = model.AddressLine1,
                            AddressLine2 = model.AddressLine2,
                            AddressCity = model.AddressCity,
                            AddressState = model.AddressState,
                            AddressZip = model.AddressZip,
                            Name = model.NameOnCard,
                            Cvc = model.Cvc                            
                        };

                        var cardService = new StripeCardService(_stripe_api_key);
                        var stripeCard = cardService.Create(user.PaymentProcessorID, card);

                        if (!String.IsNullOrEmpty(stripeCard.Id))
                        {
                            stripeCustomer.DefaultSourceId = stripeCard.Id;
                            await customerService.UpdateAsync(stripeCustomer.Id, new StripeCustomerUpdateOptions()
                            {
                                DefaultSource = stripeCard.Id
                            }).ConfigureAwait(false);

                            return await GetCreditCards(user.PaymentProcessorID).ConfigureAwait(false);
                        }
                    }
                    catch (StripeException ex)
                    {
                        if (ex.Message.ToLower().Contains("no such customer"))
                        {
                            user.PaymentProcessorID = await addCustomerWithCard(model, user.EmailAddress)
                                .ConfigureAwait(false);

                            await userSvc.UpdateUser(user).ConfigureAwait(false);
                        }
                        else
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
                else
                {
                    user.PaymentProcessorID = await addCustomerWithCard(model, user.EmailAddress).ConfigureAwait(false);
                    await userSvc.UpdateUser(user).ConfigureAwait(false);
                    return await GetCreditCards(user.PaymentProcessorID).ConfigureAwait(false);
                }
            }

            return null;
        }

        public async Task<bool> RemoveCreditCard(string cardID, int userID)
        {
            try
            {
                var userRepo = new UserRepository();
                var user = await userRepo.GetUserByID(userID).ConfigureAwait(false);

                var cardService = new StripeCardService(_stripe_api_key);
                return (await cardService.DeleteAsync(user.PaymentProcessorID, cardID).ConfigureAwait(false)).Deleted;
            }
            catch (StripeException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public async Task<List<CreditCardModel>> GetCreditCards(string paymentProcessorID)
        {
            try
            {
                if (!String.IsNullOrEmpty(paymentProcessorID))
                {
                    var customerService = new StripeCustomerService(_stripe_api_key);
                    var cardService = new StripeCardService(_stripe_api_key);

                    var customer = customerService.GetAsync(paymentProcessorID);
                    var cards = cardService.ListAsync(paymentProcessorID);

                    await Task.WhenAll(customer, cards).ConfigureAwait(false);

                    if (cards != null)
                    {
                        return cards.Result
                            .Where(x => new DateTime(Convert.ToInt32(x.ExpirationYear), Convert.ToInt32(x.ExpirationMonth), 1) >= DateTime.Now)
                            .Select(x => new CreditCardModel()
                        {
                            Number = x.Last4,
                            ExpirationMonth = x.ExpirationMonth,
                            ExpirationYear = x.ExpirationYear,
                            CardType = x.Brand,
                            NameOnCard = x.Name,
                            ID = x.Id,
                            Default = (customer.Result.DefaultSourceId == x.Id ? true : false)
                        }).OrderBy(x => x.ExpirationYear).ThenBy(x => x.ExpirationMonth).ToList();
                    }
                    else
                    {
                        return new List<CreditCardModel>();
                    }
                }
            }
            catch (StripeException ex)
            {
                //throw new Exception(ex.Message, ex.InnerException);
                return new List<CreditCardModel>();
            }
            catch (Exception ex)
            {
                return new List<CreditCardModel>();
            }

            return new List<CreditCardModel>();
        }

        public async Task UpdateDefaultCard(string paymentProcessorID, string cardID)
        {
            try
            {
                if (!String.IsNullOrEmpty(paymentProcessorID) && !String.IsNullOrEmpty(cardID))
                {
                    var customerService = new StripeCustomerService(_stripe_api_key);
                    var cardService = new StripeCardService(_stripe_api_key);

                    var customer = customerService.GetAsync(paymentProcessorID);
                    var cards = cardService.ListAsync(paymentProcessorID);

                    await Task.WhenAll(customer, cards).ConfigureAwait(false);

                    if (cards.Result.Where(x => x.Id == cardID).Any())
                    {
                        if (cards.Result != null && cards.Result.Count() > 0)
                        {
                            await customerService.UpdateAsync(customer.Result.Id, new StripeCustomerUpdateOptions()
                            {
                                DefaultSource = cardID
                            }).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (StripeException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion

        #region TakePayments
        public async Task<CardPaymentResponse> OneTimePayment(Order order)
        {
            var errorMessage = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(order.BillingInfo.CardID) && order.BillingInfo.CardID != "0")
                {
                    var response = await chargeCustomer(order.OrderTotal, 
                                                  order.BillingInfo.CardID, 
                                                  order.User.PaymentProcessorID, 
                                                  await getDescription().ConfigureAwait(false));

                    return new CardPaymentResponse()
                    {
                        Success = (String.IsNullOrEmpty(response.Id) ? false : true),
                        ConfirmationNumber = response.Id,
                        ErrorMessage = String.Empty
                    };
                }
                else
                {
                    var tokenId = await getTokenId(order.BillingInfo).ConfigureAwait(false);
                    var response = await chargeCustomer(tokenId, order.OrderTotal, await getDescription().ConfigureAwait(false));

                    return new CardPaymentResponse()
                    {
                        Success = (String.IsNullOrEmpty(response.Id) ? false : true),                        
                        ConfirmationNumber = response.Id,
                        ErrorMessage = String.Empty
                    };
                }                
            }
            catch (StripeException e)
            {
                return new CardPaymentResponse()
                {
                    Success = false,
                    ErrorMessage = e.Message,
                    ConfirmationNumber = String.Empty
                };
            }            
        }

        #endregion

        #region Calculate Payments

        public async Task<ShoppingCart> CalculateShoppingCartTotals(ShoppingCart cart, bool applyCredits, List<StoreCredit> coupons, int userID, string shipState = "")
        {
            var creditSvc = new StoreCreditService();
            var taxSvc = new TaxService();
            var taxRate = 0M;
            
            cart.Order.ApplyCredits = applyCredits;

            // ShippingTotal
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ChargeForShipping"]))
            {
                cart.Order.ShippingCharge = await getShippingTotals(cart.Order.OrderLineItems.ToList()).ConfigureAwait(false);
            }

            // Sub-totals
            cart.Order.OrderLineItems.Where(z => z.ProductOptions.Count == 2 && z.ProductOptions.Where(a => a.ParentOptionID != null).Any())
                                     .Select(x => { x.SubTotal = x.ProductOptions.Where(b => b.ParentOptionID != null).Select(c => Convert.ToDecimal(c.Price)).FirstOrDefault() * x.Quantity; return x.SubTotal; });

            cart.Order.OrderLineItems.Where(z => z.ProductOptions.Count == 1)
                                     .Select(x => { x.SubTotal = x.Product.ProductOptions.Select(a => Convert.ToDecimal(a.Price)).FirstOrDefault() * x.Quantity; return x.SubTotal; });

            cart.Order.OrderLineItems.Where(z => z.ProductOptions.Count == 0)
                                     .Select(x => { x.SubTotal = (decimal)x.Product.Price * x.Quantity; return x.SubTotal; });

            cart.Order.OrderSubTotal = cart.Order.OrderLineItems.Sum(x => x.SubTotal);

            // Order Total after updating cart in last routine
            cart.Order.OrderTotal = cart.Order.OrderSubTotal + cart.Order.ShippingCharge;

            if (userID != 0)
            {
                // Credits applied
                cart.Order.StoreCreditsAvailable = await creditSvc.GetTotalAvailableCredits(userID).ConfigureAwait(false);
            }

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ChargeTax"]))
            {
                taxRate = await taxSvc.GetTaxRateByState(shipState).ConfigureAwait(false);
                
                // Sub-tax-totals
                cart.Order.OrderLineItems.Select(x => { x.TaxTotal = (x.Product.Taxable ? ((decimal)x.Product.Price * taxRate) * x.Quantity : 0); return x.TaxTotal; });
                cart.Order.TaxTotal = cart.Order.OrderLineItems.Sum(x => x.TaxTotal);
            }

            // Order Total again
            cart.Order.OrderTotal = cart.Order.OrderSubTotal + cart.Order.ShippingCharge;

            // reset for new calculation
            cart.Order.StoreCreditsAvailable = 0;

            if ((cart.Order.ApplyCredits && userID > 0) || coupons.Count > 0)
            {
                // Check and apply coupons
                foreach (var coupon in coupons)
                {
                    cart.Order.StoreCreditsAvailable += coupon.Amount;

                    if (cart.Order.CouponCodes != null)
                    {
                        if (!cart.Order.CouponCodes.Contains(coupon.CouponCode))
                            cart.Order.CouponCodes.Add(coupon.CouponCode);
                    }
                }

                // Calculate how many credits are applicable to the current cart
                cart.Order.StoreCreditsApplicable = (cart.Order.StoreCreditsAvailable > cart.Order.OrderSubTotal ? cart.Order.OrderSubTotal : cart.Order.StoreCreditsAvailable);
                // Order Total after applying credits
                cart.Order.OrderTotal = (cart.Order.OrderTotal - cart.Order.StoreCreditsApplicable);
            }
            else
            {
                cart.Order.StoreCreditsApplicable = 0;
            }

            // Donation is total of order not including tax
            var charityPercentage = Convert.ToDecimal(ConfigurationManager.AppSettings["CharityPercentage"]);
            cart.Order.TotalToDonate = Math.Round(((charityPercentage / 100m) * (cart.Order.OrderSubTotal - cart.Order.StoreCreditsApplicable)), 2, MidpointRounding.AwayFromZero);

            // Order list
            cart.Order.OrderLineItems = cart.Order.OrderLineItems.OrderBy(x => x.Product.Name).ToList();

            return cart;
        }

        public async Task<StoreCredit> CheckCouponCode(string coupon)
        {
            var creditSvc = new StoreCreditService();
            return await creditSvc.CheckCouponCode(coupon).ConfigureAwait(false);
        }

        #endregion

        #region PaymentMethods

        public async Task<PaymentMethod> GetPaymentMethod(string method)
        {
            var paymentRepo = new PaymentRepository();
            return await paymentRepo.GetPaymentMethod(method).ConfigureAwait(false);
        }

        #endregion

        #region Helpers
        private async Task<string> getTokenId(BillingInfoModel model)
        {
            var options = new StripeTokenCreateOptions()
            {
                Card = new StripeCreditCardOptions()
                {
                    AddressCountry = model.CardAddressCountry,
                    AddressLine1 = model.CardAddressLine1,
                    AddressLine2 = model.CardAddressLine2,
                    AddressCity = model.CardAddressCity,
                    AddressZip = model.CardAddressZip,
                    Cvc = model.CardCvc.ToString(CultureInfo.CurrentCulture),
                    ExpirationMonth = model.CardExpirationMonth,
                    ExpirationYear = model.CardExpirationYear,
                    Name = model.NameOnCard,
                    Number = model.PAN,
                }
            };

            var tokenService = new StripeTokenService(_stripe_api_key);
            var response = await tokenService.CreateAsync(options).ConfigureAwait(false);
            return response.Id;
        }
        private async Task<StripeCharge> chargeCustomer(string tokenId, decimal total, string desc)
        {
            var myCharge = new StripeChargeCreateOptions()
            {
                Amount = Convert.ToInt32(total * 100),
                Currency = "usd",
                Description = desc,
                SourceTokenOrExistingSourceId = tokenId
            };

            var chargeService = new StripeChargeService(_stripe_api_key);
            return await chargeService.CreateAsync(myCharge).ConfigureAwait(false);
        }
        private async Task<StripeCharge> chargeCustomer(decimal total, string cardID, string paymentProcessorID, string desc)
        {
            var charge = new StripeChargeCreateOptions()
            {
                Amount = Convert.ToInt32(total * 100),
                Currency = "USD",
                SourceTokenOrExistingSourceId = cardID,
                CustomerId = paymentProcessorID,
                Capture = true,
                Description = desc
            };

            var chargeService = new StripeChargeService(_stripe_api_key);
            return await chargeService.CreateAsync(charge).ConfigureAwait(false);
        }
        private async Task<string> getDescription()
        {
            var prodDomRepo = new ProductDomainRepository();
            var domain = await prodDomRepo.GetDomainNameAndDescByID(
                Convert.ToInt32(
                    ConfigurationManager.AppSettings["ProductDomainID"])
                    ).ConfigureAwait(false);

            return domain.Name + " - " + domain.Description;
        }
        private async Task<string> addCustomerWithCard(CreditCardModel model, string emailAddress)
        {
            var cust = new StripeCustomerCreateOptions();
            StripeCustomer stripeCustomer = null;
            cust.Email = emailAddress;
            cust.SourceCard = new SourceCard()
            {
                AddressCity = model.AddressCity,
                AddressCountry = model.AddressCountry,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                AddressState = model.AddressState,
                AddressZip = model.AddressZip,
                ExpirationMonth = model.ExpirationMonth,
                ExpirationYear = model.ExpirationYear,
                Name = model.NameOnCard,
                Number = model.Number,
                Cvc = model.Cvc
            };

            try
            {
                var customerService = new StripeCustomerService(_stripe_api_key);
                stripeCustomer = await customerService.CreateAsync(cust).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("card was declined"))
                    throw new Exception("Invalid Credit Card!");
                else
                    throw new Exception("Error - Please contact us at service@petaccent.com.");
            }

            return stripeCustomer.Id;
        }
        private async Task<decimal> getShippingTotals(List<OrderLineItem> items)
        {
            decimal retval = 0m;
            var prodSvc = new ProductService();
            var baseCharge = Convert.ToDecimal(ConfigurationManager.AppSettings["BaseShippingCharge"]);
            var baseCharged = false;

            for (int i = 0; i < items.Count; i++)
            {
                var product = items[i].Product == null ? await prodSvc.GetProductByID(items[i].ProductID).ConfigureAwait(false) : items[i].Product;
                
                for (int j = 0; j < items[i].Quantity; j++)
                {
                    if (product.ShippingCharge > baseCharge)
                    {
                        retval += (j > 0 ? Convert.ToDecimal(product.ShippingCharge * 0.5m) : Convert.ToDecimal(product.ShippingCharge));
                    }
                    else
                    {
                        // don't charge regular shipping for 1st item since there's a base shipping charge greater than 0
                        if ((j != 0 || baseCharged) && items[i].Product.ShippingCharge < baseCharge && baseCharge > 0)
                        {
                            retval += Convert.ToDecimal(product.ShippingCharge);
                        }                            
                        else
                        {
                            if (!baseCharged)
                            {
                                baseCharged = true;
                                retval += baseCharge;
                            }                            
                        }                            
                    }
                }
            }

            return retval;
        }

        #endregion
    }
}
