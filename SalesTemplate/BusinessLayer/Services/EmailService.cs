﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class EmailService
    {
        protected readonly string WelcomeCustomerEmailTemplateName = ConfigurationManager.AppSettings["WelcomeCustomerEmailTemplateName"];

        public async Task SendWelcomeEmail(User user, string welcomeEmailHtml)
        {
            var emailTemplateSvc = new EmailTemplateService();
            var template = await emailTemplateSvc.GetTemplateByName(WelcomeCustomerEmailTemplateName)
                .ConfigureAwait(false);

            template.Html = welcomeEmailHtml;

            template.Html = template.Html.Replace("#####", user.FirstName);
            template.PlainText = template.PlainText.Replace("#####", user.FirstName);

            await EmailServiceProject.OutgoingMail.SendHttpEmail(user, template, false).ConfigureAwait(false);
        }

        public async Task SendOrderConfirmationEmail(User user, string html, bool isGift)
        {
            var emailTemplateSvc = new EmailTemplateService();

            await EmailServiceProject.OutgoingMail.SendHttpEmail(user, html, String.Empty, 
                "orders@petaccent.com", "orders@petaccent.com", "Order Confirmation", isGift, false).ConfigureAwait(false);
        }

        public async Task SendForgotPasswordEmail(User user, string html)
        {
            var emailTemplateSvc = new EmailTemplateService();

            await EmailServiceProject.OutgoingMail.SendHttpEmail(user, html, String.Empty,
                "service@petaccent.com", "service@petaccent.com", "Forgot Password", false, false).ConfigureAwait(false);
        }

        public async Task SendShippingEmail(User user, int id, string trackingNumber = "")
        {
            var html = await GetShippingEmailHtml(id).ConfigureAwait(false);
            var plainText = "Hi " + user.FirstName + "!\n\n Thanks again for placing your order at PetAccent.com! Your order has been shipped! Let us know if you have any questions or concerns. \n\n" +
              "Carrier: USPS\n" +
              "Tracking Number: " + trackingNumber + "\n\n";

            if (!String.IsNullOrEmpty(html))
            { 
                await EmailServiceProject.OutgoingMail.SendHttpEmail(user, html, plainText,
                    "orders@petaccent.com", "orders@petaccent.com", "Order Shipped!", false, false).ConfigureAwait(false);
            }
        }

        public async Task SendAboutPageEmail(string name, string emailAddress, string subject, string message)
        {
            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(emailAddress) && !String.IsNullOrEmpty(subject) && !String.IsNullOrEmpty(message))
                await EmailServiceProject.OutgoingMail.SendHttpEmailToPetAccent(name, emailAddress, subject, message).ConfigureAwait(false);
        }

        public async Task Unsubscribe(string unsubscribe)
        {
            var emailSvc = new EmailService();

            var base64EncodedBytes = System.Convert.FromBase64String(unsubscribe);
            var parts = EncryptionService.Decrypt(System.Text.Encoding.UTF8.GetString(base64EncodedBytes)).Split(new string[] { "####" }, StringSplitOptions.None);

            if (verifyEmailAddressFormat(parts[1]))
            {
                var userSvc = new UserService();
                var user = await userSvc.GetUserByEmailAddress(parts[1]).ConfigureAwait(false);

                if (user != null)
                {
                    user.EmailSubscribed = false;
                    await userSvc.UpdateUser(user).ConfigureAwait(false);
                }

                await emailSvc.DeleteEmailRequestConversion(parts[1]).ConfigureAwait(false);
            }            
        }

        public async Task SendProductToFriend(User user, string html)
        {
            var emailTemplateSvc = new EmailTemplateService();

            await EmailServiceProject.OutgoingMail.SendHttpEmail(user, html, String.Empty,
                user.EmailAddress, user.FirstName + (!String.IsNullOrEmpty(user.LastName) ? " " + user.LastName : ""), 
                "Check this out!", false, false).ConfigureAwait(false);
        }

        public async Task<bool> IsIPInEmailRequestConversion(string ip)
        {
            var eServRepo = new EmailServiceRepository();
            return await eServRepo.IsIPInEmailRequestConversion(ip).ConfigureAwait(false);
        }

        public async Task<Tuple<bool, string>> IsEmailInEmailRequestConversion(string email, string ipAddress = null)
        {
            var eServRepo = new EmailServiceRepository();
            return await eServRepo.IsEmailInEmailRequestConversion(email, ipAddress).ConfigureAwait(false);
        }

        public async Task<bool> RecordEmailAddress(string email, string ip, decimal reward)
        {
            var eServRepo = new EmailServiceRepository();
            var userSvc = new UserService();
            return await eServRepo.RecordEmailAddress(email, ip, reward).ConfigureAwait(false);
        }

        public async Task UpdateEmailRequestConversion(string email, bool accountCreated)
        {
            var eServRepo = new EmailServiceRepository();
            await eServRepo.UpdateEmailRequestConversion(email, accountCreated).ConfigureAwait(false);
        }

        public async Task DeleteEmailRequestConversion(string email)
        {
            var eServRepo = new EmailServiceRepository();
            await eServRepo.DeleteEmailRequestConversion(email).ConfigureAwait(false);
        }

        public async Task<List<string>> GetEmailAddressList()
        {
            var eServRepo = new EmailServiceRepository();
            return await eServRepo.GetEmailAddressList().ConfigureAwait(false);
        }

        public async Task<bool> VerifyEmailAddress(string email)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["TestEnv"]))
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync("https://bpi.briteverify.com/emails.json?address=" + email + "&apikey=" +
                        ConfigurationManager.AppSettings["BriteVerify:Key"]).ConfigureAwait(false);

                    var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    var objResult = JObject.Parse(result);
                    var status = ((string)objResult["status"] == "valid" ? true : false);
                    var disposable = (bool)objResult["disposable"];
                    var role_address = (bool)objResult["role_address"];

                    if (status && !disposable && !role_address)
                        return true;
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        private bool verifyEmailAddressFormat(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return (addr.Address == email);
            }
            catch
            {
                return false;
            }
        }

        private async Task<string> GetShippingEmailHtml(int id)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authentication", "JoelD311:73jkSfjDs8yrHweEo65iSo498fj#w29dfj");
                var response = await client.GetAsync(ConfigurationManager.AppSettings["ShippingConfirmationEmailURL"] + id).ConfigureAwait(false);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }

            return null;
        }
    }
}
