﻿using System.Collections.Generic;

namespace DataLayer.Data.Entities
{
    public partial class Charity
    {
        public Charity()
        {
            this.Orders = new HashSet<Order>();
            this.Users = new HashSet<User>();
        }
        public int ID { get; set; }
        public string CharityName { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
