﻿using BusinessLayer.Services;
using DataLayer.Data.Entities;
using SalesTemplate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class CartController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]        
        public async Task<ActionResult> Index()
        {
            var sessionSvc = new SessionService();
            var paymentSvc = new PaymentService();
            var userSvc = new UserService();
            var model = new CartModel(true);

            if (Request.Cookies[RememberUsernameCookieName] != null)
                ViewBag.EmailAddress = Request.Cookies[RememberUsernameCookieName].Value;

            var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);

            model.FirstName = User.FirstName;
            model.CartTotal = cart.Order.OrderLineItems.Sum(x => x.Quantity);
            model.Charity = (cart.Order.CharityID == 0 ? 1 : cart.Order.CharityID);

            var user = User;
            var calcTask = paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(),
                (user.Identity.IsAuthenticated ? user.UserID : 0));
            var userTask = userSvc.GetUserByID(user.UserID);            

            await Task.WhenAll(calcTask, userTask).ConfigureAwait(false);

            sessionSvc.UpdateShoppingCart(calcTask.Result);
            model.ShoppingCart = calcTask.Result;
            model.UsedCoupons = sessionSvc.CouponCodes();

            if (User.Identity.IsAuthenticated)
                model.Charity = Convert.ToInt32(userTask.Result.PreferredCharityID) == 0 ? 
                    1 : 
                    Convert.ToInt32(userTask.Result.PreferredCharityID);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> ApplyCredits(bool applyCredits)
        {
            var sessionSvc = new SessionService();
            var paymentSvc = new PaymentService();
            var model = new CartModel(false);

            try
            {
                var user = User;
                var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
                cart.Order.ApplyCredits = applyCredits;

                cart = await paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(),
                    (user.Identity.IsAuthenticated ? user.UserID : 0)).ConfigureAwait(false);

                model.ShoppingCart = cart;
                sessionSvc.UpdateShoppingCart(cart);

                return Json(new
                {
                    TotalsHtml = this.RenderRazorViewToString(this, "_Totals", model)
                });
            }
            catch { }

            return Json("");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> ApplyCouponCode(string coupon)
        {
            var sessionSvc = new SessionService();
            var paymentSvc = new PaymentService();
            var creditSvc = new StoreCreditService();
            var model = new CartModel(false);

            try
            {
                var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);

                if (cart.Order.OrderLineItems.Count == 0)
                {
                    return Json(new
                    {
                        ErrorMsg = "Please add items to your cart and then apply coupons.",
                        Totals = ""
                    });
                }

                if (sessionSvc.CouponCodes().Where(x => x.CouponCode == coupon).Any())
                {
                    return Json(new
                    {
                        ErrorMsg = "This coupon code has already been applied.",
                        Totals = ""
                    });
                }

                var couponobj = await creditSvc.CheckCouponCode(coupon).ConfigureAwait(false);

                if (couponobj != null)
                {
                    var user = User;                    
                    sessionSvc.CouponCodes(new List<StoreCredit>() { couponobj });

                    cart = await paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(),
                        (user.Identity.IsAuthenticated ? user.UserID : 0)).ConfigureAwait(false);

                    model.ShoppingCart = cart;
                    model.UsedCoupons = sessionSvc.CouponCodes();
                    sessionSvc.UpdateShoppingCart(cart);                    

                    return Json(new
                    {
                        ErrorMsg = "",
                        Totals = this.RenderRazorViewToString(this, "~/Views/Shared/_CartTotals.cshtml", model),
                    });
                }

                return Json(new
                {
                    ErrorMsg = "Sorry! This coupon code is invalid.",
                    Totals = ""
                });
            }
            catch (Exception ex) { }

            return Json("");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> RemoveCouponCode(string coupon)
        {
            var sessionSvc = new SessionService();
            var paymentSvc = new PaymentService();
            var creditSvc = new StoreCreditService();
            var model = new CartModel(false);

            try
            {
                if (!String.IsNullOrEmpty(coupon))
                {
                    var user = User;
                    var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);                    
                    cart.Order.CouponCodes.Remove(coupon);
                    cart = await paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.RemoveCouponCode(coupon),
                        (user.Identity.IsAuthenticated ? user.UserID : 0)).ConfigureAwait(false);

                    model.ShoppingCart = cart;
                    sessionSvc.UpdateShoppingCart(cart);
                    model.UsedCoupons = sessionSvc.CouponCodes();

                    return Json(new
                    {
                        Message = "Coupon code has been removed",
                        Totals = this.RenderRazorViewToString(this, "~/Views/Shared/_CartTotals.cshtml", (BaseModel)model),
                    });
                }
            }
            catch (Exception ex) { }

            return Json("");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<JsonResult> UpdateAndCalculateShoppingCart(int productID, int optionID, int quantity, bool quantify, bool applyCredits)
        {
            var sessionSvc = new SessionService();
            var paymentSvc = new PaymentService();
            var model = new CartModel(false);

            try
            {
                var user = User;
                var result = await sessionSvc.UpdateShoppingCart(productID, optionID, quantity, quantify).ConfigureAwait(false);                

                if (result.Success)
                {
                    var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
                    cart.Order.ApplyCredits = applyCredits;

                    cart = await paymentSvc.CalculateShoppingCartTotals(cart, cart.Order.ApplyCredits, sessionSvc.CouponCodes(),
                        (user.Identity.IsAuthenticated ? user.UserID : 0)).ConfigureAwait(false);

                    model.ShoppingCart = cart;

                    if (model.ShoppingCart.Order.OrderLineItems.Count == 0)
                    {
                        model.UsedCoupons = new List<StoreCredit>();
                        sessionSvc.ClearCouponCodes();
                    }
                    else
                        model.UsedCoupons = sessionSvc.CouponCodes();

                    sessionSvc.UpdateShoppingCart(cart);
                }
                else
                {
                    var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
                    model.ShoppingCart = cart;
                }

                return Json(new
                {
                    CartCount = model.ShoppingCart.Order.OrderLineItems.Sum(x => x.Quantity),
                    Snapshot = this.RenderRazorViewToString(this, "~/Views/Shared/_TotalsSnapshot.cshtml", model),
                    CartGrid = (quantity > 0 ? this.RenderRazorViewToString(this, "_CartGridContainer", model) : String.Empty), // using javascript to remove deleted product in cart
                    Totals = this.RenderRazorViewToString(this, "~/Views/Shared/_CartTotals.cshtml", model),
                    ErrorMsg = result.ErrorMsg
                });
            }
            catch (Exception ex) { string test = ""; }

            return Json("");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task SetSelectedCharity(int charityID)
        {
            try
            {
                var sessionSvc = new SessionService();
                var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
                cart.Order.CharityID = charityID;
                sessionSvc.UpdateShoppingCart(cart);

            }
            catch (Exception ex) { }
        }
    }
}