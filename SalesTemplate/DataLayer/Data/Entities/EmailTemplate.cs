﻿namespace DataLayer.Data.Entities
{
    public partial class EmailTemplate
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }
        public string PlainText { get; set; }
        public string FromAddress { get; set; }
        public string FromDisplayName { get; set; }
        public System.DateTime DateCreated { get; set; }
    }
}
