﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;

namespace DataLayer.Repositories
{
    public class SystemRepository : BaseRepository
    {
        public async Task<AddressType> GetAddressTypeByName(string name)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.AddressTypes
                                .Where(x => x.Type == name)
                                .FirstOrDefaultAsync()
                                .ConfigureAwait(false);
            }
        }
        public async Task<List<sys_State>> GetStatesByCountry(int country)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.States
                                .Where(x => x.Country == country)
                                .ToListAsync()
                                .ConfigureAwait(false);
            }            
        }

        public async Task<List<sys_Country>> GetCountries()
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Countries
                                .ToListAsync()
                                .ConfigureAwait(false);
            }
        }

        public async Task<sys_Country> GetCountryByID(int id)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.Countries
                                .Where(x => x.ID == id)
                                .FirstOrDefaultAsync()
                                .ConfigureAwait(false);
            }
        }
    }
}
