﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using DataLayer.Models;
using DataLayer.Repositories;
using SalesTemplate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class SessionService
    {
        private SessionRepository _repo { get; set; }

        public SessionService()
        {
            if (_repo == null)
                _repo = new SessionRepository();
        }
        public async Task<ShoppingCart> GetShoppingCart()
        {
            var productSvc = new ProductService();

            if (_repo.ShoppingCart != null)
            {
                if (_repo.ShoppingCart.Order != null)
                {
                    foreach (var item in _repo.ShoppingCart.Order.OrderLineItems)
                    {
                        item.Product = await productSvc.GetProductByID(item.ProductID).ConfigureAwait(false);

                        // move selected option price and image to Product (in shopping cart only) in order to properly
                        // display items in cart snapshot
                        if (item.ProductOptions != null && item.ProductOptions.Count > 0)
                        {
                            var option = item.ProductOptions.Where(x => x.ParentOptionID != null).FirstOrDefault();

                            if (option != null)
                            {
                                item.Product.Price = option.Price;
                                item.Product.ImagePath = option.ImagePath;
                            }
                            else
                            {
                                option = item.ProductOptions.Where(x => x.ParentOptionID == null).FirstOrDefault();

                                if (option != null)
                                {
                                    item.Product.Price = (option.Price == null ? item.Product.Price : option.Price);
                                    item.Product.ImagePath = (option.ImagePath == null ? item.Product.ImagePath : option.ImagePath);
                                }
                            }
                        }
                    }
                }                

                var cart = _repo.ShoppingCart;
                cart.Order.OrderLineItems = cart.Order.OrderLineItems.OrderBy(x => x.Product.Name).ToList();

                return cart;
            }

            return new ShoppingCart();
        }
        public async Task<UpdateAction> UpdateShoppingCart(int productID, int? optionID, int quantity, bool quantify)
        {
            optionID = optionID == 0 ? null : optionID;
            var productRepo = new ProductRepository();
            var product = await productRepo.GetProductByID(productID).ConfigureAwait(false);

            // if quantify == true add quantity to existing product/option cart quantity
            if (quantify)
                quantity = await _quantifyQuantity(product, optionID, quantity).ConfigureAwait(false);

            // only allow customer to add the total number (or less) of available quantity to their cart
            int quantAvail = 0;
            if (optionID.HasValue)
                quantAvail = Convert.ToInt32(product.ProductOptions.Where(x => x.ID == optionID).Select(x => x.QuantityAvailable).FirstOrDefault());
            else
                quantAvail = Convert.ToInt32(product.QuantityAvailable);
            
            if (quantity > quantAvail)
            {
                string msg = String.Empty;
                if (quantAvail == 0)
                    msg = "Sorry! This item is no longer available.";
                else
                    msg = "Sorry! We only have " + quantAvail + " of these left in stock.";

                return new UpdateAction()
                {
                    Success = false,
                    TotalItemsInCart = _repo.UpdateCart(product, quantAvail, optionID),
                    ErrorMsg = msg,
                    QuantityAvailable = quantAvail
                };
            }
            else
            {
                return new UpdateAction()
                {
                    Success = true,
                    TotalItemsInCart = _repo.UpdateCart(product, quantity, optionID),
                    ErrorMsg = string.Empty
                };
            }            
        }
        public void UpdateShoppingCartTotalItems(int total)
        {
            _repo.ShoppingCartTotalItems = total > 0 ? total.ToString() : "";
        }

        public void UpdateShoppingCart(ShoppingCart cart)
        {
            _repo.ShoppingCart = cart;
        }

        public int UserID(int? userID = null)
        {
            if (userID != null)
            {
                _repo.UserID = Convert.ToInt32(userID);
            }
            else
            {
                return _repo.UserID;
            }

            return 0;
        }

        public List<StoreCredit> CouponCodes(List<StoreCredit> coupons = null)
        {
            try
            {
                if (coupons != null)
                {
                    var cc = _repo.CouponCodes;

                    foreach (var item in coupons)
                    {
                        cc.Add(item);
                    }

                    _repo.CouponCodes = cc;
                }
                else
                {
                    return _repo.CouponCodes;
                }
            }
            catch (Exception ex)
            {
                string test = "";
            }

            return null;
        }
        public List<StoreCredit> RemoveCouponCode(string coupon)
        {
            if (!String.IsNullOrEmpty(coupon))
            {
                var cc = _repo.CouponCodes;
                cc.RemoveAll(x => x.CouponCode == coupon);
                _repo.CouponCodes = cc;
                return cc;
            }

            return null;
        }
        public void ClearCouponCodes()
        {
            var cc = _repo.CouponCodes;
            cc = new List<StoreCredit>();
            _repo.CouponCodes = cc;
        }
        public RegistrationViewModel RegistrationData(RegistrationViewModel model = null)
        {
            if (model != null)
                _repo.RegistrationData = model;
            else
                return _repo.RegistrationData;

            return null;
        }
        public class UpdateAction
        {
            public bool Success { get; set; }
            public int TotalItemsInCart { get; set; }
            public string ErrorMsg { get; set; }
            public int QuantityAvailable { get; set; }
        }
        private async Task<int> _quantifyQuantity(Product product, int? optionID, int quantity)
        {
            var cart = await this.GetShoppingCart().ConfigureAwait(false);
            var preSelect = cart.Order.OrderLineItems.Where(x => x.Product.ID == product.ID).ToList();

            if (preSelect.Count > 0)
            {
                if (optionID != null)
                {
                    var prod = preSelect.Where(x => x.ProductOptions.Any(y => y.ID == optionID)).FirstOrDefault();

                    if (prod != null)
                    {
                        return quantity += prod.Quantity;
                    }
                    else if (product.ProductOptions.Count > 0)
                    {
                        return quantity;
                    }                    
                }
                else
                {
                    return quantity += preSelect.Select(x => x.Quantity).FirstOrDefault();
                }
            }
            else
                return quantity;

            return quantity;
        }
    }
}
