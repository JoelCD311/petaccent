﻿$(window).on('click', function (e) {
    var target = $(e.target);
    if (target.is("a") || target.is("i")) {
        if ($('[name="hdnIsAuthenticated"]').val() == 'False') {
            if (!sessionStorage.totclka) {
                sessionStorage.totclka = 0;
            }
            if (Number(sessionStorage.totclka) == 0) {
                if ($('[name="hdnCurrentPromoAmount"]').val() > 0) {
                    requestCustomerEmail();
                    sessionStorage.totclka = 1;
                    e.preventDefault();
                    return false;
                }
            }
        }
    }
});
$('#aboutMessage').on('keyup', function () {
    var length = $(this).val().length;
    if (length > 500) {
        $('#amCount').css('color', 'red');
        $('#amCount').text('-' + length - 500);
    }
    else {
        $('#amCount').css('color', 'black');
        $('#amCount').text(500 - length);
    }
});
$('.about-input').on('focus', function () {
    if (!$('.grecaptcha-badge').is(':visible')) {
        $('.grecaptcha-badge').show();
    }
    if ($(this).attr('id') == 'aboutMessage') {
        removeNonTextError($(this));
    }
    else {
        removeBasicInputError($(this));
    }
});
$('.about-input').on('keydown', function () {
    if ($(this).attr('id') == 'aboutMessage') {
        removeNonTextError($(this));
    }
    else {
        removeBasicInputError($(this));
    }
});
$('.about-input').on('blur', function () {
    if ($('.grecaptcha-badge').is(':visible')) {
        $('.grecaptcha-badge').hide();
    }
});
$('.g-recaptcha').on('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    if (validate()) {
        $('.g-recaptcha').hide();
        $('.wait-spinner-submit').show();
        grecaptcha.execute();
    }
    else {
        resetCaptcha();
    }
});
function submitValidated() {
    var capresp = grecaptcha.getResponse();
    if (capresp != '' && capresp != undefined && capresp != null) {

        $.post('/About/SendAboutMessage', getSendAboutMsgData(), sendResult)
    }
    else {
        resetCaptcha();
    }
}
function validate() {
    var valid = true;
    var name = $('#aboutName');
    var email = $('#aboutEmail');
    var subject = $('#aboutSubject');
    var message = $('#aboutMessage');

    if (name.val() == '') {
        valid = false;
        addBasicInputError(name);
    }

    if (email.val() == '') {
        valid = false;
        addBasicInputError(email);
    }
    else {
        if (!isValidEmailAddress(email.val())) {
            valid = false;
            addBasicInputError(email);
        }
    }

    if (subject.val() == '') {
        valid = false;
        addBasicInputError(subject);
    }

    if (message.val() == '') {
        valid = false;
        addNonTextError(message);
    }

    return valid;
}
function getSendAboutMsgData() {
    return {
        __RequestVerificationToken: GLB_TOKEN,
        name: $('#aboutName').val(),
        email: $('#aboutEmail').val(),
        subject: $('#aboutSubject').val(),
        message: $('#aboutMessage').val(),
        grr: $('.g-recaptcha-response').val()
    }
}
function sendResult(result) {
    if (result.Success) {
        $('.grecaptcha-badge').hide();
        alertify.success('Your message has been sent!');
        $('.about-input').val('');
    }
    else {
        $('.grecaptcha-badge').hide();
        alertify.error(result.ErrorMsg);
    }
    $('.wait-spinner-submit').hide();
    $('.g-recaptcha').show();
}
function resetCaptcha() {
    grecaptcha.reset();
    if ($('.g-recaptcha').html().indexOf("Submit") == -1) {
        $('.g-recaptcha').html('Submit');
    }
}