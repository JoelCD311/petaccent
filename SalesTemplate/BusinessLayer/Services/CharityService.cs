﻿using DataLayer.Data.Entities;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLayer.Services
{
    public class CharityService
    {
        private CharityRepository _repo { get; set; }

        public CharityService()
        {
            _repo = new CharityRepository();
        }

        public async Task<List<Charity>> GetCharities()
        {
            return await _repo.GetAllCharities().ConfigureAwait(false);
        }

        public async Task<Charity> GetCharityByID(int id)
        {
            return await _repo.GetCharityByID(id).ConfigureAwait(false);
        }
    }
}
