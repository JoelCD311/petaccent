﻿namespace DataLayer.Data.Entities
{
    public partial class TaxRate
    {
        public int ID { get; set; }
        public string State { get; set; }
        public decimal Rate { get; set; }
        public string Type { get; set; }
    }
}
