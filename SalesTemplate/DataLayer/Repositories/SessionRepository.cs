﻿using DataLayer.Data.Entities;
using DataLayer.Data.Models;
using DataLayer.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace SalesTemplate.Repositories
{
    public class SessionRepository
    {
        private HttpSessionState _session;
        public SessionRepository()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Session != null)
                    _session = HttpContext.Current.Session;
            }
        }
        public ShoppingCart ShoppingCart
        {
            get { return (_session["ShoppingCart"] != null ? _session["ShoppingCart"] as ShoppingCart : new ShoppingCart(new Order())); }
            set { _session["ShoppingCart"] = value; }
        }

        public string ShoppingCartTotalItems
        {
            get { return (_session["ShoppingCartTotalItems"] != null ? _session["ShoppingCartTotalItems"] as string : "0"); }
            set { _session["ShoppingCartTotalItems"] = value; }
        }

        public int UserID
        {
            get { return (_session["UserID"] != null ? (int)_session["UserID"] : 0); }
            set { _session["UserID"] = value; }
        }

        public List<StoreCredit> CouponCodes
        {
            get { return (_session["CouponCodes"] != null ? _session["CouponCodes"] as List<StoreCredit> : new List<StoreCredit>()); }
            set { _session["CouponCodes"] = value; }
        }

        public RegistrationViewModel RegistrationData
        {
            get { return (_session["RegistrationData"] != null ? _session["RegistrationData"] as RegistrationViewModel : new RegistrationViewModel()); }
            set { _session["RegistrationData"] = value; }
        }

        public int UpdateCart(Product product, int quantity, int? optionID)
        {            
            var cart = ShoppingCart;

            if (cart == null)
            {
                var order = new Order();
                var lineItems = new List<OrderLineItem>();
                cart = new ShoppingCart();

                order.OrderLineItems = lineItems;
                cart.Order = order;
            }

            // remove from list items in question to be re-added (or not if quantity == 0) below
            cart.Order.OrderLineItems = _returnListMinusSubject(cart.Order.OrderLineItems, product, optionID); 
            
            if (quantity > 0)
            {
                // if product has options
                if (product.Active && product.ProductOptions.Count > 0)
                {
                    var quant = (int)(quantity <= product.ProductOptions.Where(x => x.ID == optionID).Sum(x => x.QuantityAvailable) ?
                    quantity :
                    product.ProductOptions.Where(x => x.ID == optionID).Sum(x => x.QuantityAvailable));

                    if (quant > 0 && optionID.HasValue)
                    {
                        var opts = new List<ProductOption>();
                        opts.Add(product.ProductOptions.Where(x => x.ID == optionID).First());

                        if (opts.ElementAt(0).ParentOptionID != null)
                            opts.Add(product.ProductOptions.Where(x => x.ID == opts.ElementAt(0).ParentOptionID).FirstOrDefault());

                        cart.Order.OrderLineItems.Add(new OrderLineItem()
                        {
                            ProductID = product.ID,
                            Quantity = quant,
                            ProductOptions = opts,
                            SubTotal = (decimal)(product.ProductOptions.Where(x => x.ID == optionID).Select(x => x.Price).First() * quant)
                        });
                    }
                } 
                else
                {
                    var quant = (int)(quantity <= product.QuantityAvailable ? quantity : product.QuantityAvailable);

                    if (quant > 0)
                    {
                        cart.Order.OrderLineItems.Add(new OrderLineItem()
                        {
                            ProductID = product.ID,
                            Quantity = quant,
                            SubTotal = (decimal)(product.Price * quant)
                        });
                    }
                }
            }            

            //cart.Order.OrderLineItems = cart.Order.OrderLineItems.OrderByDescending(x => x.Quantity).ToList();
            ShoppingCartTotalItems = cart.Order.OrderLineItems.Sum(x => x.Quantity).ToString();
            ShoppingCart = cart;
            return cart.Order.OrderLineItems.Sum(x => x.Quantity);
        }

        private ICollection<OrderLineItem> _returnListMinusSubject(ICollection<OrderLineItem> list, Product product, int? optionID)
        {
            try
            {
                if (optionID != null && optionID != 0)
                {
                    foreach (var item in list)
                    {
                        if (item.ProductOptions != null)
                        {
                            if (item.ProductOptions.Where(x => x.ID == optionID).Any())
                            {
                                list.Remove(item);
                                break;
                            }
                        }
                    }
                }
                else if (list.Where(x => x.ProductID == product.ID).Any())
                {
                    list = list.Where(x => x.ProductID != product.ID).ToList();
                }
            }
            catch
            {
                string test = "";
            }

            return list;
        }
    }
}