﻿using System.Security.Principal;

namespace SalesTemplate.Infrastructure
{
    interface ISalesPrincipal : IPrincipal
    {
        int UserID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string EmailAddress { get; set; }
    }
}
