﻿using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace DataLayer.Helpers.Models
{
    public class OptionSelection
    {
        public string ImagePath { get; set; }
        public decimal Price { get; set; }
        public decimal UndiscountedPrice { get; set; }
        public List<ProductOption> Options { get; set; }
    }
}
