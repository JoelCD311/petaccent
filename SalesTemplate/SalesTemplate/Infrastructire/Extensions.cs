﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesTemplate.Infrastructure
{
    public static class Extensions
    {
        private static Random rng = new Random();

        public static List<T> Shuffle<T>(this List<T> list)
        {
            int i = list.Count;
            while (i > 1)
            {
                i--;
                int k = rng.Next(i + 1);
                T value = list[k];
                list[k] = list[i];
                list[i] = value;
            }

            return list;
        }
    }
}