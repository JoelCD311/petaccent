--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

USE [SalesTemplate]
GO

----------------------------
-- Product Domains
---------------------------- 

insert into dbo.ProductDomain
values ('Pet Accents', 'Accessories for your best friends')

----------------------------
-- Payment Method
---------------------------- 

insert into dbo.PaymentMethod
values ('CreditCard')

insert into dbo.PaymentMethod
values ('ACH')

----------------------------
-- Order Status
---------------------------- 

insert into dbo.OrderStatus
values ('Created')

insert into dbo.OrderStatus
values ('Processing')

insert into dbo.OrderStatus
values ('Processed')

insert into dbo.OrderStatus
values ('Shipped')

insert into dbo.OrderStatus
values ('Cancelled')

insert into dbo.OrderStatus
values ('Returned')

insert into dbo.OrderStatus
values ('Chargeback')

-----------------------------------------------
-- Store Credit Type
-----------------------------------------------

insert into dbo.[StoreCreditType]
values ('Credit')

insert into dbo.[StoreCreditType]
values ('Reward')

-----------------------------------------------
-- Address Type
-----------------------------------------------

insert into dbo.[AddressType]
values ('Shipping')

insert into dbo.[AddressType]
values ('Billing')

----------------------------
-- Users
----------------------------

insert into dbo.[User]
values ('Joel', 'Dunn', 'joelcdunn@gmail.com', 'B3xYnWspadY/vb6hl9uH9Q==', CAST('8BA83368-571C-4570-9298-9B3FC729D54E' AS UNIQUEIDENTIFIER), NULL, 1, 1, NULL, NULL, getdate())

----------------------------
-- Address
----------------------------

insert into dbo.[Address]
values (1, 1, '5916 Fantail Dr.', NULL, NULL, 'Fort Worth', 'TX', '76179', 'US', getdate())

----------------------------
-- Product Domains
----------------------------

insert into dbo.[Product]
values ('PROD-1ASD', 'Product 1', 'This is a wonderful product that makes your life better!', 19.99, 5, 1, null, null, 5, 1, 1, '/Content/Images/Products/prod1.jpg', 1, 'Dogs', null, getdate())

insert into dbo.[Product]
values ('PROD-2ASD', 'Product 2', 'This is a wonderful product that makes your life better!', 24.99, 6, 1, null, null, 6, 1, 1, '/Content/Images/Products/prod2.jpg', 1, 'Dogs', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 3', 'This is a wonderful product that makes your life better!', 34.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod3.jpg', 1, 'Dogs', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 4', 'This is a wonderful product that makes your life better!', 54.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod4.jpg', 1, 'Dogs', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 5', 'This is a wonderful product that makes your life better!', 74.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod5.jpg', 1, 'Dogs', null, getdate())

insert into dbo.[Product]
values ('PROD-1ASD', 'Product 1', 'This is a wonderful product that makes your life better!', 19.99, 5, 1, null, null, 5, 1, 1, '/Content/Images/Products/prod1.jpg', 1, 'Cats', null, getdate())

insert into dbo.[Product]
values ('PROD-2ASD', 'Product 2', 'This is a wonderful product that makes your life better!', 24.99, 6, 1, null, null, 6, 1, 1, '/Content/Images/Products/prod2.jpg', 1, 'Cats', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 3', 'This is a wonderful product that makes your life better!', 34.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod3.jpg', 1, 'Cats', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 4', 'This is a wonderful product that makes your life better!', 54.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod4.jpg', 1, 'Cats', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 5', 'This is a wonderful product that makes your life better!', 74.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod5.jpg', 1, 'Cats', null, getdate())

insert into dbo.[Product]
values ('PROD-1ASD', 'Product 1', 'This is a wonderful product that makes your life better!', 19.99, 5, 1, null, null, 5, 1, 1, '/Content/Images/Products/prod1.jpg', 1, 'Bunnies', null, getdate())

insert into dbo.[Product]
values ('PROD-2ASD', 'Product 2', 'This is a wonderful product that makes your life better!', 24.99, 6, 1, null, null, 6, 1, 0, '/Content/Images/Products/prod2.jpg', 1, 'Bunnies', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 3', 'This is a wonderful product that makes your life better!', 34.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod3.jpg', 1, 'Bunnies', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 4', 'This is a wonderful product that makes your life better!', 54.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod4.jpg', 1, 'Bunnies', null, getdate())

insert into dbo.[Product]
values ('PROD-3ASD', 'Product 5', 'This is a wonderful product that makes your life better!', 74.99, 7, 0, null, null, 3, 1, 0, '/Content/Images/Products/prod5.jpg', 1, 'Cats', null, getdate())

----------------------------
-- Product Options
---------------------------- 

insert into dbo.[ProductOption]
values (null, 1, 'Color', 'Blue', 3, 19.99, null, null)

insert into dbo.[ProductOption]
values (null, 1, 'Color', 'Red', 3, 19.99, null, null)

insert into dbo.[ProductOption]
values (null, 1, 'Color', 'Green', 3, 19.99, null, null)

insert into dbo.[ProductOption]
values (null, 1, 'Color', 'Yellow', 3, 19.99, null, null)

insert into dbo.[ProductOption]
values (1, 1, 'Size', 'Small', 3, 19.99, 'S', null)

insert into dbo.[ProductOption]
values (1, 1, 'Size', 'Medium', 3, 19.99, 'M', null)

insert into dbo.[ProductOption]
values (1, 1, 'Size', 'Large', 3, 19.99, 'L', null)

insert into dbo.[ProductOption]
values (1, 1, 'Size', 'XL', 3, 19.99, null, null)

insert into dbo.[ProductOption]
values (null, 2, 'Color', 'Blue', 4, 29.99, null, null)

insert into dbo.[ProductOption]
values (null, 2, 'Color', 'Red', 4, 29.99, null, null)

insert into dbo.[ProductOption]
values (null, 2, 'Color', 'Green', 4, 29.99, null, null)

insert into dbo.[ProductOption]
values (null, 2, 'Color', 'Yellow', 4, 29.99, null, null)

insert into dbo.[ProductOption]
values (null, 3, 'Size', 'Small', 4, 29.99, 'S', null)

insert into dbo.[ProductOption]
values (null, 3, 'Size', 'Medium', 4, 29.99, 'M', null)

insert into dbo.[ProductOption]
values (null, 3, 'Size', 'L', 4, 29.99, 'L', null)

insert into dbo.[ProductOption]
values (null, 3, 'Size', 'XL', 4, 29.99, null, null)

-------------------------
-- Charities
--------------------------

insert into dbo.[Charity]
values ('ASPCA: American Society for the Prevention of Cruelty to Animals', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Humane Society of The United States', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Paws for Purple Hearts', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Paws4Vets', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('PETA: People for the Ethical Treatment of Animals', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('San Diego Zoo Global', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('SNAP: Spay-Neuter Assistance Program', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('House Rabbit Society', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Horse Charities of America', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('NEADS: Dogs for Deaf and Disabled Americans', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Africa Network for Animal Welfare - USA', '', '', '', '', '', '', '', '', '', getdate())

insert into dbo.[Charity]
values ('Alley Cat Rescue', '', '', '', '', '', '', '', '', '', getdate())

-----------------------------------
-- Country -----
-----------------------------------

insert into dbo.[sys_Country]
values ('US', 'United States of America')

-----------------------------------
-- StoreCredit -----
-----------------------------------

insert into dbo.[StoreCredit]
values (2, 1, 10, 'Signup', null, '2018-01-01', getdate())

-----------------------------------
-- State -----
-----------------------------------

insert into dbo.[sys_State]
values('AL','Alabama', 1)

insert into dbo.[sys_State]
values('AK','Alaska', 1)

insert into dbo.[sys_State]
values('AZ','Arizona', 1)

insert into dbo.[sys_State]
values('AR','Arkansas', 1)

insert into dbo.[sys_State]
values('CA','California', 1)

insert into dbo.[sys_State]
values('CO','Colorado', 1)

insert into dbo.[sys_State]
values('CT','Connecticut', 1)

insert into dbo.[sys_State]
values('DE','Delaware', 1)

insert into dbo.[sys_State]
values('FL','Florida', 1)

insert into dbo.[sys_State]
values('GA','Georgia', 1)

insert into dbo.[sys_State]
values('HI','Hawaii', 1)

insert into dbo.[sys_State]
values('ID','Idaho', 1)

insert into dbo.[sys_State]
values('IL','Illinois', 1)

insert into dbo.[sys_State]
values('IN','Indiana', 1)

insert into dbo.[sys_State]
values('IA','Iowa', 1)

insert into dbo.[sys_State]
values('KS','Kansas', 1)

insert into dbo.[sys_State]
values('KY','Kentucky', 1)

insert into dbo.[sys_State]
values('LA','Louisiana', 1)

insert into dbo.[sys_State]
values('ME','Maine', 1)

insert into dbo.[sys_State]
values('MD','Maryland', 1)

insert into dbo.[sys_State]
values('MA','Massachusetts', 1)

insert into dbo.[sys_State]
values('MI','Michigan', 1)

insert into dbo.[sys_State]
values('MN','Minnesota', 1)

insert into dbo.[sys_State]
values('MS','Mississippi', 1)

insert into dbo.[sys_State]
values('MO','Missouri', 1)

insert into dbo.[sys_State]
values('MT','Montana', 1)

insert into dbo.[sys_State]
values('NE','Nebraska', 1)

insert into dbo.[sys_State]
values('NV','Nevada', 1)

insert into dbo.[sys_State]
values('NH','New Hampshire', 1)

insert into dbo.[sys_State]
values('NJ','New Jersey', 1)

insert into dbo.[sys_State]
values('NM','New Mexico', 1)

insert into dbo.[sys_State]
values('NY','New York', 1)

insert into dbo.[sys_State]
values('NC','North Carolina', 1)

insert into dbo.[sys_State]
values('ND','North Dakota', 1)

insert into dbo.[sys_State]
values('OH','Ohio', 1)

insert into dbo.[sys_State]
values('OK','Oklahoma', 1)

insert into dbo.[sys_State]
values('OR','Oregon', 1)

insert into dbo.[sys_State]
values('PA','Pennsylvania', 1)

insert into dbo.[sys_State]
values('RI','Rhode Island', 1)

insert into dbo.[sys_State]
values('SC','South Carolina', 1)

insert into dbo.[sys_State]
values('SD','South Dakota', 1)

insert into dbo.[sys_State]
values('TN','Tennessee', 1)

insert into dbo.[sys_State]
values('TX','Texas', 1)

insert into dbo.[sys_State]
values('UT','Utah', 1)

insert into dbo.[sys_State]
values('VT','Vermont', 1)

insert into dbo.[sys_State]
values('VA','Virginia', 1)

insert into dbo.[sys_State]
values('WA','Washington', 1)

insert into dbo.[sys_State]
values('WV','West Virginia', 1)

insert into dbo.[sys_State]
values('WI','Wisconsin', 1)

insert into dbo.[sys_State]
values('WY','Wyoming', 1)

------------------------------------------------
-- Email Templates
------------------------------------------------
  insert into emailtemplate
  values ('WelcomeCustomer', 'Welcome to PetAccent.com!', '<html style="height: 100%;">
<body style="font-family: Arial, Helvetica, sans-serif;">
    <div style="min-height: 100%;
            width: 70%;
            min-width: 380px;
            min-height: 400px;
            border: 2px solid;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
            position: relative;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -webkit-border-bottom-left-radius: 7px;
            -webkit-border-bottom-right-radius: 7px;
            -moz-border-radius-topleft: 7px;
            -moz-border-radius-topright: 7px;
            -moz-border-radius-bottomleft: 7px;
            -moz-border-radius-bottomright: 7px;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            background-image: url(''http://www.petaccent.com/Content/Images/welcome-image-trans.png'');
            background-position: center center;
            background-repeat: no-repeat;">
        <div style="height: 40px; min-height: 40px;             
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54);"><span stye="font-size: 0pt;">.</span></div>
        <div style="padding: 20px;">
            <img src="http://www.petaccent.com/Content/Images/heart_pets_trans_sm6.png" />
            <div style="top: 30%; padding-right: 10px; margin-bottom: 40px; margin-top: 60px;">
                Welcome, #####!<br /><br />
                Thank you for signing up with PetAccent.com! Your account has been granted a $10 credit for use any time you''d like. Stay tuned for product updates and news from the site! <br /><br />
                10% of every order goes to charity, forever.<br /><br />
                Every time you make a purchase on PetAccent.com 10% of your order''s proceeds will be sent to the charity of your choosing, selected from a list of
                our preferred animal charities at the time of checkout. We send out checks to these charities quarterly. Donating to charity is a part of our committment to helping animals in
                need and is not a temporary policy or promotional offer.<br /><br />
                Sincerely,<br />
                PetAccent.com<br />
                <a href="mailto:customerservice@petaccent.com">CustomerService@PetAccent.com</a>
            </div>            
            <div style="font-size: 9pt; float: right;">* Charity options are listed in the checkout section of <a href="https://www.petaccent.com">petaccent.com</a>.</div>
        </div>
        <div style="height: 25px; min-height: 25px;
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54);"><span stye="font-size: 0pt;">.</span>
        </div>
    </div>
</body>
</html>', 'Welcome, #####!\n\n
                Thank you for signing up with PetAccent.com! Your account has been granted a $10 credit for use any time you''d like. Stay tuned for product updates and news from the site!\n\n
                10% of every order goes to charity, forever.\n\n
                Every time you make a purchase on PetAccent.com 10% of your order''s proceeds will be sent to the charity of your choosing, selected from a list of
                our preferred animal charities at the time of checkout. We send out checks to these charities quarterly. Donating to charity is a part of our committment to helping animals in
                need and is not a temporary policy or promotional offer.\n\n
                Sincerely,\n
                PetAccent.com\n
                CustomerService@PetAccent.com', 'CustomerService@PetAccent.com', 'Customer Service', getdate())

  insert into dbo.[EmailTemplate]
  values ('ForgotPassword', 'Password Request', '<html style="height: 100%;">
<body style="font-family: Arial, Helvetica, sans-serif;">
    <div style="min-height: 100%;
            width: 70%;
            min-width: 380px;
            min-height: 400px;
            border: 2px solid;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
            position: relative;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -webkit-border-bottom-left-radius: 7px;
            -webkit-border-bottom-right-radius: 7px;
            -moz-border-radius-topleft: 7px;
            -moz-border-radius-topright: 7px;
            -moz-border-radius-bottomleft: 7px;
            -moz-border-radius-bottomright: 7px;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            background-image: url(''http://www.petaccent.com/Content/Images/welcome-image-trans.png'');
            background-position: center center;
            background-repeat: no-repeat;">
        <div style="height: 40px; min-height: 40px;             
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54);"><span stye="font-size: 0pt;">.</span></div>
        <div style="padding: 20px;">
            <img src="http://www.petaccent.com/Content/Images/heart_pets_trans_sm6.png" />
            <div style="top: 30%; padding-right: 10px; margin-bottom: 40px; margin-top: 60px;">
                Hi, #####!<br /><br />
                Here is your password. If you need to change your password please log into <a href="https://www.petaccent.com">https://www.petaccent.com</a> and navigate to your profile. If you did not request 
                your password please contact us at <a href="mailto:CustomerService@PetAccent.com">CustomerService@PetAccent.com</a> so that we can take action. Thank you!<br /><br />

                %%%%%<br /><br />

                Sincerely,
                Customer Service, PetAccent.com
            </div>            
            <div style="font-size: 9pt; float: right;">* Charity options are listed in the checkout section of <a href="https://www.petaccent.com">petaccent.com</a>.</div>
        </div>
        <div style="height: 25px; min-height: 25px;
            width: 100%; min-width: 100%;
            background: rgb(182, 54, 54);"><span stye="font-size: 0pt;">.</span>
        </div>
    </div>
</body>
</html>', 'Hi, #####!\n\n
                Here is your password. If you need to change your password please log into https://www.petaccent.com and navigate to your profile. If you did not request 
                your password please contact us at <a href="mailto:CustomerService@PetAccent.com">CustomerService@PetAccent.com</a> so that we can take action. Thank you!\n\n

                %%%%%\n\n

                Sincerely,\n
                Customer Service, PetAccent.com', 'CustomerService@PetAccent.com', 'Customer Service - Pet Accent!', getdate())