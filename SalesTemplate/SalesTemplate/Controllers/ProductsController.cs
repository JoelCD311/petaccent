﻿using BusinessLayer.Services;
using SalesTemplate.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SalesTemplate.Controllers
{
    [AllowAnonymous]
    public class ProductsController : BaseController
    {
        public async Task<ActionResult> Index()
        {
            var model = new ProductsModel();
            var sessionSvc = new SessionService();
            model.ProductList = model.Products.ToList();
            model.Show = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultProductsPerPage"]);
            model.ProductList.RemoveRange(Convert.ToInt32(model.Show), model.ProductList.Count - Convert.ToInt32(model.Show));            
            model.Skip = 0;
            model.CurrentCount = model.Products.Count;
            model.DisplayType = "list";
            model.ShoppingCart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            model.FirstName = User.FirstName;
            model.CartTotal = model.ShoppingCart.Order.OrderLineItems.Sum(x => x.Quantity);
            return View(model);
        }

        public async Task<ActionResult> ProductDetail(string id)
        {
            var sessionSvc = new SessionService();
            var model = new ProductsModel();
            model.Product = model.Products.Where(x => x.SKU == id).FirstOrDefault();

            if (model.Product == null)
                return HttpNotFound();

            var cart = await sessionSvc.GetShoppingCart().ConfigureAwait(false);
            model.FirstName = User.FirstName;
            model.CartTotal = cart.Order.OrderLineItems.Sum(x => x.Quantity);
            model.ShoppingCart = cart;

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<JsonResult> Sort(string show, string skip, string sortby, string category, string pricelow, string pricehigh, string gridtype)
        {
            var prodSvc = new ProductService();
            string[] sortbyArr = sortby.Split(':');
            var model = new ProductsModel();
            var listAndCount = await prodSvc.GetProducts((show == "all" ? 100000 : Convert.ToInt32(show)),
                (Convert.ToInt32(skip) - 1),
                Convert.ToDecimal(pricelow),
                Convert.ToDecimal(pricehigh),
                (sortbyArr[0] == "position" ? "Order" : sortbyArr[0]), sortbyArr[1],
                category).ConfigureAwait(false);

            model.Show = (show == "all" ? 100000 : Convert.ToInt32(show));
            model.Skip = Convert.ToInt32(skip);
            model.ProductList = listAndCount.Item1;
            model.CurrentCount = listAndCount.Item2;
            model.DisplayType = gridtype;

            return Json(this.RenderRazorViewToString(this, "_Products", model));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> SendToFriend(int productID, string refName = "", string refEmail = "", string name = "", string email = "")
        {
            var prodSvc = new ProductService();
            var emailSvc = new EmailService();
            var model = new EmailModel();
            var user = new DataLayer.Data.Entities.User();

            model.Product = await prodSvc.GetProductByID(productID).ConfigureAwait(false);

            if (User.Identity.IsAuthenticated)
            {
                user = new DataLayer.Data.Entities.User() { FirstName = User.FirstName, LastName = User.LastName, EmailAddress = User.EmailAddress };
                model.ReferralName = User.FirstName + " " + user.LastName;
                model.ReferralEmail = User.EmailAddress;
            }
            else
            {
                user = new DataLayer.Data.Entities.User() { FirstName = name, EmailAddress = email };
                model.ReferralName = refName;
                model.ReferralEmail = email;
            }

            var html = RenderRazorViewToString(this, "_SentFromFriend", model);

            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => emailSvc.SendProductToFriend(user, html));

            return Json(true);
        }
    }
}