﻿using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace SalesTemplate.Models
{
    public class ProductsModel : BaseModel
    {
        public List<Product> ProductList { get; set; }
        public int Show { get; set; }
        public int Skip { get; set; }
        public string SortBy { get; set; }
        public string Category { get; set; }
        public decimal PriceLow { get; set; }
        public decimal PriceHigh { get; set; }
        public int CurrentCount { get; set; }
        public string DisplayType { get; set; }
        public Product Product { get; set; }

        public ProductsModel(bool populateBase = true) : base(populateBase) { }

        public readonly List<KeyValuePair<string, string>> SortByOptions = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("position:asc","--"),
            new KeyValuePair<string, string>("price:asc","Price: Lowest first"),
            new KeyValuePair<string, string>("price:desc","Price: Highest first"),
            new KeyValuePair<string, string>("name:asc", "Product Name: A to Z"),
            new KeyValuePair<string, string>("name:desc","Product Name: Z to A")
        };
    }
}