﻿namespace DataLayer.Data.Entities
{
    public partial class OrderStatus
    {
        public int ID { get; set; }
        public string Status { get; set; }
    }
}
