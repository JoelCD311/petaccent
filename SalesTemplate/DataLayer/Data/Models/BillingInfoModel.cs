﻿using DataLayer.Data.Entities;
using System.Collections.Generic;

namespace DataLayer.Data.Models.ViewModels
{
    public class BillingInfoModel
    {
        public string CardID { get; set; }
        public string CardAddressCountry { get; set; }
        public string CardAddressLine1 { get; set; }
        public string CardAddressLine2 { get; set; }
        public string CardAddressCity { get; set; }
        public string CardAddressZip { get; set; }
        public string CardCvc { get; set; }
        public string CardExpirationMonth { get; set; }
        public string CardExpirationYear { get; set; }
        public string NameOnCard { get; set; }
        public string PAN { get; set; }
        public decimal Amount { get; set; }
        public List<Charity> Charities { get; set; }
        public string IPAddress { get; set; }
        public bool SaveCard { get; set; }
    }
}