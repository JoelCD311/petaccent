﻿using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;
using DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class StoreCreditService
    {
        private StoreCreditRepository _repo { get; set; }

        public StoreCreditService()
        {
            _repo = new StoreCreditRepository();
        }

        public async Task<string> CreateCouponCode()
        {
            string retval = String.Empty;

            var random = new Random();
            string input = "9ABC0DEF1GHI2JKL3MNO4PQR5STU6VWX7YZ8";

            while (true)
            {
                var coupon = Enumerable.Range(0, 10).Select(x => input[random.Next(0, input.Length)]);
                retval = new string(coupon.ToArray());

                if (!await _repo.IsCouponCodeUnique(retval).ConfigureAwait(false))
                    return retval;
            }
        }

        public async Task<decimal> GetTotalAvailableCredits(int userID)
        {
            return await _repo.GetTotalAvailableCredits(userID).ConfigureAwait(false);
        }

        public async Task<StoreCredit> CheckCouponCode(string coupon)
        {
            return await _repo.CheckCouponCode(coupon).ConfigureAwait(false);
        }

        public async Task WriteStoreCreditGrantedRecord(int? userID, int? typeID, decimal amount, string descr, string notes, string email = null, string coupon = null)
        {
            await _repo.WriteStoreCreditGrantedRecord(userID, typeID, amount, descr, notes, email, coupon).ConfigureAwait(false);
        }

        public async Task WriteStoreCreditUsedRecord(int userID, int? typeID, decimal amount, string descr, string notes, string coupon)
        {
            await _repo.WriteStoreCreditUsedRecord(userID, typeID, amount, descr, notes, coupon).ConfigureAwait(false);
        }

        public async Task WriteStoreCreditUsedRecord(List<StoreCredit> coupons)
        {
            await _repo.WriteStoreCreditUsedRecord(coupons).ConfigureAwait(false);
        }

        public async Task<StoreCreditType> GetStoreCreditTypeByName(string name)
        {
            return await _repo.GetStoreCreditTypeByName(name).ConfigureAwait(false);
        }
    }
}
