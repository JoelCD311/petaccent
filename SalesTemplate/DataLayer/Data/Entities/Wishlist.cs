﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Data.Entities
{
    public partial class Wishlist
    {
        public int ID { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        [ForeignKey("Product")]
        public int ProductID { get; set; }
        [ForeignKey("ProductOption")]
        public System.Nullable<int> OptionID { get; set; }
        public System.Nullable<int> Quantity { get; set; }
        public System.DateTime DateCreated { get; set; }        

        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductOption ProductOption { get; set; }
    }
}
