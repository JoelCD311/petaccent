﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Data.Contexts;
using DataLayer.Data.Entities;

namespace DataLayer.Repositories
{
    public class PaymentRepository : BaseRepository
    {
        public async Task<PaymentMethod> GetPaymentMethod(string method)
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.PaymentMethods.Where(x => x.Method == method)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);         
            }            
        }

        public async Task<List<PaymentMethod>> GetPaymentMethods()
        {
            using (var _db = new SalesTemplateContext())
            {
                return await _db.PaymentMethods.ToListAsync()
                    .ConfigureAwait(false);
            }
        }
    }
}
